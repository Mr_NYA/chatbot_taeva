# Deploying the API with docker

The point of using docker here is to do a versionning of images. We need to make a new image for each version. Each version is defined by a change in either the dictionaries and graphs or in the binary of the app (so change in the fuctionnality). Then we can choose which version we want to deploy and deploy it on a prod server with a simple `docker run`. 

List of images of chatbot-prod: [click here](https://eu.gcr.io/ia-robotique/chatbot-prod)

List of images of chatbot-dev: [click here](https://eu.gcr.io/ia-robotique/chatbot-dev)

## Auth with google cloud

First you need to be logged in and have permission to do anything with the chatbot images. Do the steps written in this link: https://cloud.google.com/container-registry/docs/advanced-authentication
If you already did the steps to authentify yourself but you have the followiing message "You don't have the needed permissions to perform this operation, and you may have invalid credentials." then try this command:

```shell
gcloud auth print-access-token | sudo docker login -u oauth2accesstoken --password-stdin https://eu.gcr.io
```

You should see this message if all went well:
```shell
...
Login Succeeded
```


## Deploying to a server

Once you are logged in with the necessary permissions, you need to build the image with the `Dockerfile` in this folder. Don't forget to change the version of the image ex: [eu.gcr.io/ia-robotique/chatbot-prod:**testversion**](https://eu.gcr.io/ia-robotique/chatbot-prod)
```shell
$ docker build -t eu.gcr.io/ia-robotique/chatbot-prod:testversion -f deploy/Dockerfile .
```

If you haven't done changes to the image and just want to build a specfic version, you can pass this step and go to the docker run part.
After the image is build you need to publish it. First tag it `docker tag eu.gcr.io/ia-robotique/chatbot-prod:testversion eu.gcr.io/ia-robotique/chatbot-prod:version` then push it.
```shell
$ docker push eu.gcr.io/ia-robotique/chatbot-prod:version
```

Now you can get the image from any server that has internet connection and docker installed. 
You can run the API with a single command.
```shell
$ docker run -d -p 8080:8080 --name chatbot --restart=always eu.gcr.io/ia-robotique/chatbot-prod:version
```

The -d option will make the app run in background allowing you to exit from the server without killing the process.
If you want to stop the bot you can either do `docker stop chatbot & docker rm chatbot` or `docker kill chatbot & docker rm chatbot`.
You can access the API from a web page in the project `src/main/webapp/chatbotWeb/chatbotPage.html`, just change the ip in the script1.js in the same folder to the ip of the server where you are running the API or keep it to localhost if you are running the container on you machine.