#!/bin/bash

## Needs docker and docker compose
# apt install docker.io
# apt install docker-compose

# force update
# docker-compose up --force-recreate --build -d
# docker image prune -f

#update workspace


docker-compose stop
docker-compose rm -f
docker-compose pull
docker-compose rm -f application nginx php
docker-compose -f docker-compose.yml up -d

winpty docker exec -it chatbot-senior_chatbot_1 //bin//bash 
#docker exec -it chatbot-senior_chatbot_1 //bin//bash