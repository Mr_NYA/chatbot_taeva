bs4 : 
Script de scrapping python utilisant la bibliothèque beautifulsoup (bs4)

excel_dic : 
script de création de dictionnaires DELA à partir d'un tableau excel

graphes : 
script d'automatisation de la création de certains graphes tels que les graphes de multiplication et d'addition

test : 
test de script python. La bibliotheque justext a été testé pour savoir si elle était meilleure que la bibliothèque beautifulsoup. Après test, la bibliothèque beautifulsoup a été gardé car plus documenté.

transformation : 
script de transformation d'un fichier vers un autre fichier. Par exemple le fichier "transf_lexique_dic" permet de générer deux fichiers excel au format de l'interface ontomantics.