# Chatbot-senior
Context:

## Development

### Without Docker

Before contributing to the project you must have the following:

1. The lastest [JDK][] available.
2. The Source repository of the [Unitex library][].

Do not forget to set the **JAVA_HOME** variable.
```shell
export JAVA_HOME=/path/to/jdk:$JAVA_HOME
export PATH=$PATH:$JAVA_HOME
```

If, when using your IDE, you see an error with maven not finding the Unitex library, take the .jar from the `additionalFiles` directory and put it in the `.m2` repository
```shell
$ cp additionalFiles/UnitexJni.jar ~/.m2/repository/fr/umlv/unitex/jni/UnitexJni/1.0.0/
$ cp additionalFiles/UnitexJni.pom ~/.m2/repository/fr/umlv/unitex/jni/UnitexJni/1.0.0/
```

The `libUnitexJni.so` in the additionalFiles directory must be pointed by a library path, it can be set using:
```shell
export LD_LIBRARY_PATH='/path/to/additionalFiles/'
```


The `workspace` directory **must** be in the `$HOME` directory


### With Docker

List of images of chatbot-dev: [click here](https://eu.gcr.io/ia-robotique/chatbot-dev)

Obviously you need to have Docker installed (`apt install docker.io` or go to [Docker website][] if you're on OSX or Windows).
Check that the docker daemon is running on OSX and windows.
Now you just need to run the environnement and start developping. Be sure to be placed in the top folder (where you see this README) and change the [**version**](https://eu.gcr.io/ia-robotique/chatbot-dev) to the one you need.
```shell
$ docker run -it --rm -p 443:443 -v `pwd`:/code/ eu.gcr.io/ia-robotique/chatbot-dev:version
```

This will create a container with the environnement set up and all the files needed are there. The -it make it so you can interact with container such as running commands. The --rm deletes the container after exiting. The -p map the port 8080 of your machine to the port 8080 of the container. The -v builds a volume with you curent position, which means (if you did it correctly) that you folder (which should be this entire github repository) is shared with the container in /code/, which means that changes done to the files on the container will be carried to you local machine and vice-versa meaning you can still develop using your favorite IDE. Finally, there is the name of the image you need, don't forget to change the version if the environnement is not suitable.


Once you've entered this command, you are in the container. Now you can run the application, develop or test things. You can run the API with the command `./mvnw` and then check on you local machine the web page in `src/main/webapp/chatbotWeb/chatbotPage.html` to communicate, because the ports are linked/forwarded.


If you made changes to the environnement such as changing the JDK or adding new files necessary for the app, you need to rebuild the image and update the version.
```shell
$ docker build -t eu.gcr.io/ia-robotique/chatbot-dev:testversion .
```

Then don't forget to **test it**. After you're sure that it's working you can publish it. Check what was the last version [here](https://eu.gcr.io/ia-robotique/chatbot-dev), name it with `docker tag eu.gcr.io/ia-robotique/chatbot-dev:testversion eu.gcr.io/ia-robotique/chatbot-dev:newversion` replacing **newversion** by the number of the next version, then you push it.
```shell
$ docker push eu.gcr.io/ia-robotique/chatbot-dev:newversion
```

Now everyone can develop on this new image with the environnement updated.


### Docker Compose

To launch the chatbot using docker-compose, you need to use the bash file `launch.sh`. It will build an image for the chatbot and a nginx image for the webpage. You will then be on the chatbot container.
```sh
$ sudo ./launch.sh
```


Then you can access the page on `localhost` on google chrome.
If the graphs or dictionnaries are update you need to rebuild the images. To rebuild the images, do the following:
```sh
$ docker-compose down
...
$ docker-compose build
...
$ sudo ./launch.sh
```


Now the graphs should be up to date


## Compiling

Here are the things you can do once your environnement is set up:

* run the API (you can exchange to the API with the web page `src/main/webapp/chatbotWeb/chatbotPage.html`):
```shell
$ ./mvnw
```
if you have an error with this line like this `bash: ./mvnw: /bin/sh^M: bad interpreter:` then do `sed -i 's/\r//' mvnw` and now it should be okay.
You can use different profiles when building the chatbot (dev, test and prod) like so :
```shell
# For dev
$ ./mvnw -Pdev
# For test
$ ./mvnw -Ptest
# For prod
$ ./mvnw -Pprod
```

* compile in .jar (output in `target/`, the `libUnitexJni.so` file must still be pointed to and `workspace` be in home directory):
```shell
$ mvn package
    ...
$ java -jar target/chat-bot-unitext-0.0.1-SNAPSHOT.jar
```

* run the node.js:
```shell
$ yarn start
```

* run docker and dev on it:
```shell
$ sudo docker build -t chatbot-dev .
$ sudo docker run -it --rm -p 8080:8080 -p 443:443 -v `pwd`:/code/ chatbot-dev
```

* run docker in detached mode (change `$ENV` by either dev, test, prod):
```shell
$ sudo docker run --name chatbot --restart=always -p 8080:8080 -p 443:443 -v `pwd`:/code/ eu.gcr.io/ia-robotique/chatbot-$ENV:latest
```




### Publishing

The chabot is being tested in CI after each commit. You can check the jenkins for the result of any build at this url: `https://teamnet.ml/jenkins/`.

During the job, the code will be reviewed by Sonar, if the code quality is inferior to the last commit, Sonar will deny the commit. Sonar can be accessed here: `https://teamnet.ml/sonar/`

After a commit has passed, jenkins will build a new image to the registry on google cloud, and then deploy the new image on an instance in google cloud. You can view the registry here: `https://console.cloud.google.com/gcr/images/ia-robotique?project=ia-robotique`

You can see the chatbot container on Portainer and watch the logs and other things related to the container. Portainer can be accessed here: `https://teamnet.ml/portainer/`


### Graphs and Dictionnaries

To update a graph, you need to download it from the dropbox (here: `https://www.dropbox.com/home/teamnet`) and then put or replace the file in one of these folders:

* `workspace/Unitex-GramLab/Unitex/French/Graphs/` pour les graphs.
* `workspace/Unitex-GramLab/Unitex/French/Dela/` pour les dictionnaires.


[JDK]: https://www.oracle.com/technetwork/java/javase/downloads/
[Unitex library]: http://unitexgramlab.org/fr
[Docker website]: https://www.docker.com/


## ERREUR
When you find some problème of connexion  with Docker Server
you need to use VPN
 Installe OPENVPN AND UNZIP FILE OF OPENVIDU :
 `apt-get update`
 `apt-get install openvpn`
` apt-get install wget`
 `apt-get install unzip`
 ## GET OPENVPN ZIP 'wget link' depuis https://www.vpnbook.com/
`wget https://www.vpnbook.com/free-openvpn-account/VPNBook.com-OpenVPN-PL226.zip`
 `unzip VPNBook.com-OpenVPN-PL226`
 `openvpn --config vpnbook-pl226-udp53.ovpn`
 taper l'identifiant et le mot de passe qui se trouvent dans le site : https://www.vpnbook.com/
 ## ERREUR liquibase
 Pour fixer le problème liquibase il taper dans la base la commande :
 `DELETE FROM DATABASECHANGELOGLOCK where ID = 1;`
 ## ERRUER [Cannot start service nginx: driver failed programming external connectivity]
 Pour fixer cette erreur il faut tuer le processus qui point sur le port 8080
 `netstat -ano | findstr :8080`
 `taskkill /PID PID_RECUPERE_DEPUIS_NETSTAT /F`
 ensuite il faut redémarrer docker