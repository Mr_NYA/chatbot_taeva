package org.teamnet.unitext.application.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.teamnet.unitext.application.domain.RegleAjustementMorphologique;

public interface RegleAjustementMorphologiqueRepository extends JpaRepository<RegleAjustementMorphologique, Long> {
    RegleAjustementMorphologique findOneById(int id);

    ArrayList<RegleAjustementMorphologique> findAll();

    ArrayList<RegleAjustementMorphologique> findAllByOrdre(int ordre);

    ArrayList<RegleAjustementMorphologique> findAllByOrderByOrdreAsc();

}
