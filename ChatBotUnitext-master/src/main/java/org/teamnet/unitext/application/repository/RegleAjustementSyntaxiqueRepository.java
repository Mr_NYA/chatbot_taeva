package org.teamnet.unitext.application.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.teamnet.unitext.application.domain.RegleAjustementSyntaxique;

public interface RegleAjustementSyntaxiqueRepository extends JpaRepository<RegleAjustementSyntaxique, Long> {
    RegleAjustementSyntaxique findOneById(int id);

    ArrayList<RegleAjustementSyntaxique> findAll();

    ArrayList<RegleAjustementSyntaxique> findAllByOrderByOrdreAsc();

    ArrayList<RegleAjustementSyntaxique> findAllByOrdre(int ordre);

}
