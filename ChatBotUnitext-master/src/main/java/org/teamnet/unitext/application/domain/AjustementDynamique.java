package org.teamnet.unitext.application.domain;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class AjustementDynamique {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "regle")
    private String regle;

    @Column(name = "action")
    private String action;

    @Column(name = "ordre")
    private Integer ordre;

    @Column(name = "commentaire")
    private String commentaire;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRegle() {
        return regle;
    }

    public String[] getRegleAsTableString(String split) {
        return regle.split(split);
    }

    public void setRegle(String regle) {
        this.regle = regle;
    }

    public String getAction() {
        if (action == null) {
            action = "";
        }
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getOrdre() {
        return ordre;
    }

    public void setOrdre(int ordre) {
        this.ordre = ordre;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

}
