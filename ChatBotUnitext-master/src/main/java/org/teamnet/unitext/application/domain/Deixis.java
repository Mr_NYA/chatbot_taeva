package org.teamnet.unitext.application.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

@Table(name = "deixis")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
public class Deixis {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "classe")
    String classe;

    @Lob
    @Column(name = "valeur")
    String valeur;

    @Column(name = "categorie_grammaticale")
    String categorieGrammaticale;

    @Column(name = "categorie_morphologique")
    String categorieMorphologique;

    @Column(name = "massif_comptable")
    String massifComptable;

    @Column(name = "hyperclasse")
    String hyperclasse;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    public String getCategorieGrammaticale() {
        return categorieGrammaticale;
    }

    public void setCategorieGrammaticale(String categorieGrammaticale) {
        this.categorieGrammaticale = categorieGrammaticale;
    }

    public String getCategorieMorphologique() {
        return categorieMorphologique;
    }

    public void setCategorieMorphologique(String categorieMorphologique) {
        this.categorieMorphologique = categorieMorphologique;
    }

    public String getMassifComptable() {
        return massifComptable;
    }

    public void setMassifComptable(String massifComptable) {
        this.massifComptable = massifComptable;
    }

    public String getHyperclasse() {
        return hyperclasse;
    }

    public void setHyperclasse(String hyperclasse) {
        this.hyperclasse = hyperclasse;
    }

}
