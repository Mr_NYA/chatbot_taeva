package org.teamnet.unitext.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.teamnet.unitext.application.domain.ArgumentCompose;

@Repository
public interface ArgumentComposeRepository extends JpaRepository<ArgumentCompose, Long> {

    ArgumentCompose findOneById(int id);

    ArgumentCompose findOneByForme(String forme);

    ArgumentCompose[] findByForme(String forme);
}
