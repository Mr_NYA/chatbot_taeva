package org.teamnet.unitext.application.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "nslemmes")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
public class Nslemme {
    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Column(name = "Lemme")
    String lemme;

    @Column(name = "catgram")
    String catgram;

    @Column(name = "Flex")
    String flex;

    @Column(name = "Dom")
    String dom;

    @Column(name = "Grs")
    String grs;

    @Column(name = "Maj")
    String maj;

    @Column(name = "Lig")
    String lig;

    @Column(name = "Standard")
    String standard;

    @Column(name = "Notes")
    String notes;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLemme() {
        return lemme;
    }

    public void setLemme(String lemme) {
        this.lemme = lemme;
    }

    public String getCatgram() {
        return catgram;
    }

    public void setCatgram(String catgram) {
        this.catgram = catgram;
    }

    public String getFlex() {
        return flex;
    }

    public void setFlex(String flex) {
        this.flex = flex;
    }

    public String getLig() {
        return lig;
    }

    public void setLig(String lig) {
        this.lig = lig;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public String getDom() {
        return dom;
    }

    public void setDom(String dom) {
        this.dom = dom;
    }

    public String getGrs() {
        return grs;
    }

    public void setGrs(String grs) {
        this.grs = grs;
    }

    public String getMaj() {
        return maj;
    }

    public void setMaj(String maj) {
        this.maj = maj;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
