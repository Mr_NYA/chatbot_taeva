package org.teamnet.unitext.application.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "vscodes")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
public class Vscode {

    @Id
    private String code;
    @Column(name = "modele")
    private String modele;
    @Column(name = "rad")
    private long rad;
    @Column(name = "`r-Modele`")
    private String r_Modele;
    @Column(name = "`inf::`")
    private String inf__;
    @Column(name = "`ind-Pr:1:S`")
    private String ind_Pr_1_S;
    @Column(name = "`ind-Pr:2:S`")
    private String ind_Pr_2_S;
    @Column(name = "`ind-Pr:3:S`")
    private String ind_Pr_3_S;
    @Column(name = "`ind-Pr:1:P`")
    private String ind_Pr_1_P;
    @Column(name = "`ind-Pr:2:P`")
    private String ind_Pr_2_P;
    @Column(name = "`ind-Pr:3:P`")
    private String ind_Pr_3_P;
    @Column(name = "`ind-Imp:1:S`")
    private String ind_Imp_1_S;
    @Column(name = "`ind-Imp:2:S`")
    private String ind_Imp_2_S;
    @Column(name = "`ind-Imp:3:S`")
    private String ind_Imp_3_S;
    @Column(name = "`ind-Imp:1:P`")
    private String ind_Imp_1_P;
    @Column(name = "`ind-Imp:2:P`")
    private String ind_Imp_2_P;
    @Column(name = "`ind-Imp:3:P`")
    private String ind_Imp_3_P;
    @Column(name = "`ind-Ps:1:S`")
    private String ind_Ps_1_S;
    @Column(name = "`ind-Ps:2:S`")
    private String ind_Ps_2_S;
    @Column(name = "`ind-Ps:3:S`")
    private String ind_Ps_3_S;
    @Column(name = "`ind-Ps:1:P`")
    private String ind_Ps_1_P;
    @Column(name = "`ind-Ps:2:P`")
    private String ind_Ps_2_P;
    @Column(name = "`ind-Ps:3:P`")
    private String ind_Ps_3_P;
    @Column(name = "`ind-Fut:1:S`")
    private String ind_Fut_1_S;
    @Column(name = "`ind-Fut:2:S`")
    private String ind_Fut_2_S;
    @Column(name = "`ind-Fut:3:S`")
    private String ind_Fut_3_S;
    @Column(name = "`ind-Fut:1:P`")
    private String ind_Fut_1_P;
    @Column(name = "`ind-Fut:2:P`")
    private String ind_Fut_2_P;
    @Column(name = "`ind-Fut:3:P`")
    private String ind_Fut_3_P;
    @Column(name = "`cond-Pr:1:S`")
    private String cond_Pr_1_S;
    @Column(name = "`cond-Pr:2:S`")
    private String cond_Pr_2_S;
    @Column(name = "`cond-Pr:3:S`")
    private String cond_Pr_3_S;
    @Column(name = "`cond-Pr:1:P`")
    private String cond_Pr_1_P;
    @Column(name = "`cond-Pr:2:P`")
    private String cond_Pr_2_P;
    @Column(name = "`cond-Pr:3:P`")
    private String cond_Pr_3_P;
    @Column(name = "`sub-Pr:1:S`")
    private String sub_Pr_1_S;
    @Column(name = "`sub-Pr:2:S`")
    private String sub_Pr_2_S;
    @Column(name = "`sub-Pr:3:S`")
    private String sub_Pr_3_S;
    @Column(name = "`sub-Pr:1:P`")
    private String sub_Pr_1_P;
    @Column(name = "`sub-Pr:2:P`")
    private String sub_Pr_2_P;
    @Column(name = "`sub-Pr:3:P`")
    private String sub_Pr_3_P;
    @Column(name = "`sub-Imp:1:S`")
    private String sub_Imp_1_S;
    @Column(name = "`sub-Imp:2:S`")
    private String sub_Imp_2_S;
    @Column(name = "`sub-Imp:3:S`")
    private String sub_Imp_3_S;
    @Column(name = "`sub-Imp:1:P`")
    private String sub_Imp_1_P;
    @Column(name = "`sub-Imp:2:P`")
    private String sub_Imp_2_P;
    @Column(name = "`sub-Imp:3:P`")
    private String sub_Imp_3_P;
    @Column(name = "`imp-Pr:2:S`")
    private String imp_Pr_2_S;
    @Column(name = "`imp-Pr:1:P`")
    private String imp_Pr_1_P;
    @Column(name = "`imp-Pr:2:P`")
    private String imp_Pr_2_P;
    @Column(name = "`ppres::`")
    private String ppres__;
    @Column(name = "`pp::S:M`")
    private String pp__S_M;
    @Column(name = "`pp::S:F`")
    private String pp__S_F;
    @Column(name = "`pp::P:M`")
    private String pp__P_M;
    @Column(name = "`pp::P:F`")
    private String pp__P_F;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }


    public long getRad() {
        return rad;
    }

    public void setRad(long rad) {
        this.rad = rad;
    }


    public String getR_Modele() {
        return r_Modele;
    }

    public void setR_Modele(String r_Modele) {
        this.r_Modele = r_Modele;
    }


    public String getInf__() {
        return inf__;
    }

    public void setInf__(String inf__) {
        this.inf__ = inf__;
    }


    public String getInd_Pr_1_S() {
        return ind_Pr_1_S;
    }

    public void setInd_Pr_1_S(String ind_Pr_1_S) {
        this.ind_Pr_1_S = ind_Pr_1_S;
    }


    public String getInd_Pr_2_S() {
        return ind_Pr_2_S;
    }

    public void setInd_Pr_2_S(String ind_Pr_2_S) {
        this.ind_Pr_2_S = ind_Pr_2_S;
    }


    public String getInd_Pr_3_S() {
        return ind_Pr_3_S;
    }

    public void setInd_Pr_3_S(String ind_Pr_3_S) {
        this.ind_Pr_3_S = ind_Pr_3_S;
    }


    public String getInd_Pr_1_P() {
        return ind_Pr_1_P;
    }

    public void setInd_Pr_1_P(String ind_Pr_1_P) {
        this.ind_Pr_1_P = ind_Pr_1_P;
    }


    public String getInd_Pr_2_P() {
        return ind_Pr_2_P;
    }

    public void setInd_Pr_2_P(String ind_Pr_2_P) {
        this.ind_Pr_2_P = ind_Pr_2_P;
    }


    public String getInd_Pr_3_P() {
        return ind_Pr_3_P;
    }

    public void setInd_Pr_3_P(String ind_Pr_3_P) {
        this.ind_Pr_3_P = ind_Pr_3_P;
    }


    public String getInd_Imp_1_S() {
        return ind_Imp_1_S;
    }

    public void setInd_Imp_1_S(String ind_Imp_1_S) {
        this.ind_Imp_1_S = ind_Imp_1_S;
    }


    public String getInd_Imp_2_S() {
        return ind_Imp_2_S;
    }

    public void setInd_Imp_2_S(String ind_Imp_2_S) {
        this.ind_Imp_2_S = ind_Imp_2_S;
    }


    public String getInd_Imp_3_S() {
        return ind_Imp_3_S;
    }

    public void setInd_Imp_3_S(String ind_Imp_3_S) {
        this.ind_Imp_3_S = ind_Imp_3_S;
    }


    public String getInd_Imp_1_P() {
        return ind_Imp_1_P;
    }

    public void setInd_Imp_1_P(String ind_Imp_1_P) {
        this.ind_Imp_1_P = ind_Imp_1_P;
    }


    public String getInd_Imp_2_P() {
        return ind_Imp_2_P;
    }

    public void setInd_Imp_2_P(String ind_Imp_2_P) {
        this.ind_Imp_2_P = ind_Imp_2_P;
    }


    public String getInd_Imp_3_P() {
        return ind_Imp_3_P;
    }

    public void setInd_Imp_3_P(String ind_Imp_3_P) {
        this.ind_Imp_3_P = ind_Imp_3_P;
    }


    public String getInd_Ps_1_S() {
        return ind_Ps_1_S;
    }

    public void setInd_Ps_1_S(String ind_Ps_1_S) {
        this.ind_Ps_1_S = ind_Ps_1_S;
    }


    public String getInd_Ps_2_S() {
        return ind_Ps_2_S;
    }

    public void setInd_Ps_2_S(String ind_Ps_2_S) {
        this.ind_Ps_2_S = ind_Ps_2_S;
    }


    public String getInd_Ps_3_S() {
        return ind_Ps_3_S;
    }

    public void setInd_Ps_3_S(String ind_Ps_3_S) {
        this.ind_Ps_3_S = ind_Ps_3_S;
    }


    public String getInd_Ps_1_P() {
        return ind_Ps_1_P;
    }

    public void setInd_Ps_1_P(String ind_Ps_1_P) {
        this.ind_Ps_1_P = ind_Ps_1_P;
    }


    public String getInd_Ps_2_P() {
        return ind_Ps_2_P;
    }

    public void setInd_Ps_2_P(String ind_Ps_2_P) {
        this.ind_Ps_2_P = ind_Ps_2_P;
    }


    public String getInd_Ps_3_P() {
        return ind_Ps_3_P;
    }

    public void setInd_Ps_3_P(String ind_Ps_3_P) {
        this.ind_Ps_3_P = ind_Ps_3_P;
    }


    public String getInd_Fut_1_S() {
        return ind_Fut_1_S;
    }

    public void setInd_Fut_1_S(String ind_Fut_1_S) {
        this.ind_Fut_1_S = ind_Fut_1_S;
    }


    public String getInd_Fut_2_S() {
        return ind_Fut_2_S;
    }

    public void setInd_Fut_2_S(String ind_Fut_2_S) {
        this.ind_Fut_2_S = ind_Fut_2_S;
    }


    public String getInd_Fut_3_S() {
        return ind_Fut_3_S;
    }

    public void setInd_Fut_3_S(String ind_Fut_3_S) {
        this.ind_Fut_3_S = ind_Fut_3_S;
    }


    public String getInd_Fut_1_P() {
        return ind_Fut_1_P;
    }

    public void setInd_Fut_1_P(String ind_Fut_1_P) {
        this.ind_Fut_1_P = ind_Fut_1_P;
    }


    public String getInd_Fut_2_P() {
        return ind_Fut_2_P;
    }

    public void setInd_Fut_2_P(String ind_Fut_2_P) {
        this.ind_Fut_2_P = ind_Fut_2_P;
    }


    public String getInd_Fut_3_P() {
        return ind_Fut_3_P;
    }

    public void setInd_Fut_3_P(String ind_Fut_3_P) {
        this.ind_Fut_3_P = ind_Fut_3_P;
    }


    public String getCond_Pr_1_S() {
        return cond_Pr_1_S;
    }

    public void setCond_Pr_1_S(String cond_Pr_1_S) {
        this.cond_Pr_1_S = cond_Pr_1_S;
    }


    public String getCond_Pr_2_S() {
        return cond_Pr_2_S;
    }

    public void setCond_Pr_2_S(String cond_Pr_2_S) {
        this.cond_Pr_2_S = cond_Pr_2_S;
    }


    public String getCond_Pr_3_S() {
        return cond_Pr_3_S;
    }

    public void setCond_Pr_3_S(String cond_Pr_3_S) {
        this.cond_Pr_3_S = cond_Pr_3_S;
    }


    public String getCond_Pr_1_P() {
        return cond_Pr_1_P;
    }

    public void setCond_Pr_1_P(String cond_Pr_1_P) {
        this.cond_Pr_1_P = cond_Pr_1_P;
    }


    public String getCond_Pr_2_P() {
        return cond_Pr_2_P;
    }

    public void setCond_Pr_2_P(String cond_Pr_2_P) {
        this.cond_Pr_2_P = cond_Pr_2_P;
    }


    public String getCond_Pr_3_P() {
        return cond_Pr_3_P;
    }

    public void setCond_Pr_3_P(String cond_Pr_3_P) {
        this.cond_Pr_3_P = cond_Pr_3_P;
    }


    public String getSub_Pr_1_S() {
        return sub_Pr_1_S;
    }

    public void setSub_Pr_1_S(String sub_Pr_1_S) {
        this.sub_Pr_1_S = sub_Pr_1_S;
    }


    public String getSub_Pr_2_S() {
        return sub_Pr_2_S;
    }

    public void setSub_Pr_2_S(String sub_Pr_2_S) {
        this.sub_Pr_2_S = sub_Pr_2_S;
    }


    public String getSub_Pr_3_S() {
        return sub_Pr_3_S;
    }

    public void setSub_Pr_3_S(String sub_Pr_3_S) {
        this.sub_Pr_3_S = sub_Pr_3_S;
    }


    public String getSub_Pr_1_P() {
        return sub_Pr_1_P;
    }

    public void setSub_Pr_1_P(String sub_Pr_1_P) {
        this.sub_Pr_1_P = sub_Pr_1_P;
    }


    public String getSub_Pr_2_P() {
        return sub_Pr_2_P;
    }

    public void setSub_Pr_2_P(String sub_Pr_2_P) {
        this.sub_Pr_2_P = sub_Pr_2_P;
    }


    public String getSub_Pr_3_P() {
        return sub_Pr_3_P;
    }

    public void setSub_Pr_3_P(String sub_Pr_3_P) {
        this.sub_Pr_3_P = sub_Pr_3_P;
    }


    public String getSub_Imp_1_S() {
        return sub_Imp_1_S;
    }

    public void setSub_Imp_1_S(String sub_Imp_1_S) {
        this.sub_Imp_1_S = sub_Imp_1_S;
    }


    public String getSub_Imp_2_S() {
        return sub_Imp_2_S;
    }

    public void setSub_Imp_2_S(String sub_Imp_2_S) {
        this.sub_Imp_2_S = sub_Imp_2_S;
    }


    public String getSub_Imp_3_S() {
        return sub_Imp_3_S;
    }

    public void setSub_Imp_3_S(String sub_Imp_3_S) {
        this.sub_Imp_3_S = sub_Imp_3_S;
    }


    public String getSub_Imp_1_P() {
        return sub_Imp_1_P;
    }

    public void setSub_Imp_1_P(String sub_Imp_1_P) {
        this.sub_Imp_1_P = sub_Imp_1_P;
    }


    public String getSub_Imp_2_P() {
        return sub_Imp_2_P;
    }

    public void setSub_Imp_2_P(String sub_Imp_2_P) {
        this.sub_Imp_2_P = sub_Imp_2_P;
    }


    public String getSub_Imp_3_P() {
        return sub_Imp_3_P;
    }

    public void setSub_Imp_3_P(String sub_Imp_3_P) {
        this.sub_Imp_3_P = sub_Imp_3_P;
    }


    public String getImp_Pr_2_S() {
        return imp_Pr_2_S;
    }

    public void setImp_Pr_2_S(String imp_Pr_2_S) {
        this.imp_Pr_2_S = imp_Pr_2_S;
    }


    public String getImp_Pr_1_P() {
        return imp_Pr_1_P;
    }

    public void setImp_Pr_1_P(String imp_Pr_1_P) {
        this.imp_Pr_1_P = imp_Pr_1_P;
    }


    public String getImp_Pr_2_P() {
        return imp_Pr_2_P;
    }

    public void setImp_Pr_2_P(String imp_Pr_2_P) {
        this.imp_Pr_2_P = imp_Pr_2_P;
    }


    public String getPpres__() {
        return ppres__;
    }

    public void setPpres__(String ppres__) {
        this.ppres__ = ppres__;
    }


    public String getPp__S_M() {
        return pp__S_M;
    }

    public void setPp__S_M(String pp__S_M) {
        this.pp__S_M = pp__S_M;
    }


    public String getPp__S_F() {
        return pp__S_F;
    }

    public void setPp__S_F(String pp__S_F) {
        this.pp__S_F = pp__S_F;
    }


    public String getPp__P_M() {
        return pp__P_M;
    }

    public void setPp__P_M(String pp__P_M) {
        this.pp__P_M = pp__P_M;
    }


    public String getPp__P_F() {
        return pp__P_F;
    }

    public void setPp__P_F(String pp__P_F) {
        this.pp__P_F = pp__P_F;
    }

}
