package org.teamnet.unitext.application.service.util;

import java.util.List;

public class WordLists {
    private List<String> conj;
    private List<String> nms;
    private List<String> adjs;
    private List<String> verbes;
    private List<String> vbConj;
    private List<String> dts;
    private List<String> acts;
    private List<String> vbInf;

    public List<String> getVerbes() {
        return verbes;
    }

    public void setVerbes(List<String> verbes) {
        this.verbes = verbes;
    }

    public List<String> getConjugaisons() {
        return conj;
    }

    public void setConjugaisons(List<String> conjugaisons) {
        this.conj = conjugaisons;
    }

    public List<String> getNoms() {
        return nms;
    }

    public void setNoms(List<String> noms) {
        this.nms = noms;
    }

    public List<String> getAdjectifs() {
        return adjs;
    }

    public void setAdjectifs(List<String> adjectifs) {
        this.adjs = adjectifs;
    }

    public List<String> getVerbesConj() {
        return vbConj;
    }

    public void setVerbesConj(List<String> verbesConj) {
        this.vbConj = verbesConj;
    }

    public List<String> getDets() {
        return dts;
    }

    public void setDets(List<String> dets) {
        this.dts = dets;
    }

    public List<String> getActus() {
        return acts;
    }

    public void setActus(List<String> actus) {
        this.acts = actus;
    }

    public List<String> getVerbeInf() {
        return vbInf;
    }

    public void setVerbeInf(List<String> verbeInf) {
        this.vbInf = verbeInf;
    }
}
