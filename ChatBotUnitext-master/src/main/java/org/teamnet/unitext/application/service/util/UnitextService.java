package org.teamnet.unitext.application.service.util;

import fr.umlv.unitex.jni.UnitexJni;
import java.io.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Service;

@Service
public class UnitextService {

    private UnitexJni JNI;
    private String strPathWorspace =
        System.getProperty("user.home") + "/workspace";
    private String strDefaultPathDicFolder =
        System.getProperty("user.home") +
        "/workspace/Unitex-GramLab/Unitex/French/Dela";
    private String strPathGraphFile;
    private String dic = ".dic";
    private String strGraphFileName = "GRAPHE_PERE.fst2";
    private String strDicFileName = "team_net";
    private String strDicNomSensation = "nom_sensation";
    private String strDicVetement = "VETEMENT";
    private String strDicPartieCorps = "partie_corps";
    private String strDicProblemeSante = "probleme_sante";
    private String strDicChaineTv = "chaine_tv";
    private String strDicChaineRadio = "chaine_radio";
    private String strDicChiffre = "chiffre";

    private String strPathDicFile;
    private String strPathWorkSpaceUnitexFrench;
    private String strPathCorpus = strPathWorspace;
    private String strPathGraph = strPathWorkSpaceUnitexFrench;
    private String strInputFileName;
    private String strResultFileName;
    private String text;
    private String strPathLog = strPathWorkSpaceUnitexFrench + "log.txt";
    private int counter;
    private static Logger logger = Logger.getLogger("UnitextService");
    private FileHandler fh;

    public UnitextService() {
        preCompilation();
        this.strPathWorkSpaceUnitexFrench =
            strPathWorspace + "/Unitex-GramLab/Unitex/French";
        this.strPathCorpus = strPathWorkSpaceUnitexFrench + "/Corpus";
        this.strPathGraph = strPathWorkSpaceUnitexFrench + "/Graphs";
        this.strInputFileName = "inputText";
        this.strResultFileName = "resultText";
        this.strPathGraphFile = strPathGraph + "/" + strGraphFileName;
        this.strPathDicFile = strDefaultPathDicFolder + "/" + strDicFileName;
    }

    public void preCompilation() {

       compileDictionnaire(strPathDicFile);
        File[] listDico = new File(strDefaultPathDicFolder).listFiles();
        logger.info("strDefaultPathDicFolder ==> " +  strDefaultPathDicFolder);
        logger.log(
            Level.SEVERE,
            "listDico: {0}",
            listDico.length
        );
        for (File fileEntry : listDico) {
            String name =fileEntry.getName();
            logger.log(
                Level.SEVERE,
                "Name: {0}",
                name
            );
            logger.info("nom fichier du repot Dela " + name);
            if(name.endsWith(".dic")){
                logger.info("dictionnaire selectionné " + strDefaultPathDicFolder + "/" + name);
                compileDictionnaire(strDefaultPathDicFolder + "/" + name.replaceFirst("[.][^.]+$",""));
            }
        }
    }

    public String chatWithMe(String message) throws IOException {
        counter = 0;

        JNI = new UnitexJni();

        Calendar calendar = Calendar.getInstance();
        Timestamp currenttime = new Timestamp(calendar.getTime().getTime());

        String strInputFile =
            currenttime.getTime() + "_" + strInputFileName + "_" + counter++;
        String strResultFile =
            currenttime.getTime() + "_" + strResultFileName + "_" + counter;
        String strPathInputFile = strPathCorpus + "/" + strInputFile;
        String strPathResultFile = strPathCorpus + "/" + strResultFile;

        File inputFile = createFile(strPathInputFile);
        File outputFile = createFile(strPathResultFile);
        System.out.println(
            "StrPathinputfile: " +
            strPathInputFile +
            "\nstrpathresultfile" +
            strPathResultFile +
            "\nstrpathcorpus" +
            strPathCorpus +
            "\ninputfilename : " +
            strInputFileName +
            "\ninputfilename with this : " +
            strInputFileName
        );
        String messageString = message.toString();
        textToFile(messageString, inputFile);

        analyseFile(inputFile, outputFile);

        String resultat = loadFileString(outputFile);

       deleteFiles(inputFile, outputFile);

        return resultat;
    }

    public boolean analyseFile(File inputFile, File outputFile) {
        String strInPath = inputFile.getPath() + "_snt";
        String strOutPath = outputFile.getPath() + "_snt";
        File inPath = new File(strInPath);
        File outPath = new File(strOutPath);
        if (!inPath.exists()) {
           inPath.mkdir();

        }

        if (!outPath.exists()) {
            outPath.mkdir();
        }

        String strPathTraitedFile =
            inputFile.getParent() +
            "/" +
            renameFileExtension(inputFile.getName(), "snt");
        pretraitement(inputFile, strPathTraitedFile, inPath);

        ArrayList<String> allDicoList = new ArrayList<String>();

        for (File fileEntry : new File(strDefaultPathDicFolder).listFiles()) {
            if (fileEntry.getName().contains(".bin")) allDicoList.add(
                fileEntry.getName()
            );
        }
        String strAllDicoFlag = "";
        String strAllDico = "";

        for (String dico : allDicoList) {
            strAllDicoFlag +=
                "\"-m" + strDefaultPathDicFolder + "/" + dico + "\" ";
        }

        for (String dico : allDicoList) {
            strAllDico += "\"" + strDefaultPathDicFolder + "/" + dico + "\" ";
        }

        logger.info("strDefaultPathDicFolder " + strDefaultPathDicFolder);
        chargerDictionnaire(
            strPathTraitedFile,
            strAllDico,
            strAllDicoFlag,
            inPath
        );

        appliquerGraphe(
            strPathGraphFile,
            strPathTraitedFile,
            outputFile.getPath(),
            inPath,
            strAllDicoFlag
        );

        return true;
    }

    public String pretraitement(
        File inputFile,
        String strPathTraitedFile,
        File inPath
    ) {
        // Prétraitement
        logger.info("PRETRAITEMENT");
        logger.info("pretraitement ==============> Normalize ");
        UnitexJni.execUnitexTool(
            "UnitexTool Normalize \"" +
            inputFile.getPath() +
            "\" \"-r" +
            strPathWorkSpaceUnitexFrench +
            "/Norm.txt\" \"--output_offsets=" +
            inPath.getPath() +
            "/normalize.out.offsets\" -qutf8-no-bom"
        );
        logger.info("pretraitement ==============> Grf2Fst2 ");
        UnitexJni.execUnitexTool(
            "UnitexTool Grf2Fst2 \"" +
            strPathGraph +
            "/Preprocessing/Sentence/Sentence.grf\" -y \"--alphabet=" +
            strPathWorkSpaceUnitexFrench +
            "/Alphabet.txt\" -qutf8-no-bom"
        );
        logger.info("pretraitement ==============> Flatten ");
        UnitexJni.execUnitexTool(
            "UnitexTool Flatten \"" +
            strPathGraph +
            "/Preprocessing/Sentence/Sentence.fst2\" --rtn -d5 -qutf8-no-bom"
        );
        logger.info("pretraitement ==============> Fst2Txt ");
        UnitexJni.execUnitexTool(
            "UnitexTool Fst2Txt \"-t/" +
            strPathTraitedFile +
            "\" \"" +
            strPathGraph +
            "/Preprocessing/Sentence/Sentence.fst2\" \"-a" +
            strPathWorkSpaceUnitexFrench +
            "/Alphabet.txt\" -M \"--input_offsets=" +
            inPath.getPath() +
            "/normalize.out.offsets\" \"--output_offsets=" +
            inPath.getPath() +
            "/normalize.out.offsets\" -qutf8-no-bom"
        );
        logger.info("pretraitement ==============> Grf2Fst2 ");
        UnitexJni.execUnitexTool(
            "UnitexTool Grf2Fst2 \"" +
            strPathGraph +
            "/Preprocessing/Replace/Replace.grf\" -y \"--alphabet=" +
            strPathWorkSpaceUnitexFrench +
            "/Alphabet.txt\" -qutf8-no-bom"
        );
        logger.info("pretraitement ==============> Fst2Txt ");
        UnitexJni.execUnitexTool(
            "UnitexTool Fst2Txt \"-t" +
            strPathTraitedFile +
            "\" \"" +
            strPathGraph +
            "/Preprocessing/Replace/Replace.fst2\" \"-a" +
            strPathWorkSpaceUnitexFrench +
            "/Alphabet.txt\" -R \"--input_offsets=" +
            inPath.getPath() +
            "/normalize.out.offsets\" \"--output_offsets=" +
            inPath.getPath() +
            "/normalize.out.offsets\" -qutf8-no-bom"
        );
        // Resultat de la premier étape => tnbot.snt
        logger.info("pretraitement ==============> Tokenize ");
        UnitexJni.execUnitexTool(
            "UnitexTool Tokenize \"" +
            strPathTraitedFile +
            "\" \"-a" +
            strPathWorkSpaceUnitexFrench +
            "/Alphabet.txt\" \"--input_offsets=" +
            inPath.getPath() +
            "/normalize.out.offsets\" \"--output_offsets=" +
            inPath.getPath() +
            "/tokenize.out.offsets\" -qutf8-no-bom"
        );

        return strPathTraitedFile;
    }

    public Boolean chargerDictionnaire(
        String strPathTraitedFile,
        String strAllDico,
        String strAllDicoFlag,
        File inPath
    ) {
        // Prétraitement : Charger le dictionnaire local
        logger.info("Start chargerDictionnaire");
        logger.info("LOADING: " + strAllDico);
        logger.info("LOADING: chargerDictionnaire 1" );
        logger.info("Charger Dictionnaire ===============> Dico  ");
        logger.info("strPathTraitedFile -t ==> " + strPathTraitedFile);
        logger.info("strPathWorkSpaceUnitexFrench ==> -a" + strPathWorkSpaceUnitexFrench);
        logger.info("strAllDicoFlag ==> " + strAllDicoFlag);
        logger.info("strAllDico ==> " + strAllDico);
        logger.info("strDefaultPathDicFolder ==> " + strDefaultPathDicFolder);
        UnitexJni.execUnitexTool(
            "UnitexTool Dico \"-t" +
           strPathTraitedFile +
            "\" \"-a" +
            strPathWorkSpaceUnitexFrench +
            "/Alphabet.txt\" " +
            strAllDicoFlag +
            strAllDico +
            "\"" +
            strDefaultPathDicFolder +
            "/motsGramf-.bin\" \"" +
            strDefaultPathDicFolder +
            "/dela-fr-public.bin\" -qutf8-no-bom"
        );
        logger.info("Charger Dictionnaire ===============> SortTxt 1");
        UnitexJni.execUnitexTool(
            "UnitexTool SortTxt \"" +
            inPath.getPath() +
            "/dlf\" \"-l" +
            inPath.getPath() +
            "/dlf.n\" \"-o" +
            strPathWorkSpaceUnitexFrench +
            "/Alphabet_sort.txt\" -qutf8-no-bom"
        );
        logger.info("Charger Dictionnaire ===============> SortTxt 2");
        UnitexJni.execUnitexTool(
            "UnitexTool SortTxt \"" +
            inPath.getPath() +
            "/dlc\" \"-l" +
            inPath.getPath() +
            "/dlc.n\" \"-o" +
            strPathWorkSpaceUnitexFrench +
            "/Alphabet_sort.txt\" -qutf8-no-bom"
        );
        logger.info("Charger Dictionnaire ===============> SortTxt 3");
        UnitexJni.execUnitexTool(
            "UnitexTool SortTxt \"" +
            inPath.getPath() +
            "/err\" \"-l" +
            inPath.getPath() +
            "/err.n\" \"-o" +
            strPathWorkSpaceUnitexFrench +
            "/Alphabet_sort.txt\" -qutf8-no-bom"
        );
        logger.info("Charger Dictionnaire ===============> SortTxt 4");
        UnitexJni.execUnitexTool(
            "UnitexTool SortTxt \"" +
            inPath.getPath() +
            "/tags_err\" \"-l" +
            inPath.getPath() +
            "/tags_err.n\" \"-o" +
            strPathWorkSpaceUnitexFrench +
            "/Alphabet_sort.txt\" -qutf8-no-bom"
        );

    //    logger.info("chargerDictionnaire ==============> Grf2Fst2 ");
    //    UnitexJni.execUnitexTool(
    //        "UnitexTool Grf2Fst2 \"" +
    //           "/root/workspace/Unitex-GramLab/Unitex/French/Graphs/GRAPHE_PERE.grf\" -y \"--alphabet=/root/workspace/Unitex-GramLab/Unitex/French/Alphabet.txt\" -d \"/root/workspace/Unitex-GramLab/Unitex/French/Graphs/Outil\"  -qutf8-no-bom"
    //    );
       logger.info("End chargerDictionnaire");

        return true;
    }

    public Boolean chargerDictionnaire(
        String strPathTraitedFile,
        String strPathCompiledDicFile,
        File inPath
    ) {

        logger.info("LOADING: chargerDictionnaire 2" );
        UnitexJni.execUnitexTool(
            "UnitexTool Dico \"-t" +
            strPathTraitedFile +
            "\" \"-a" +
            strPathWorkSpaceUnitexFrench +
            "/Alphabet.txt\" \"" +
            strDefaultPathDicFolder +
            "/motsGramf-.bin\" \"" +
            strDefaultPathDicFolder +
            "/dela-fr-public.bin\" \"" +
            strDefaultPathDicFolder +
            "/team_net.bin\" \"" +
            strPathCompiledDicFile +
            "\" -qutf8-no-bom"
        );
        UnitexJni.execUnitexTool(
            "UnitexTool SortTxt \"" +
            inPath.getPath() +
            "/dlf\" \"-l" +
            inPath.getPath() +
            "/dlf.n\" \"-o" +
            strPathWorkSpaceUnitexFrench +
            "/Alphabet_sort.txt\" -qutf8-no-bom"
        );
        UnitexJni.execUnitexTool(
            "UnitexTool SortTxt \"" +
            inPath.getPath() +
            "/dlc\" \"-l" +
            inPath.getPath() +
            "/dlc.n\" \"-o" +
            strPathWorkSpaceUnitexFrench +
            "/Alphabet_sort.txt\" -qutf8-no-bom"
        );
        UnitexJni.execUnitexTool(
            "UnitexTool SortTxt \"" +
            inPath.getPath() +
            "/err\" \"-l" +
            inPath.getPath() +
            "/err.n\" \"-o" +
            strPathWorkSpaceUnitexFrench +
            "/Alphabet_sort.txt\" -qutf8-no-bom"
        );
        UnitexJni.execUnitexTool(
            "UnitexTool SortTxt \"" +
            inPath.getPath() +
            "/tags_err\" \"-l" +
            inPath.getPath() +
            "/tags_err.n\" \"-o" +
            strPathWorkSpaceUnitexFrench +
            "/Alphabet_sort.txt\" -qutf8-no-bom"
        );

        return true;
    }

    public String compileDictionnaire(String strPathDicFile) {
        logger.info("Start compileDictionnaire");
        // Charger le dictionnaire local
        // La compression peusx étre une seul fois.
        strPathDicFile += dic;
        logger.info("COMPILING: " + strPathDicFile);
        UnitexJni.execUnitexTool(
            "UnitexTool Compress \"" + strPathDicFile + "\" -qutf8-no-bom"
        );
        String strPathDicFileCompiled = renameFileExtension(
            strPathDicFile,
            ".bin"
        );
        logger.info("End compileDictionnaire");
        return strPathDicFileCompiled;
    }

    public boolean appliquerGraphe(
        String strPathGraphFile,
        String strPathTraitedFile,
        String strPathResultFile,
        File inPath,
        String strAllDicoFlag
    ) {
        logger.info("Start appliquerGraphe");
        logger.info("Application du dictionnaire || strPathGraphFile ===> " + strPathGraphFile + " || strPathTraitedFile ===> " + strPathTraitedFile + " || strPathResultFile ==> " + strPathResultFile );
        // Executer le graphe
        logger.info("Appliquer Graph ===============> Locate");
        logger.info("Appliquer Graph ===============> Execution" +   "UnitexTool Locate \"-t" +
            strPathTraitedFile +
            "\" \"" +
            strPathGraphFile +
            "\" \"-a" +
            strPathWorkSpaceUnitexFrench +
            "/Alphabet.txt\" -L -R --all -b -Y --stack_max=1000 --max_matches_per_subgraph=200 --max_matches_at_token_pos=400 --max_errors=50 -qutf8-no-bom");
       UnitexJni.execUnitexTool(
            "UnitexTool Locate \"-t" +
            strPathTraitedFile +
            "\" \"" +
            strPathGraphFile +
            "\" \"-a" +
            strPathWorkSpaceUnitexFrench +
            "/Alphabet.txt\" -A -R -n200 " + strAllDicoFlag + " -b -Y --stack_max=1000 --max_matches_per_subgraph=200 --max_matches_at_token_pos=400 --max_errors=50 -qutf8-no-bom"
        );
        logger.info("Appliquer Graph ===============> Concord");
        logger.info("Appliquer Graph ===============> Execution " + "UnitexTool Concord \"" +
            inPath.getPath() +
            "/concord.ind\" \"-m" +
            strPathResultFile +
            "\" -qutf8-no-bom");
         UnitexJni.execUnitexTool(
            "UnitexTool Concord \"" +
            inPath.getPath() +
            "/concord.ind\" \"-m" +
            strPathResultFile +
            "\" -qutf8-no-bom"
        );
        logger.info("Appliquer Graph ===============> Normalize");
        logger.info("strPathResultFile ==> " + strPathResultFile);
        logger.info("strPathWorkSpaceUnitexFrench ==> " + strPathWorkSpaceUnitexFrench);
      UnitexJni.execUnitexTool(
            "UnitexTool Normalize \"" +
            strPathResultFile +
            "\" \"-r" +
            strPathWorkSpaceUnitexFrench +
            "/Norm.txt\" -qutf8-no-bom"
        );
        logger.info("Appliquer Graph ===============> Tokenize");
      UnitexJni.execUnitexTool(
            "UnitexTool Tokenize \"" +
            strPathResultFile +
            "\" \"-a" +
            strPathWorkSpaceUnitexFrench +
            "/Alphabet.txt\" -qutf8-no-bom"
        );

        logger.info("End appliquerGraphe");
        return true;
    }

    public static String renameFileExtension(
        String source,
        String newExtension
    ) {
        String target;
        String currentExtension = getFileExtension(source);

        if (currentExtension.equals("")) {
            target = source + "." + newExtension;
        } else {
            target =
                source.replaceFirst(
                    Pattern.quote("." + currentExtension) + "$",
                    Matcher.quoteReplacement("." + newExtension)
                );
        }
        return target;
    }

    public static String getFileExtension(String f) {
        String ext = "";
        int i = f.lastIndexOf('.');
        if (i > 0 && i < f.length() - 1) {
            ext = f.substring(i + 1);
        }
        return ext;
    }

    public Boolean textToFile(String text, File inputFile) {
        try (
            BufferedWriter writer = new BufferedWriter(
                new FileWriter(inputFile)
            )
        ) {
            writer.write(text);
            writer.close();

            return true;
        } catch (IOException e) {
            logger.log(
                Level.SEVERE,
                "Text to file error: {0}",
                e.getLocalizedMessage()
            );
            return false;
        } catch (Exception e) {
            logger.log(
                Level.SEVERE,
                "Text to file error: {0}",
                e.getLocalizedMessage()
            );
            return false;
        }
    }

    public File createFile(String strPathFileName) {
        File file = new File(strPathFileName);
        if (!file.exists()) {
            try {
                boolean ret = file.createNewFile();
                if (!ret) {
                    logger.log(
                        Level.SEVERE,
                        "createFile(): Could not create file."
                    );
                }
                return file;
            } catch (IOException e1) {
                logger.log(
                    Level.SEVERE,
                    "Error creating file: {0}",
                    e1.getLocalizedMessage()
                );
                return null;
            }
        }
        return file;
    }

    public String loadFileString(File file) throws IOException {
        Reader reader = new InputStreamReader(
            new FileInputStream(file),
            "UTF-8"
        );
        try {
            StringBuilder builder = new StringBuilder();
            char[] buffer = new char[512];
            int nbRead = reader.read(buffer);
            while (nbRead > 0) {
                builder.append(buffer, 0, nbRead);
                nbRead = reader.read(buffer);
            }
            String result = builder.toString();
            return result;
        } finally {
            reader.close();
        }
    }

    public void logMessage(String resultat) {
        File logs = new File(strPathLog);

        try {
            if (!logs.exists()) {
                boolean ret = logs.createNewFile();
                if (!ret) {
                    logger.log(
                        Level.SEVERE,
                        "logMessage(): Could not create file: {0}.",
                        strPathLog
                    );
                }
            }
            fh = new FileHandler(strPathLog, true);
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
            logger.info(resultat);
            fh.close();
        } catch (IOException e) {
            logger.log(
                Level.SEVERE,
                "Error logging message: {0}",
                e.getLocalizedMessage()
            );
        }
    }

    private void checkDelete(boolean ret, File f) {
        if (!ret) {
            logger.log(
                Level.SEVERE,
                "checkDelete(): Could not delete file {0}.",
                f.getName()
            );
        }
    }

    public void deleteFiles(File inputFile, File outputFile) {
        File inPath = new File(inputFile.getPath() + "_snt");
        File outPath = new File(outputFile.getPath() + "_snt");
        File traitedFile = new File(
            inputFile.getParent() +
            "/" +
            renameFileExtension(inputFile.getName(), "snt")
        );
        File resultTraitedFile = new File(
            outputFile.getParent() +
            "/" +
            renameFileExtension(outputFile.getName(), "snt")
        );
        File inputTempFile = new File(
            inputFile.getParent() +
            "/" +
            renameFileExtension(inputFile.getName(), "tmp")
        );
        File outputTempFile = new File(
            outputFile.getParent() +
            "/" +
            renameFileExtension(outputFile.getName(), "tmp")
        );
        deleteAll(inPath);
        deleteAll(outPath);
        checkDelete(inputFile.delete(), inputFile);
        checkDelete(traitedFile.delete(), traitedFile);
        checkDelete(outputFile.delete(), outputFile);
        checkDelete(resultTraitedFile.delete(), resultTraitedFile);
        checkDelete(inputTempFile.delete(), inputTempFile);
        checkDelete(outputTempFile.delete(), outputTempFile);
    }

    private void deleteAll(File dir) {
        if (dir.isDirectory()) {
            if (dir.list().length == 0) {
                checkDelete(dir.delete(), dir);
                System.out.println(
                    "Directory is deleted : " + dir.getAbsolutePath()
                );
            } else {
                File files[] = dir.listFiles();

                for (File file : files) {
                    checkDelete(file.delete(), file);
                }
                deleteAll(dir);
            }
        }
    }

    public UnitexJni getUnitexJni() {
        return JNI;
    }

    public String getStrPathWorspace() {
        return strPathWorspace;
    }

    public String getStrPathWorkSpaceUnitexFrench() {
        return strPathWorkSpaceUnitexFrench;
    }

    public String getStrPathCorpus() {
        return strPathCorpus;
    }

    public String getStrPathGraph() {
        return strPathGraph;
    }

    public String getStrDefaultPathDicFolder() {
        return strDefaultPathDicFolder;
    }

    public String getStrPathGraphFile() {
        return strPathGraphFile;
    }

    public String getStrPathDicFile() {
        return strPathDicFile;
    }

    public String getText() {
        return text;
    }

    public int getCounter() {
        return this.counter;
    }
}
