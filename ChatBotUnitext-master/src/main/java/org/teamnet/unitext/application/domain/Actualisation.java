package org.teamnet.unitext.application.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "actualisation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
public class Actualisation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "identifiant")
    String identifiant;

    @Column(name = "predicat")
    String predicat;

    @Column(name = "x0")
    String x0;

    @Column(name = "x1")
    String x1;

    @Column(name = "x2")
    String x2;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public String getPredicat() {
        return predicat;
    }

    public void setPredicat(String predicat) {
        this.predicat = predicat;
    }

    public String getX0() {
        return x0;
    }

    public void setX0(String x0) {
        this.x0 = x0;
    }

    public String getX1() {
        return x1;
    }

    public void setX1(String x1) {
        this.x1 = x1;
    }

    public String getX2() {
        return x2;
    }

    public void setX2(String x2) {
        this.x2 = x2;
    }

    public String[] getAxisAsArray() {

        String[] axis = new String[3];
        axis[0] = null;
        axis[1] = null;
        axis[2] = null;

        if (this.x0 != null) {// 6
            axis[0] = this.x0;
        }
        if (this.x1 != null)// 7
            axis[1] = this.x1;
        if (this.x2 != null)// 8
            axis[2] = this.x2;

        return axis;
    }

}
