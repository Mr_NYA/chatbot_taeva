package org.teamnet.unitext.application.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "nom_compose")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
public class ArgumentCompose implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    @Column(name = "forme")
    String forme;

    @Column(name = "lemme_id")
    String lemmeId;

    @Lob
    @Column(name = "lemme")
    String lemme;

    @Column(name = "catgram")
    String catgram;

    @Column(name = "gender")
    String gender;

    @Column(name = "number")
    String num;

    @Column(name = "person")
    String person;

    @Column(name = "tense")
    String tense;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFo() {
        return forme;
    }

    public void setFo(String fo) {
        this.forme = fo;
    }

    public String getLeId() {
        return lemmeId;
    }

    public void setLeId(String leId) {
        this.lemmeId = leId;
    }

    public String getLe() {
        return lemme;
    }

    public void setLe(String le) {
        this.lemme = le;
    }

    public String getCa() {
        return catgram;
    }

    public void setCa(String ca) {
        this.catgram = ca;
    }

    public String getGe() {
        return gender;
    }

    public void setGe(String ge) {
        this.gender = ge;
    }

    public String getNu() {
        return num;
    }

    public void setNu(String nu) {
        this.num = nu;
    }

    public String getPe() {
        return person;
    }

    public void setPe(String pe) {
        this.person = pe;
    }

    public String getTe() {
        return tense;
    }

    public void setTe(String te) {
        this.tense = te;
    }

}
