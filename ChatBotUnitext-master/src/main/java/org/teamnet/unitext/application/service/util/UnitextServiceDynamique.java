package org.teamnet.unitext.application.service.util;

import fr.umlv.unitex.jni.UnitexJni;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UnitextServiceDynamique implements Runnable {

    private String strPathWorspace =
        System.getProperty("user.home") + "/workspace";

    private String pathUnitexWS =
        strPathWorspace + "/Unitex-GramLab/Unitex/French";

    private String strPathGraph = pathUnitexWS + "/Graphs";

    private static Logger logger = Logger.getLogger("UnitextService");

    private File corpusTXT;
    private File normalizeOut;
    private File directorySNT;
    private File outputFileDirectory;
    private File tokenizeOut;
    private File outputFile;
    private File AlphabetSort;
    private File corpusSNT;
    private File concordInd;
    private File tmpFolder;
    private File graphe;
    private List<File> dela;

    public UnitextServiceDynamique(
        File tmpFolder,
        File graphe,
        List<File> dela,
        File corpusTXT
    ) {
        this.tmpFolder = tmpFolder;
        this.graphe = graphe;
        this.corpusTXT = corpusTXT;
        this.dela = dela;

        UnitexJni.setStdOutTrashMode(true);
        UnitexJni.setStdErrTrashMode(true);
    }

    public void run() {
        try {
            this.init();
        } catch (IOException e) {}
    }

    private void Grf2Fst2() {
        logger.log(
            Level.WARNING,
            "2  -----------------\n {0}",
            (
                "UnitexTool Grf2Fst2 \"" +
                strPathGraph +
                "/Preprocessing/Sentence/Sentence.grf\" -y \"--alphabet=" +
                pathUnitexWS +
                "/Alphabet.txt\" -d \"" +
                strPathGraph +
                "/OUTIL" +
                "\"  -qutf8-no-bom"
            )
        );

        UnitexJni.execUnitexTool(
            "UnitexTool Grf2Fst2 \"" +
            strPathGraph +
            "/Preprocessing/Sentence/Sentence.grf\" -y \"--alphabet=" +
            pathUnitexWS +
            "/Alphabet.txt\" -d \"" +
            strPathGraph +
            "/OUTIL" +
            "\"  -qutf8-no-bom"
        );
    }

    private void Flatten() {
        logger.log(
            Level.WARNING,
            "3  -----------------\n {0}",
            (
                "UnitexTool Flatten \"" +
                strPathGraph +
                "/Preprocessing/Sentence/Sentence.fst2\" --rtn -d5 -qutf8-no-bom"
            )
        );

        UnitexJni.execUnitexTool(
            "UnitexTool Flatten \"" +
            strPathGraph +
            "/Preprocessing/Sentence/Sentence.fst2\" --rtn -d5 -qutf8-no-bom"
        );
    }

    private void Fst2Txt() {
        logger.log(
            Level.WARNING,
            "4  -----------------\n {0}",
            (
                "UnitexTool Fst2Txt \"-t" +
                corpusSNT.getAbsolutePath() +
                "\" \"" +
                strPathGraph +
                "/Preprocessing/Sentence/Sentence.fst2\" \"-a" +
                pathUnitexWS +
                "/Alphabet.txt\" -M \"--input_offsets=" +
                normalizeOut.getAbsolutePath() +
                "\" \"--output_offsets=" +
                normalizeOut.getAbsolutePath() +
                "\" -qutf8-no-bom"
            )
        );

        UnitexJni.execUnitexTool(
            "UnitexTool Fst2Txt \"-t" +
            corpusSNT.getAbsolutePath() +
            "\" \"" +
            strPathGraph +
            "/Preprocessing/Sentence/Sentence.fst2\" \"-a" +
            pathUnitexWS +
            "/Alphabet.txt\" -M \"--input_offsets=" +
            normalizeOut.getAbsolutePath() +
            "\" \"--output_offsets=" +
            normalizeOut.getAbsolutePath() +
            "\" -qutf8-no-bom"
        );
    }

    private void Grf2Fst2Etape2() {
        logger.log(
            Level.WARNING,
            "5  -----------------\n {0}",
            (
                "UnitexTool Grf2Fst2 \"" +
                strPathGraph +
                "/Preprocessing/Replace/Replace.grf\" -y \"--alphabet=" +
                pathUnitexWS +
                "/Alphabet.txt\" -d \"" +
                strPathGraph +
                "/OUTIL" +
                "\" -qutf8-no-bom"
            )
        );

        UnitexJni.execUnitexTool(
            "UnitexTool Grf2Fst2 \"" +
            strPathGraph +
            "/Preprocessing/Replace/Replace.grf\" -y \"--alphabet=" +
            pathUnitexWS +
            "/Alphabet.txt\" -d \"" +
            strPathGraph +
            "/OUTIL" +
            "\" -qutf8-no-bom"
        );
    }

    private void Fst2TxtEtape2() {
        logger.log(
            Level.WARNING,
            "6  -----------------\n {0}",
            (
                "UnitexTool Fst2Txt \"-t" +
                corpusSNT.getAbsolutePath() +
                "\" \"" +
                strPathGraph +
                "/Preprocessing/Replace/Replace.fst2\" \"-a" +
                pathUnitexWS +
                "/Alphabet.txt\" -R \"--input_offsets=" +
                normalizeOut.getAbsolutePath() +
                "\" \"--output_offsets=" +
                normalizeOut.getAbsolutePath() +
                "\" -qutf8-no-bom"
            )
        );

        UnitexJni.execUnitexTool(
            "UnitexTool Fst2Txt \"-t" +
            corpusSNT.getAbsolutePath() +
            "\" \"" +
            strPathGraph +
            "/Preprocessing/Replace/Replace.fst2\" \"-a" +
            pathUnitexWS +
            "/Alphabet.txt\" -R \"--input_offsets=" +
            normalizeOut.getAbsolutePath() +
            "\" \"--output_offsets=" +
            normalizeOut.getAbsolutePath() +
            "\" -qutf8-no-bom"
        );
    }

    private void Tokenize() {
        logger.log(
            Level.WARNING,
            "7  -----------------\n {0}",
            (
                "UnitexTool Tokenize \"" +
                corpusSNT.getAbsolutePath() +
                "\" \"-a" +
                pathUnitexWS +
                "/Alphabet.txt\" \"--input_offsets=" +
                normalizeOut.getAbsolutePath() +
                "\" \"--output_offsets=" +
                tokenizeOut.getAbsolutePath() +
                "\" -qutf8-no-bom"
            )
        );

        UnitexJni.execUnitexTool(
            "UnitexTool Tokenize \"" +
            corpusSNT.getAbsolutePath() +
            "\" \"-a" +
            pathUnitexWS +
            "/Alphabet.txt\" \"--input_offsets=" +
            normalizeOut.getAbsolutePath() +
            "\" \"--output_offsets=" +
            tokenizeOut.getAbsolutePath() +
            "\" -qutf8-no-bom"
        );
    }

    private void Dico(List<File> dela) {
        String strAllDico = "";

        for (File f : dela) {
            strAllDico += "\"-m" + f.getAbsolutePath() + "\" ";
            // strAllDico += "\"-m" + f.getParent() + "/fruit.bin\" ";
            strAllDico += "\"" + f.getAbsolutePath() + "\" ";
        }

        logger.log(
            Level.WARNING,
            "8  -----------------\n {0}",
            (
                "UnitexTool Dico \"-t" +
                corpusSNT.getAbsolutePath() +
                "\" \"-a" +
                pathUnitexWS +
                "/Alphabet.txt\" " +
                strAllDico +
                "-qutf8-no-bom"
            )
        );

        UnitexJni.execUnitexTool(
            "UnitexTool Dico \"-t" +
            corpusSNT.getAbsolutePath() +
            "\" \"-a" +
            pathUnitexWS +
            "/Alphabet.txt\" " +
            strAllDico +
            "-qutf8-no-bom"
        );
    }

    public void SortTxt() {
        logger.log(
            Level.WARNING,
            "9  -----------------\n {0}",
            (
                "UnitexTool SortTxt \"" +
                directorySNT.getAbsolutePath() +
                "/dlf\" \"-l" +
                directorySNT.getAbsolutePath() +
                "/dlf.n\" \"-o" +
                AlphabetSort.getAbsolutePath() +
                "\" -qutf8-no-bom"
            )
        );

        UnitexJni.execUnitexTool(
            "UnitexTool SortTxt \"" +
            directorySNT.getAbsolutePath() +
            "/dlf\" \"-l" +
            directorySNT.getAbsolutePath() +
            "/dlf.n\" \"-o" +
            AlphabetSort.getAbsolutePath() +
            "\" -qutf8-no-bom"
        );

        logger.log(
            Level.WARNING,
            "10  -----------------\n {0}",
            (
                "UnitexTool SortTxt \"" +
                directorySNT.getAbsolutePath() +
                "/dlc\" \"-l" +
                directorySNT.getAbsolutePath() +
                "/dlc.n\" \"-o" +
                AlphabetSort.getAbsolutePath() +
                "\" -qutf8-no-bom"
            )
        );

        UnitexJni.execUnitexTool(
            "UnitexTool SortTxt \"" +
            directorySNT.getAbsolutePath() +
            "/dlc\" \"-l" +
            directorySNT.getAbsolutePath() +
            "/dlc.n\" \"-o" +
            AlphabetSort.getAbsolutePath() +
            "\" -qutf8-no-bom"
        );

        logger.log(
            Level.WARNING,
            "11  -----------------\n {0}",
            (
                "UnitexTool SortTxt \"" +
                directorySNT.getPath() +
                "/err\" \"-l" +
                directorySNT.getPath() +
                "/err.n\" \"-o" +
                AlphabetSort.getAbsolutePath() +
                "\" -qutf8-no-bom"
            )
        );

        UnitexJni.execUnitexTool(
            "UnitexTool SortTxt \"" +
            directorySNT.getPath() +
            "/err\" \"-l" +
            directorySNT.getPath() +
            "/err.n\" \"-o" +
            AlphabetSort.getAbsolutePath() +
            "\" -qutf8-no-bom"
        );

        logger.log(
            Level.WARNING,
            "12  -----------------\n {0}",
            (
                "UnitexTool SortTxt \"" +
                directorySNT.getAbsolutePath() +
                "/tags_err\" \"-l" +
                directorySNT.getAbsolutePath() +
                "/tags_err.n\" \"-o" +
                AlphabetSort.getAbsolutePath() +
                "\" -qutf8-no-bom"
            )
        );

        UnitexJni.execUnitexTool(
            "UnitexTool SortTxt \"" +
            directorySNT.getAbsolutePath() +
            "/tags_err\" \"-l" +
            directorySNT.getAbsolutePath() +
            "/tags_err.n\" \"-o" +
            AlphabetSort.getAbsolutePath() +
            "\" -qutf8-no-bom"
        );
    }

    private void Locate(List<File> dela) {
        String strAllDico = "";
        for (File f : dela) {
            strAllDico += "\"-m" + f.getAbsolutePath() + "\" ";
        }

        logger.log(
            Level.WARNING,
            "13  -----------------\n {0}",
            (
                "UnitexTool Locate \"-t" +
                corpusSNT.getAbsolutePath() +
                "\" \"" +
                graphe.getAbsolutePath() +
                "\" \"-a" +
                pathUnitexWS +
                "/Alphabet.txt\" -L -R --all " +
                strAllDico +
                "-b -Y --stack_max=1000 --max_matches_per_subgraph=200 --max_matches_at_token_pos=400 --max_errors=50 -qutf8-no-bom"
            )
        );

        UnitexJni.execUnitexTool(
            "UnitexTool Locate \"-t" +
            corpusSNT.getAbsolutePath() +
            "\" \"" +
            graphe.getAbsolutePath() +
            "\" \"-a" +
            pathUnitexWS +
            "/Alphabet.txt\" -L -R --all " +
            strAllDico +
            "-b -Y --stack_max=1000 --max_matches_per_subgraph=200 --max_matches_at_token_pos=400 --max_errors=50 -qutf8-no-bom"
        );
    }

    public void createFiles() throws IOException {
        //etape1
        directorySNT = new File(tmpFolder.getAbsoluteFile() + "/corpusML_snt");
        directorySNT.mkdir();

        // tmpFolder.deleteOnExit();

        normalizeOut =
            new File(
                tmpFolder.getAbsoluteFile() +
                "/corpusML_snt/normalize.out.offsets"
            );

        // normalizeOut.createNewFile();

        tokenizeOut =
            new File(
                tmpFolder.getAbsoluteFile() +
                "/corpusML_snt/tokenize.out.offsets"
            );

        // tokenizeOut.createNewFile();

        // File errOut = new File(tmpFolder.getAbsoluteFile() + "/err.n.offsets");
        // errOut.createNewFile();

        outputFile =
            new File(tmpFolder.getAbsoluteFile() + "/resultat_corpusML.txt");

        // outputFile.createNewFile();

        corpusSNT = new File(tmpFolder.getAbsoluteFile() + "/corpusML.snt");
        // corpusSNT.createNewFile();

        AlphabetSort =
            new File(tmpFolder.getAbsoluteFile() + "/Alphabet_sort.txt");
        // AlphabetSort.createNewFile();
    }

    private void Concord() throws IOException {
        concordInd = new File(directorySNT.getAbsoluteFile() + "/concord.ind");
        // if (!concordInd.exists()) {
        //     concordInd.createNewFile();
        // }

        logger.log(
            Level.WARNING,
            "14  -----------------\n {0}",
            (
                "UnitexTool Concord \"" +
                concordInd.getAbsolutePath() +
                "\" \"-m" +
                outputFile.getAbsolutePath() +
                "\" -qutf8-no-bom"
            )
        );

        UnitexJni.execUnitexTool(
            "UnitexTool Concord \"" +
            concordInd.getAbsolutePath() +
            "\" \"-m" +
            outputFile.getAbsolutePath() +
            "\" -qutf8-no-bom"
        );
    }

    private void NormalizeEtape2() {
        logger.log(
            Level.WARNING,
            "15  -----------------\n {0}",
            (
                "UnitexTool Normalize \"" +
                outputFile.getAbsolutePath() +
                "\" \"-r" +
                pathUnitexWS +
                "/Norm.txt\" -qutf8-no-bom"
            )
        );

        UnitexJni.execUnitexTool(
            "UnitexTool Normalize \"" +
            outputFile.getAbsolutePath() +
            "\" \"-r" +
            pathUnitexWS +
            "/Norm.txt\" -qutf8-no-bom"
        );
    }

    public void init() throws IOException {
        createFiles();
        normalize();
        Grf2Fst2();

        Flatten();
        Fst2Txt();

        Grf2Fst2Etape2();

        Fst2TxtEtape2();

        Tokenize();

        Dico(dela);

        SortTxt();
        Locate(dela);
        // moveFiles();

        Concord();
        NormalizeEtape2();

        outputFileDirectory =
            new File(tmpFolder.getAbsoluteFile() + "/resultat_corpusML_snt");
        outputFileDirectory.mkdir();

        try {
            TokenizeFinal();
        } catch (OutOfMemoryError e) {}
    }

    private void TokenizeFinal() throws IOException {
        logger.log(
            Level.WARNING,
            "16  -----------------\n {0}",
            (
                "UnitexTool Tokenize \"" +
                outputFile.getAbsolutePath() +
                "\" \"-a" +
                pathUnitexWS +
                "/Alphabet.txt\" -qutf8-no-bom"
            )
        );

        UnitexJni.execUnitexTool(
            "UnitexTool Tokenize \"" +
            outputFile.getAbsolutePath() +
            "\" \"-a" +
            pathUnitexWS +
            "/Alphabet.txt\" -qutf8-no-bom"
        );

        logger.log(
            Level.WARNING,
            "17  -----------------\n {0}",
            "created end fin"
        );

        File finTraitement = new File(tmpFolder.getAbsolutePath() + "/fin.end");
        finTraitement.createNewFile();
    }

    private void normalize() {
        logger.log(
            Level.WARNING,
            "1  -----------------\n {0}",
            (
                "UnitexTool Normalize \"" +
                corpusTXT.getAbsolutePath() +
                "\" \"-r" +
                pathUnitexWS +
                "/Norm.txt\" \"--output_offsets=" +
                normalizeOut.getAbsolutePath() +
                "\" -qutf8-no-bom"
            )
        );

        UnitexJni.execUnitexTool(
            "UnitexTool Normalize \"" +
            corpusTXT.getAbsolutePath() +
            "\" \"-r" +
            pathUnitexWS +
            "/Norm.txt\" \"--output_offsets=" +
            normalizeOut.getAbsolutePath() +
            "\" -qutf8-no-bom"
        );
    }
}
