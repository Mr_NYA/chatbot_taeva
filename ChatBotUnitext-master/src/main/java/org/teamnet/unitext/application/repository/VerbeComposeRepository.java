package org.teamnet.unitext.application.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.teamnet.unitext.application.domain.VerbeCompose;


@Repository
public interface VerbeComposeRepository extends JpaRepository<VerbeCompose, Long> {

    VerbeCompose findOneByForm(String forme);
    VerbeCompose[] findByForm(String forme);

    VerbeCompose findOneByLemAndTen(String lemme, String tense);
    VerbeCompose[] findByLemAndTen(String lemme, String tense);

    VerbeCompose[] findByLemAndTenAndNumAndPer(String lemme, String tense, String number, String person);

}


