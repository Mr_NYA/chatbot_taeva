package org.teamnet.unitext.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.teamnet.unitext.application.domain.Formes;

@Repository
public interface FormesRepository extends JpaRepository<Formes, Long> {

    Formes[] findByFormeAndCatgram(String forme, String catgram);
    Formes[] findByForme(String forme);
}
