package org.teamnet.unitext.application.service.dto;


import org.teamnet.unitext.application.domain.QASenior;

public class QASeniorDTO {


    private Long id;

    String inputMessage;
    String typeMessage;
    String response;
    String action;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInputMessage() {
        return this.inputMessage;
    }

    public void setInputMessage(String inputMessage) {
        this.inputMessage = inputMessage;
    }

    public String getTypeMessage() {
        return this.typeMessage;
    }

    public void setTypeMessage(String typeMessage) {
        this.typeMessage = typeMessage;
    }

    public String getResponse() {
        return this.response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getAction() {
        return this.action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public QASeniorDTO(QASenior qaSenior)
    {
        this.inputMessage = qaSenior.getInputMessage();
        this.typeMessage = qaSenior.getTypeMessage();
        this.response = qaSenior.getResponse();
        this.action = qaSenior.getAction();

    }

}
