package org.teamnet.unitext.application.web.rest;

public class MessageChat {

    private String that;

    public String getThat() {
        return that;
    }

    public void setThat(String that) {
        this.that = that;
    }

    public MessageChat(String value) {
        this.that = value;
    }

}
