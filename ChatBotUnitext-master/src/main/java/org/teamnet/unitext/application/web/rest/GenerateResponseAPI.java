package org.teamnet.unitext.application.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.teamnet.unitext.application.domain.beans.ReponseChatbot;
import org.teamnet.unitext.application.service.util.ConstructionInfo;
import org.teamnet.unitext.application.service.util.GenerateTextService;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Resource to return information about the currently running Spring profiles.
 */
@RestController
@RequestMapping("/api")
public class GenerateResponseAPI {

    private static Logger logger = Logger.getLogger("GenerateResponseAPI");

    @Autowired
    GenerateTextService generateTextService;

    @PostMapping(value = "/GenerateResponseAPI")
    public MessageChat generateResponse(@RequestBody Message json) {
        try {
            String messageDecoded = java.net.URLDecoder.decode(json.message, "UTF-8");
            String response = generateTextService.generateResponse(messageDecoded, new ReponseChatbot(""),
                    new ConstructionInfo(), true).reponse;
            return new MessageChat(response);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "An error has occured: {0}", e.getLocalizedMessage());
            return new MessageChat("Une erreur est survenue.");
        }
    }
}
