package org.teamnet.unitext.application.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.teamnet.unitext.application.domain.Client;

import java.util.ArrayList;


@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    Client findOneByNom(String nom);

    Client findOneByToken(String token);

    ArrayList<Client> findAll();
}


