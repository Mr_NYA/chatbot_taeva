package org.teamnet.unitext.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.teamnet.unitext.application.domain.Actualisation;

import java.util.List;

@Repository
public interface ActualisationRepository extends JpaRepository<Actualisation, Long> {

    List<Actualisation> findAll();
}
