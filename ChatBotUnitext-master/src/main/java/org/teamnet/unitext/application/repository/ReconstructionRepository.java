package org.teamnet.unitext.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.teamnet.unitext.application.domain.Reconstruction;

public interface ReconstructionRepository extends JpaRepository<Reconstruction, Long> {
    Reconstruction findOneById(int id);
}
