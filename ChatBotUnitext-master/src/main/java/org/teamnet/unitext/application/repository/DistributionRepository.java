package org.teamnet.unitext.application.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.teamnet.unitext.application.domain.Distribution;

@Repository
public interface DistributionRepository extends JpaRepository<Distribution, Long> {

}
