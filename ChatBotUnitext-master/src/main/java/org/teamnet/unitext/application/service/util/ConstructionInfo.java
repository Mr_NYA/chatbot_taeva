package org.teamnet.unitext.application.service.util;

import java.util.ArrayList;
import java.util.List;

public class ConstructionInfo {
    private List<String> noms;
    private List<String> adjectifs;
    private List<String> conjugaisons;
    private List<String> verbesConj;
    private List<String> dets;
    private List<String> actus;

    public List<String> getNoms() {
        return noms;
    }

    public void setNoms(List<String> noms) {
        this.noms = noms;
    }

    public List<String> getAdjectifs() {
        return adjectifs;
    }

    public void setAdjectifs(List<String> adjectifs) {
        this.adjectifs = adjectifs;
    }

    public List<String> getConjugaisons() {
        return conjugaisons;
    }

    public void setConjugaisons(List<String> conjugaisons) {
        this.conjugaisons = conjugaisons;
    }

    public List<String> getVerbesConj() {
        return verbesConj;
    }

    public void setVerbesConj(List<String> verbesConj) {
        this.verbesConj = verbesConj;
    }

    public List<String> getDets() {
        return dets;
    }

    public void setDets(List<String> dets) {
        this.dets = dets;
    }

    public List<String> getActus() {
        return actus;
    }

    public void setActus(List<String> actus) {
        this.actus = actus;
    }

    public ConstructionInfo() {
        noms = new ArrayList<>();
        adjectifs = new ArrayList<>();
        dets = new ArrayList<>();
        conjugaisons = new ArrayList<>();
        verbesConj = new ArrayList<>();
        actus = new ArrayList<>();
    }

}
