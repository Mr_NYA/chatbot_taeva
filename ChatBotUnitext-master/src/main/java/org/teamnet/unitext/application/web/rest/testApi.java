package org.teamnet.unitext.application.web.rest;

import io.github.jhipster.config.JHipsterProperties;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;
import org.teamnet.unitext.application.service.util.testClass;

/**
 * Resource to return information about the currently running Spring profiles.
 */
@RestController
@RequestMapping("/api")
public class testApi{
    private int i;


    @PostMapping("/testApi")
    public String testApiFunction(@RequestBody String message) {
        String resultat;

        i++;
        String iString = "";
        iString = Integer.toString(i);
        try {
            resultat = testClass.process(message);
            //resultat = "Successfully entered API for the " + iString + "times.\n also, you wrote this : " + message;
        } catch (Exception e) {
            return "Error getting API";
        }
        return resultat;


    }
}
