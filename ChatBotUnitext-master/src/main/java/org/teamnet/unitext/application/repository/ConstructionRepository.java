package org.teamnet.unitext.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.teamnet.unitext.application.domain.Construction;

@Repository
public interface ConstructionRepository extends JpaRepository<Construction, Long> {
    Construction findOneById(Long id);
}
