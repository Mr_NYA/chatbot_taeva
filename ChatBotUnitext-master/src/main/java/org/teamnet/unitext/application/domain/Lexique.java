package org.teamnet.unitext.application.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "lexique")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
public class Lexique implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "categorie")
    String categorie;

    @Column(name = "predicat")
    String predicat;

    @Column(name = "variable")
    String variable;

    @Column(name = "valeur")
    String valeur;

    @Column(name = "type")
    String types;

    @ManyToOne
    @JoinColumn(name = "construction_id", referencedColumnName = "id")
    private Construction construction;

    @ManyToOne
    @JoinColumn(name = "actualisation_id", referencedColumnName = "id")
    private Actualisation actualisation;

    @ManyToOne
    @JoinColumn(name = "distribution_id", referencedColumnName = "id")
    private Distribution distribution;

    @Column(name = "famille")
    String famille;

    public void setFamille(String famille) {
        this.famille = famille;
    }

    public String getFamille() {
        return this.famille;
    }

    public String[] getFamilleAsElements() {
        if (this.famille == null) {
            return null;
        }
        return this.famille.split(",");
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getPredicat() {
        return predicat;
    }

    public void setPredicat(String predicat) {
        this.predicat = predicat;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Construction getConstruction() {
        return construction;
    }

    public void setConstruction(Construction construction) {
        this.construction = construction;
    }

    public Actualisation getActualisation() {
        return actualisation;
    }

    public void setActualisation(Actualisation actualisation) {
        this.actualisation = actualisation;
    }

    public Distribution getDistribution() {
        return distribution;
    }

    public void setDistribution(Distribution distribution) {
        this.distribution = distribution;
    }

}
