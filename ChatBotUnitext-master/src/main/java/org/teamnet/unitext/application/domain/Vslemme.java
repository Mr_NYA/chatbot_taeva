package org.teamnet.unitext.application.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

@Table(name = "vslemmes")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
public class Vslemme {
    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Column(name = "Lemme")
    String lemme;

    @Column(name = "catgram")
    String catgram;

    @ManyToOne
    @JoinColumn(name = "Flex", referencedColumnName = "Code")
    Vscode vscode;

    @Column(name = "Lig")
    String lig;

    @Column(name = "Standard")
    String standard;

    @Column(name = "Notes")
    String notes;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLemme() {
        return lemme;
    }

    public void setLemme(String lemme) {
        this.lemme = lemme;
    }

    public String getCatgram() {
        return catgram;
    }

    public void setCatgram(String catgram) {
        this.catgram = catgram;
    }

    public Vscode getVscode() {
        return vscode;
    }

    public void setVscode(Vscode vscode) {
        this.vscode = vscode;
    }

    public String getLig() {
        return lig;
    }

    public void setLig(String lig) {
        this.lig = lig;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
