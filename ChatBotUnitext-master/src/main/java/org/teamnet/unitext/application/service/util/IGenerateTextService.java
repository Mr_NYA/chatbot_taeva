package org.teamnet.unitext.application.service.util;

import java.util.ArrayList;

public interface IGenerateTextService {
    void action(ArrayList<String> phrase, String toReplace);
}
