package org.teamnet.unitext.application.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Table(name = "regle_ajustement_morphologique", uniqueConstraints = @UniqueConstraint(columnNames = { "code",
        "regle" }))
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
public class RegleAjustementMorphologique extends AjustementDynamique implements Serializable {

    private static final long serialVersionUID = 1L;

}
