package org.teamnet.unitext.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.teamnet.unitext.application.domain.Deixis;

@Repository
public interface DeixisRepository extends JpaRepository<Deixis, Long> {

    Deixis[] findByClasse(String classe);

    Deixis findOneByValeur(String valeur);
}
