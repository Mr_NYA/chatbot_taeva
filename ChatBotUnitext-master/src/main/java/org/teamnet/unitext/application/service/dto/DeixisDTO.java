package org.teamnet.unitext.application.service.dto;

public class DeixisDTO {
    private Long id;

    String deixis;
    String pronom;
    String type;
    String conjugaison;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeixis() {
        return deixis;
    }

    public void setDeixis(String deixis) {
        this.deixis = deixis;
    }

    public String getPronom() {
        return pronom;
    }

    public void setPronom(String pronom) {
        this.pronom = pronom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getConjugaison() {
        return conjugaison;
    }

    public void setConjugaison(String conjugaison) {
        this.conjugaison = conjugaison;
    }
}
