package org.teamnet.unitext.application.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "formes")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
public class Formes implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long formeId;

    @Column(name = "forme")
    String forme;

    @Column(name = "lemme_id")
    String lemmeId;

    @Column(name = "lemme")
    String lemme;

    @Column(name = "catgram")
    String catgram;

    @Column(name = "gender")
    String gender;

    @Column(name = "number")
    String number;

    @Column(name = "person")
    String person;

    @Column(name = "tense")
    String tense;

    public Long getFormeId() {
        return formeId;
    }

    public void setFormeId(Long formeId) {
        this.formeId = formeId;
    }

    public String getLemmeId() {
        return lemmeId;
    }

    public void setLemmeId(String lemmeId) {
        this.lemmeId = lemmeId;
    }

    public String getForme() {
        return forme;
    }

    public void setForme(String forme) {
        this.forme = forme;
    }

    public String getLemme() {
        return lemme;
    }

    public void setLemme(String lemme) {
        this.lemme = lemme;
    }

    public String getCatgram() {
        return catgram;
    }

    public void setCatgram(String catgram) {
        this.catgram = catgram;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getTense() {
        return tense;
    }

    public void setTense(String tense) {
        this.tense = tense;
    }

}
