package org.teamnet.unitext.application.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.teamnet.unitext.application.domain.Ontologie;



@Repository
public interface OntologieRepository extends JpaRepository<Ontologie, Long> {

    Ontologie findOneByCode(String code);

    Ontologie findOneByValeur(String valeur);

}


