package org.teamnet.unitext.application.service.util;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;

@Service
public class PragmatiqueService {

    private static String APIKeyMeteo = "20670b68f0e7c60309fde8fd0e316356";
    private static java.util.logging.Logger logger = java.util.logging.Logger.getLogger("PragmatiqueService");

    private Map<String, String> meteos;

    public PragmatiqueService() {
        meteos = new HashMap<>();
        meteos.put("pluie","rain");
        meteos.put("nuage", "cloud");
        meteos.put("soleil", "sun");
        meteos.put("beau temps", "clear-day");
        meteos.put("brume", "fog");
        meteos.put("neige", "snow");
        meteos.put("brouillard", "fog");
    }

    public String getMeteo(){
        logger.log(Level.FINE, "Send request to Weather API");
        String strLat =  "48.846362199999994";
        String strLon =  "2.3772889999999998";
        String summary = "";
        final String uri = "https://api.darksky.net/forecast/"+APIKeyMeteo+"/"+strLat+","+strLon+"?lang=fr";

        try
        {
            RestTemplate restTemplate = new RestTemplate();
            String result = restTemplate.getForObject(uri, String.class);
            JSONObject jsonObj = new JSONObject(result);
            JSONObject hourly = jsonObj.getJSONObject("hourly");
            summary = hourly.getString("icon");
        }
        catch(JSONException e)
        {
            logger.info(e.getLocalizedMessage());
        }
        return summary;
    }

    public String getMeteoFromMap(String summary)
    {

        logger.log(Level.FINE, "Getting Meteo from Map");
        ArrayList<String> to_ret = new ArrayList<>();
        for (Map.Entry<String, String> entry : meteos.entrySet())
        {
            if (summary.contains(entry.getValue()))
                to_ret.add(entry.getKey());
        }

       return to_ret.get(new Random().nextInt(to_ret.size())).replaceAll(" ", "_");
    }

    public boolean checkMeteo(String summary, String indexMeteo)
    {
        logger.log(Level.FINE, "verifying meteo against map");
        if (indexMeteo == null)
            return false;
        if (meteos.get(indexMeteo) == null)
            return false;
        return summary.contains(meteos.get(indexMeteo));
    }


}
