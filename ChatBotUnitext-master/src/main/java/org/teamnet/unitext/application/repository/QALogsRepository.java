package org.teamnet.unitext.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.teamnet.unitext.application.domain.QALogs;

@Repository
public interface QALogsRepository extends JpaRepository<QALogs, Long> {

    QALogs findOneByInputMessage(String inputMessage);

}
