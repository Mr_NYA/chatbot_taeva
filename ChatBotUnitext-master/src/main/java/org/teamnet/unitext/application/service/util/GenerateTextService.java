package org.teamnet.unitext.application.service.util;

import static java.lang.Math.toIntExact;

import java.io.File;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teamnet.unitext.application.domain.*;
import org.teamnet.unitext.application.domain.beans.ReponseChatbot;
import org.teamnet.unitext.application.repository.*;
import org.teamnet.unitext.application.domain.Client;
import org.teamnet.unitext.application.repository.ClientRepository;

import javax.annotation.Nullable;

@Service
public class GenerateTextService {

    private static java.util.logging.Logger logger = java.util.logging.Logger.getLogger("GenerateTextService");

    @Autowired
    private LexiqueRepository lexiqueRepository;

    @Autowired
    private VslemmeRepository vslemmeRepository;

    @Autowired
    private NslemmeRepository nslemmeRepository;

    @Autowired
    private AlemmeRepository alemmeRepository;

    @Autowired
    private PragmatiqueService pragmatiqueService;

    @Autowired
    private DeixisRepository deixisRepository;

    @Autowired
    private FormesRepository formesRepository;

    @Autowired
    private OntologieRepository ontologieRepository;

    @Autowired
    private ArgumentComposeRepository argumentComposeRepository;

    @Autowired
    private VerbeComposeRepository verbeComposeRepository;

    @Autowired
    private RegleAjustementMorphologiqueRepository regleAjustementMorphologiqueRepository;

    @Autowired
    private RegleAjustementSyntaxiqueRepository regleAjustementSyntaxiqueRepository;

    @Autowired
    private DeterminantRepository determinantRepository;

    private Map<String, Object> etre;
    static final String AVOIR = "avoir";
    private Set<String> hNonAspire;
    Formes[] formsSpecial = null;
    String intentReponse;
    public  int indexLexique = 0;
    public ArrayList<Long> listDeixisID = new ArrayList<Long>();
    public Deixis selectedDeixis;
    public int randomDeixisX0 = -1;
    public int randomDeixisX1 = -1;
    public int randomDeixisX2 = -1;


    public GenerateTextService() {
        logger.log(Level.FINE, "GenerateTextService: -*-*-*-*-*-*-*-*-*-*-*-*-*");
        etre = new HashMap<>();
        if (hNonAspire == null) {
            try {
                URI uri = new File(System.getProperty("user.home") + "/workspace/files/h_aspire.txt").toURI();
                hNonAspire = new HashSet<>(Files.readAllLines(Paths.get(uri)));
            } catch (Exception e) {
                logger.log(Level.WARNING, "H ASPIRE FILE NOT FOUND: {0}");
            }
        }

        etre.put("code", "114");
        etre.put("modele", "être");
        etre.put("rad", 4L);
        etre.put("r_Modele", "0");
        etre.put("inf__", "être");
        etre.put("ind_Pr_1_S", "suis");
        etre.put("ind_Pr_2_S", "es");
        etre.put("ind_Pr_3_S", "est");
        etre.put("ind_Pr_1_P", "sommes");
        etre.put("ind_Pr_2_P", "êtes");
        etre.put("ind_Pr_3_P", "sont");
        etre.put("ind_Imp_1_S", "étais");
        etre.put("ind_Imp_2_S", "étais");
        etre.put("ind_Imp_3_S", "était");
        etre.put("ind_Imp_1_P", "étions");
        etre.put("ind_Imp_2_P", "étiez");
        etre.put("ind_Imp_3_P", "étaient");
        etre.put("ind_Ps_1_S", "fus");
        etre.put("ind_Ps_2_S", "fus");
        etre.put("ind_Ps_3_S", "fut");
        etre.put("ind_Ps_1_P", "fûmes");
        etre.put("ind_Ps_2_P", "fûtes");
        etre.put("ind_Ps_3_P", "furent");
        etre.put("ind_Fut_1_S", "serai");
        etre.put("ind_Fut_2_S", "seras");
        etre.put("ind_Fut_3_S", "sera");
        etre.put("ind_Fut_1_P", "serons");
        etre.put("ind_Fut_2_P", "serez");
        etre.put("ind_Fut_3_P", "seront");
        etre.put("cond_Pr_1_S", "serais");
        etre.put("cond_Pr_2_S", "serais");
        etre.put("cond_Pr_3_S", "serait");
        etre.put("cond_Pr_1_P", "serions");
        etre.put("cond_Pr_2_P", "seriez");
        etre.put("cond_Pr_3_P", "seraient");
        etre.put("sub_Pr_1_S", "sois");
        etre.put("sub_Pr_2_S", "sois");
        etre.put("sub_Pr_3_S", "soit");
        etre.put("sub_Pr_1_P", "soyons");
        etre.put("sub_Pr_2_P", "soyez");
        etre.put("sub_Pr_3_P", "soient");
        etre.put("sub_Imp_1_S", "fusse");
        etre.put("sub_Imp_2_S", "fusses");
        etre.put("sub_Imp_3_S", "fût");
        etre.put("sub_Imp_1_P", "fussions");
        etre.put("sub_Imp_2_P", "fussiez");
        etre.put("sub_Imp_3_P", "fussent");
        etre.put("imp_Pr_2_S", "sois");
        etre.put("imp_Pr_1_P", "soyons");
        etre.put("imp_Pr_2_P", "soyez");
        etre.put("ppres__", "étant");
        etre.put("pp__S_M", "été");
        etre.put("pp__S_F", "-");
        etre.put("pp__P_M", "-");
        etre.put("pp__P_F", "-");
    }

    private Lexique[] getListLexiqueIfPredicatOrTag(String predicat, String tag) {
        logger.log(Level.FINE, "*** getListLexiqueIfPredicatOrTag");
        Lexique[] lexiques;
        if (predicat.equals("")) {// 5
            lexiques = lexiqueRepository.findByCategorie(tag);
        } else// 6
            lexiques = lexiqueRepository.findByPredicat(predicat);

        return lexiques;
    }

    boolean fromReconstruction = false;

    // Point d'entree du service, appelle toute les etapes de la generation de
    // phrase
    // Permet aussi de generer des phrases complexes grace a un appel recursif
    public ReponseChatbot generateResponse(String tag, ReponseChatbot constructionPrecedente,
            ConstructionInfo constInfo, boolean isSimple) {


        fromReconstruction = false;
        logger.log(Level.FINE, "generateResponse: *+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*");
        Lexique[] lexiques;
        ArrayList<String> phrase = new ArrayList<>();
        String predicat = "";
        String prevPredicat = "";

        logger.log(Level.FINE, "===============================================================>  0  <====================");
        if (tag.contains("))")) {// 1
            logger.log(Level.FINE, "===========================================================>  1  <====================");
            prevPredicat = tag.substring(tag.indexOf('(') + 1, tag.indexOf('(', tag.indexOf('(') + 1));
            String prevPredicatFull = tag.substring(tag.indexOf('(') + 1, tag.lastIndexOf(')'));
            // Appel recursif pour les constructions imbriquees
            // ex: PROPOSITION(MENU(X)) -> Envoie MENU dans generateResponse()
            constructionPrecedente = generateResponse(prevPredicatFull, new ReponseChatbot(""), constInfo, false);
            if (constructionPrecedente.reponse.toUpperCase().contains("ERREUR")) {// 2
                logger.log(Level.FINE, "=======================================================>  2  <====================");
                return constructionPrecedente;
            }
            tag = tag.replace(prevPredicatFull, "");
        }

        if (tag.contains("(")) {// 3
            String replace;

            logger.log(Level.FINE, "===========================================================>  3  <====================");
            replace = tag.substring(tag.indexOf('('), tag.indexOf(")")) + ")";
            tag = tag.replace(replace, "");
            String pred = replace.substring(1, replace.length() - 1);
            // AJUSTEMENT PRAGMATIQUE
            if (pred.equalsIgnoreCase("METEO")) {// 4

                logger.log(Level.FINE, "========================================================>  4  <====================");
                String summary = pragmatiqueService.getMeteo();
                String toResearch = pragmatiqueService.getMeteoFromMap(summary);

                predicat = pred + "_" + toResearch;
            }
        }

       if (tag.contains(",")) {// 5
           tag = tag.substring(tag.lastIndexOf(',') + 1);
           logger.log(Level.FINE, "============================================================>  5  <====================");
       }

        lexiques = getListLexiqueIfPredicatOrTag(predicat, tag);
        // lexiques == null || lexiques.length == 0
        if (isArrayNullOrVide(lexiques)) {// 6
            logger.log(Level.FINE, "===========================================================>  6  <====================");
            logger.log(Level.INFO, "Didn't find a categorie {0}", tag);
            logger.log(Level.INFO, "Trying with predicat {0}", predicat);

            lexiques = lexiqueRepository.findByPredicat(tag);
            if (isArrayNullOrVide(lexiques))// 7
            {

                logger.log(Level.FINE, "========================================================>  7  <====================");
                return new ReponseChatbot("Désolé, je n'ai pas de réponse. aucun lexique trouvé pour la categorie:"
                        + tag + " predicat:" + predicat, intentReponse,"aucun Lexique trouvé pour le tag #" + tag + "# sur la table lexique");

            }

        }

        logger.log(Level.FINE, "================================================================>  8  <====================");
        logger.log(Level.INFO, "=> USED TAG {0}", tag);

        // Choix aléatoire d'une ligne de la table (pour avoir une diversification des
        // reponses)
        int rand = ThreadLocalRandom.current().nextInt(0, lexiques.length);


        if(lexiques.length < 2){
            indexLexique = 0;
        }
        else {
            indexLexique =  (indexLexique + 1) % lexiques.length;
        }
        Lexique lexique = lexiques[indexLexique];
        Reconstruction reconstruction = isReconstruction(lexique);
        // Utilisation d'une reconstruction ou non

        Construction construction = lexique.getConstruction();


        String[] args = getArgs(reconstruction, lexique);

        WordLists wordlists = new WordLists();

        wordlists.setConjugaisons(new ArrayList<>(constInfo.getConjugaisons()));
        wordlists.setNoms(new ArrayList<>(constInfo.getNoms()));
        wordlists.setAdjectifs(new ArrayList<>(constInfo.getAdjectifs()));
        wordlists.setVerbesConj(new ArrayList<>(constInfo.getVerbesConj()));
        wordlists.setDets(new ArrayList<>(constInfo.getDets()));
        wordlists.setActus(new ArrayList<>(constInfo.getActus()));
        wordlists.setVerbeInf(new ArrayList<>());
        wordlists.setVerbes(new ArrayList<>());
        String temps = lexique.getActualisation().getPredicat();

        ArrayList<String> preconjugaisons = new ArrayList<>();
        // CONSTRUCTION

        String message = applyConstruction(phrase, args, construction, lexique, reconstruction, wordlists,
                preconjugaisons);
        logger.log(Level.INFO, "AFTER applyConstruction: {0}", phrase.toString());
        initConjugaison(preconjugaisons, wordlists, message);
        // ACTUALISATION
        message = actualisation(phrase, lexique, wordlists, message);

        logger.log(Level.INFO, "AFTER actualisation: {0}", phrase.toString());
        // INSTANCIATION
        message = replaceVariable(phrase, prevPredicat, message);
        logger.log(Level.INFO, "AFTER replaceVariable: {0}", phrase.toString());
        // CONJUGAISON
        message = conjugaison(phrase, lexique, temps, wordlists, message);

        if (message != null) {// 9

            logger.log(Level.FINE, "=========================================================>  9  <====================");
            return new ReponseChatbot(message + " [ID_LEXIQUE=" + lexique.getId() + " CATEGORIE="
                    + lexique.getCategorie() + "] [ID_CONSTRUCTION=" + construction.getIdentifiant()
                    + "] [CODE_DISTRUBUTION=" + lexique.getDistribution().getCode() + "]"
                    + getReconstructionERRORCodeOrVide(reconstruction), intentReponse, " [ID_LEXIQUE=" + lexique.getId() + " CATEGORIE="
                    + lexique.getCategorie() + "] [ID_CONSTRUCTION=" + construction.getIdentifiant()
                    + "] [CODE_DISTRUBUTION=" + lexique.getDistribution().getCode() + "]" );

        }
        logger.log(Level.FINE, "=============================================================>  10  <====================");

        logger.log(Level.INFO, "AFTER conjugaison: {0}", phrase.toString());
        // ACCORDS
        accord(phrase, wordlists);
        logger.log(Level.INFO, "AFTER accord: {0}", phrase.toString());

        ArrayList<String> prevConst = new ArrayList<>(Arrays.asList(constructionPrecedente.reponse.split("\\s+")));
        phrase.addAll(prevConst);
        logger.log(Level.INFO, "AFTER add constructionPrecedente: {0}", phrase.toString());

        // NEGATION
        if (fromReconstruction) {
            negation(phrase, wordlists);
            logger.log(Level.INFO, "AFTER negation: {0}", phrase.toString());

            logger.log(Level.FINE, "=======================================================>  11  <====================");
        }

        logger.log(Level.FINE, "===========================================================>  12  <====================");
        logger.log(Level.INFO, "* noms: {0}", wordlists.getNoms().toString());
        logger.log(Level.INFO, "* adjectifs: {0}", wordlists.getAdjectifs().toString());
        logger.log(Level.INFO, "* dets: {0}", wordlists.getDets().toString());
        logger.log(Level.INFO, "* verbes_conj: {0}", wordlists.getVerbesConj().toString());

        constInfo.setVerbesConj(wordlists.getVerbesConj());
        constInfo.setNoms(wordlists.getNoms());
        constInfo.setAdjectifs(wordlists.getAdjectifs());
        constInfo.setConjugaisons(wordlists.getConjugaisons());
        constInfo.setDets(wordlists.getDets());
        constInfo.setActus(wordlists.getActus());

        Collections.copy(constInfo.getVerbesConj(), wordlists.getVerbesConj());
        Collections.copy(constInfo.getConjugaisons(), wordlists.getConjugaisons());
        Collections.copy(constInfo.getNoms(), wordlists.getNoms());
        Collections.copy(constInfo.getAdjectifs(), wordlists.getAdjectifs());
        Collections.copy(constInfo.getDets(), wordlists.getDets());
        Collections.copy(constInfo.getActus(), wordlists.getActus());

        fillVerbeInf(phrase, lexique, wordlists);
        logger.log(Level.INFO, "AFTER fillVerbeInf: {0}", phrase.toString());

        String returnedAjustement = ajustementIfSimple(isSimple, phrase);
        logger.log(Level.FINE, "========================================================>  13  <====================");
//        fillQALogs(client,tag,returnedAjustement,tag,intentReponse,true,"ID Lexique : " + lexique.getId(), clientIP);
        return new ReponseChatbot(returnedAjustement , intentReponse,
        "ID Lexique : " + lexique.getId() +
        " || ID construction : " + construction.getId() +
        " || ID Distribution : " + lexique.getDistribution().getId() +
        " || ID actualisation : " + lexique.getActualisation().getId()  +
        " || Liste ID Deixis : " + listDeixisID.toString() );
    }

    private String getReconstructionERRORCodeOrVide(Reconstruction reconsctruction) {
        logger.log(Level.FINE, "*** getReconstructionERRORCodeOrVide");
        if (reconsctruction != null) {
            return " [CODE_RECONSTRUCTION = " + reconsctruction.getCode() + "]";
        }
        return "";
    }

    private String initConjugaison(ArrayList<String> preconjugaisons, WordLists wordlists, String isNotOk) {
        logger.log(Level.FINE, "initConjugaison: ****************************************");
        if (isNotOk != null) {
            return isNotOk;
        }
        preconjugaisons.addAll(wordlists.getConjugaisons());
        wordlists.getConjugaisons().clear();
        wordlists.getConjugaisons().addAll(preconjugaisons);
        return null;

    }

    boolean isArrayNullOrVide(Object[] list) {
        logger.log(Level.FINE, "*** isArrayNullOrVide");
        return list == null || list.length == 0;
    }

    String ajustementIfSimple(boolean isSimple, ArrayList<String> phrase) {
        logger.log(Level.FINE, "*** ajustementIfSimple");
        if (isSimple)// 9
            return ajustements(phrase);
        else
            return String.join(" ", phrase);
    }

    boolean isNullDistibutionForGNConstuction(String xDistribution, String classXconstruction) {
        logger.log(Level.FINE, "*** isNullDistibutionForGNConstuction");
        return xDistribution == null && classXconstruction.equalsIgnoreCase("GN");
    }

    private String isXNullForGN(Distribution distribution, Construction construction, String typeX) {
        logger.log(Level.FINE, "isXNullForGN: ***");
        if (construction == null) {// 1
            return "ERREUR CONSTUCTION NULL";
        }

        switch (typeX.toLowerCase()) {// 2
            case "x0":
                if (construction.getX0() == null) {// 3
                    return "ERREUR X0 DE LA CONSTRUCTION [" + construction.getIdentifiant() + "] EST NULL";
                } else if (isNullDistibutionForGNConstuction(distribution.getX0(), construction.getX0().getClasse())) {// 4
                    return "ERREUR X0 DE LA DITRIBUTION [" + distribution.getCode() + "] EST NULL";
                }
                return null;
            case "x1":
                if (construction.getX1() == null) {// 5
                    return "ERREUR X1 DE LA CONSTRUCTION [" + construction.getIdentifiant() + "] EST NULL";
                } else if (isNullDistibutionForGNConstuction(distribution.getX1(), construction.getX1().getClasse())) {// 6
                    return "ERREUR X1 DE LA DITRIBUTION [" + distribution.getCode() + "] EST NULL";
                }
                return null;
            case "x2":

                if (construction.getX2() == null) {// 7
                    return "ERREUR X2 DE LA CONSTRUCTION [" + construction.getIdentifiant() + "] EST NULL";
                } else if (isNullDistibutionForGNConstuction(distribution.getX2(), construction.getX2().getClasse())) {// 8
                    return "ERREUR X2 DE LA DITRIBUTION [" + distribution.getCode() + "] EST NULL";
                }

                return null;
            default:
                return null;
        }

    }

    // Applique soit la construction soit la reconstruction en fonction de l'objet
    // lexRec
    private String applyConstruction(ArrayList<String> phrase, String[] args, Construction construction,
            Lexique lexique, Reconstruction reconstruction, WordLists wordLists, ArrayList<String> preconjugaison) {
        logger.log(Level.FINE, "applyConstruction: ****************************************");
        Distribution dis = lexique.getDistribution();

        if (dis == null) {// 1
            return "ERREUR DISTRIBUTION NULL";
        } else if (construction == null) {// 2
            return "ERREUR CONSTUCTION NULL";
        }

        for (String el : args) {// 3
            switch (el.toLowerCase()) {// 4
                case "x0,":
                case "x0":
                    String x0Verif = isXNullForGN(dis, construction, "x0");
                    if (x0Verif != null) {// 5
                        logger.log(Level.FINE, "x0Verif: << ");
                        return x0Verif;
                    }
                    break;
                case "x1,":
                case "x1":
                    String x1Verif = isXNullForGN(dis, construction, "x1");
                    if (x1Verif != null) {// 6
                        logger.log(Level.FINE, "x1Verif: << ");
                        return x1Verif;
                    }
                    break;
                case "x2,":
                case "x2":
                    String x2Verif = isXNullForGN(dis, construction, "x2");
                    if (x2Verif != null) {// 7
                        logger.log(Level.FINE, "x2Verif: << ");
                        return x2Verif;
                    }
                    break;
                default:
                    break;
            }
        }

        if (reconstruction == null) {// 8
            construction(phrase, args, construction, lexique, wordLists, preconjugaison);
            logger.log(Level.FINE, "AFTER construction:  {0}", phrase.toString());

        } else {// 9
            reconstruction(phrase, args, lexique, wordLists, reconstruction, preconjugaison);
            fromReconstruction = true;
            logger.log(Level.FINE, "AFTER reconstruction:  {0}", phrase.toString());
        }

        return null;
    }

    // Renvoie la liste des arguments qui vont etre utiliser pour la construction ou
    // recontrstruction
    private String[] getArgs(Reconstruction reconstruction, Lexique lexique) {
        logger.log(Level.FINE, "*** getArgs");
        if (reconstruction == null)
            return lexique.getConstruction().getTypeConstruction().split(",");
        else
            return reconstruction.getValue().split("\\s+");
    }

    // ce lexique et si l'aleatoire le veut
    private Reconstruction isReconstruction(Lexique lexique) {
        logger.log(Level.FINE, "isReconstruction: ****************************************");
        int rand = ThreadLocalRandom.current().nextInt(0, 5);// 80% reconstruction 20% construction
        logger.log(Level.INFO, ">> RANDOM: {0} ", rand);
        if (rand > 0) {// 1
            Construction construction = lexique.getConstruction();
            ArrayList<Reconstruction> listReconstruction = new ArrayList<>();

            Set<Reconstruction> setListReconstruction = construction.getReconstructions();
            listReconstruction.addAll(setListReconstruction);
            logger.log(Level.INFO, ">> listReconstruction: {0} ", listReconstruction.toString());
            int size = listReconstruction.size();
            logger.log(Level.INFO, ">> nombre de reconstruction: {0}", size);
            if (size > 0) {// 2
                String[] familleLexique = lexique.getFamilleAsElements();
                logger.log(Level.INFO, ">> FAMILLE LEXIQUE: {0}", lexique.getFamille());
                int randReconstruction = ThreadLocalRandom.current().nextInt(0, size);
                Reconstruction reconstructionAleatoire = listReconstruction.get(randReconstruction);
                logger.log(Level.INFO, ">> SELECTED RECONSTRUCTION: {0}", reconstructionAleatoire.getCode());
                String[] familleReconstruction = reconstructionAleatoire.getFamilleAsElements();
                logger.log(Level.INFO, ">> FAMILLE RECONSTRUCTION: {0}", reconstructionAleatoire.getFamille());
                if (isArrayNullOrVide(familleReconstruction)) {// 3
                    logger.log(Level.INFO, ">> SELECTED RECONSTRUCTION: {0}", reconstructionAleatoire.getCode());
                    logger.log(Level.INFO, "<< SELECTED RECONSTRUCTION");
                    return reconstructionAleatoire;
                } else {// 4

                    if (isArrayNullOrVide(familleLexique)) {// 5
                        logger.log(Level.INFO, ">> FAMILLE LEXIQUE NULL OU VIDE");
                        logger.log(Level.INFO, "<< NULL");
                        return null;
                    }

                    boolean contientAuMoinsUneFamilleAcceptee = listReconstHasFamilleAccepted(listReconstruction,
                            familleLexique);

                    logger.log(Level.INFO, ">> FAMILLE ACCEPTED: {0}", contientAuMoinsUneFamilleAcceptee);

                    if (!contientAuMoinsUneFamilleAcceptee) {// 6
                        logger.log(Level.INFO, "<< NULL");
                        return null;
                    }

                    boolean reconstructionAccepted = contientAuMoinsUneFamille(familleLexique,
                            reconstructionAleatoire.getFamilleAsElements());

                    logger.log(Level.INFO, ">> RECONSTRUCTION ACCEPTED: {0}", reconstructionAccepted);

                    logger.log(Level.INFO, ">> SEARCH ACCEPTED RECONSTRUCTION...");

                    while (!reconstructionAccepted) {// 7

                        randReconstruction = ThreadLocalRandom.current().nextInt(0, size);
                        reconstructionAleatoire = listReconstruction.get(randReconstruction);

                        reconstructionAccepted = contientAuMoinsUneFamille(familleLexique,
                                reconstructionAleatoire.getFamilleAsElements());

                        logger.log(Level.INFO, "*****/ -*-*-*-*-* \n rec: {0}", reconstructionAleatoire.getCode());
                        logger.log(Level.INFO, "*****/ -*-*-*-*-* \n hasFamille2: {0}", reconstructionAccepted);

                    }

                    logger.log(Level.INFO, ">> END SEARCH RECONSTRUCTION.");

                    logger.log(Level.INFO, ">> RECONSTURCTION APPLIQUEE: {0}", reconstructionAleatoire.getCode());
                    logger.log(Level.INFO, "<< RECONSTURCTION");
                    return reconstructionAleatoire;
                }

            } else {
                logger.log(Level.INFO, ">> AUCUNE RECONSTRUCTION");
            }
        }

        logger.log(Level.INFO, "<< NULL");
        return null;
    }

    boolean listReconstHasFamilleAccepted(ArrayList<Reconstruction> reconstruction, String[] listFamilleLexique) {
        logger.log(Level.INFO, "*** listReconstHasFamilleAccepted");
        for (Reconstruction rec : reconstruction) {
            String[] listFamille = rec.getFamilleAsElements();
            if (!isArrayNullOrVide(listFamille)
                    && contientAuMoinsUneFamille(listFamilleLexique, rec.getFamilleAsElements())) {
                logger.log(Level.INFO, "<< TRUE");
                return true;
            }
        }

        logger.log(Level.INFO, "<< FALSE");
        return false;
    }

    boolean contientAuMoinsUneFamille(String[] familleLexique, String[] familleReconstruction) {
        logger.log(Level.INFO, "*** contientAuMoinsUneFamille");
        Set<String> familleReconstructionSet = new HashSet<>(Arrays.asList(familleReconstruction));

        for (String fLexique : familleLexique) {
            logger.log(Level.WARNING, "*****/ -*-*-*-*-* \n fLexique: {0}", fLexique);
            logger.log(Level.WARNING, "*****/ -*-*-*-*-* \n familleReconstructionSet has fLexique: {0}",
                    familleReconstructionSet.contains(fLexique));
            if (familleReconstructionSet.contains(fLexique)) {
                logger.log(Level.INFO, "<< TRUE");
                return true;
            }
        }
        logger.log(Level.INFO, "<< FALSE");
        return false;
    }

    // Ajoute un Deixis a la phrase et ajoute les informations necessaires dans les
    // listes (conjugaisons notemment)
    private void pushX(ArrayList<String> conjugaisons, ArrayList<String> phrase, Lexique lexique, WordLists wordLists,
            Deixis x, String distribX) {
        logger.log(Level.INFO, "pushX *****************************************");
        listDeixisID.add(x.getId());
        if (x.getId() != null && x.getValeur() != null) {
            String pronom = getPronom(x, conjugaisons);
            logger.log(Level.INFO, ">> ADD TO PHRASE {0} ", pronom);
            phrase.add(pronom);
        }

        if (x.getClasse().equalsIgnoreCase("GN")) {
            logger.log(Level.INFO, ">> ADD TO CONJUGAISON {0} ", x.getCategorieMorphologique());
            conjugaisons.add(x.getCategorieMorphologique());
            String gGN = getGN(distribX, wordLists);
            logger.log(Level.INFO, ">> ADD TO PHRASE {0} ", gGN);
            phrase.add(gGN);
        }

        if (x.getClasse().equalsIgnoreCase("VAL")) {
            logger.log(Level.INFO, ">> ADD TO PHRASE {0} ", lexique.getValeur());
            phrase.add(lexique.getValeur());
        }

    }

    // Ajoute le champs valeur du lexique a la phrase et le met dans une liste en
    // fonction de son type (adjectif ou nom)
    private void pushVal(Lexique lexique, WordLists wordLists, ArrayList<String> phrase) {
        logger.log(Level.INFO, "pushVal *****************************************");
        if (lexique.getValeur() != null) {
            if (lexique.getTypes().equalsIgnoreCase("adjectif")) {
                wordLists.getAdjectifs().add(lexique.getValeur());
                logger.log(Level.INFO, ">> ADD TO Adjectifs wordLists {0} ", lexique.getValeur());
            }

            if (lexique.getTypes().equalsIgnoreCase("nom")) {
                wordLists.getNoms().add(lexique.getValeur());
                logger.log(Level.INFO, ">> ADD TO Noms wordLists {0} ", lexique.getValeur());
            }

            phrase.add(lexique.getValeur());
            logger.log(Level.INFO, ">> ADD TO PHRASE {0} ", lexique.getValeur());
        }
    }

    // verifier les verbes composés
    private void checkVerb(String arg, ArrayList<String> args, WordLists wordLists) {
        logger.log(Level.INFO, "checkVerb *****************************************");
        int index = args.indexOf(arg);
        if (index >= args.size() - 1)
            return;

        String vbComp = arg + " " + args.get(index + 1);
        VerbeCompose[] verbComps = verbeComposeRepository.findByForm(vbComp);

        if (verbComps.length > 0) {
            wordLists.getVerbes().add(arg);
            logger.log(Level.INFO, ">> ADD TO VERBES wordLists {0} ", arg);
        }

    }

    // La meme chose que la fonction de construction
    private void reconstruction(ArrayList<String> phrase, String[] args, Lexique lexique, WordLists wordLists,
            Reconstruction reconstruction, ArrayList<String> conjugaisons) {
        logger.log(Level.FINE, "reconstruction: ****************************************");
        Construction construction = lexique.getConstruction();
        logger.log(Level.INFO, ">> construction : {0}", construction.getTypeConstruction());

        for (String arg : args) {
            logger.log(Level.INFO, ">> arg : {0}", arg);
            switch (arg.toLowerCase()) {
                case "v":
                case "v,":
                case "a":
                case "a,":
                case "vppé":
                case "vppé,":
                case "adv":
                case "adv,":
                case "n,":
                case "n":
                    pushVal(lexique, wordLists, phrase);
                    logger.log(Level.INFO, ">> AFTER  pushVal: {0}", phrase.toString());
                    break;
                case "vsup,":
                case "vsup":
                    if (construction.getVsup() != null) {
                        logger.log(Level.INFO, ">> ADD TO phrase {0} ", construction.getVsup());
                        phrase.add(construction.getVsup());
                    }

                    logger.log(Level.INFO, ">> AFTER  addVsup: {0}", phrase.toString());

                    break;
                case "x0,":
                case "x0":
                    pushX(conjugaisons, phrase, lexique, wordLists, construction.getX0(),
                            lexique.getDistribution().getX0());
                    logger.log(Level.INFO, ">> AFTER  pushX: {0}", phrase.toString());
                    break;
                case "x1,":
                case "x1":
                    pushX(conjugaisons, phrase, lexique, wordLists, construction.getX1(),
                            lexique.getDistribution().getX1());
                    logger.log(Level.INFO, ">> AFTER  pushX: {0}", phrase.toString());
                    break;
                case "x2,":
                case "x2":
                    pushX(conjugaisons, phrase, lexique, wordLists, construction.getX2(),
                            lexique.getDistribution().getX2());
                    logger.log(Level.INFO, ">> AFTER  pushX: {0}", phrase.toString());
                    break;
                default:
                    if (stringEqualOR(arg, "LEUR", "LE", "de")) {
                        logger.log(Level.INFO, ">> ADD TO Dets wordLists {0} ", arg);
                        wordLists.getDets().add(arg);
                    }

                    checkVerb(arg, new ArrayList<>(Arrays.asList(args)), wordLists);
                    logger.log(Level.INFO, ">> AFTER  checkVerb: {0}", phrase.toString());
                    phrase.add(arg);
                    logger.log(Level.INFO, ">> ADD TO phrase {0} ", arg);
                    logger.log(Level.INFO, ">> ADD arg TO phrase {0} ", phrase.toString());
            }
        }
    }

    // remplis la liste de verbe a l'infinitif pour d'autre regles
    private void fillVerbeInf(ArrayList<String> phrase, Lexique lexique, WordLists wordLists) {
        logger.log(Level.FINE, "fillVerbeInf: ****************************************");
        for (int i = 0; i < phrase.size(); ++i) {
            Vslemme vslemme = vslemmeRepository.findOneByLemme(phrase.get(i));
            if (vslemme != null && !phrase.get(i).equals(lexique.getConstruction().getVsup())
                    && !phrase.get(i).equals(lexique.getValeur())) {
                logger.log(Level.INFO, ">> ADD TO VerbeInf wordLists {0} ", phrase.get(i));
                wordLists.getVerbeInf().add(phrase.get(i));
                continue;
            }
            VerbeCompose[] vbCompose = verbeComposeRepository.findByForm(phrase.get(i));
            if (vbCompose != null && vbCompose.length > 0 && !phrase.get(i).equals(lexique.getConstruction().getVsup())
                    && !phrase.get(i).equals(lexique.getValeur())) {
                logger.log(Level.INFO, ">> ADD TO VerbeInf wordLists {0} ", phrase.get(i));
                wordLists.getVerbeInf().add(phrase.get(i));
            }
        }
    }

    // Push de chaque argument de la construction dans la phrase ainsi que l'ajout
    // de certains parametre dans des listes en parallele pour pouvoir appliquer des
    // regles plus specifiques
    private void construction(ArrayList<String> phrase, String[] args, Construction construction, Lexique lexique,
            WordLists wordLists, ArrayList<String> conjugaisons) {
        logger.log(Level.FINE, "construction: ****************************************");

        for (String arg : args) {
            logger.log(Level.INFO, ">> arg : {0}", arg);
            switch (arg.toLowerCase()) {
                case "prep0":
                case "prep0,":
                    logger.log(Level.INFO, ">> prep0: {0}", construction.getPrep0());
                    if (construction.getPrep0() != null) {
                        phrase.add(construction.getPrep0());
                        logger.log(Level.INFO, ">> AFTER ADD Prep0: {0}", phrase.toString());
                    }

                    break;
                case "prep":
                case "prep,":

                    logger.log(Level.INFO, ">> prep: {0}", construction.getPrep());

                    if (construction.getPrep() != null) {
                        phrase.add(construction.getPrep());
                        logger.log(Level.INFO, ">> AFTER ADD Prep: {0}", phrase.toString());
                    }

                    break;
                case "prep1":
                case "prep1,":
                    logger.log(Level.INFO, ">> prep1: {0}", construction.getPrep1());
                    if (construction.getPrep1() != null) {
                        if(construction.getPrep1().contains("LE1"))
                        {
                            logger.info("Coordination LE1 " + construction.getX1().getCategorieMorphologique() );
                            Distribution localDistribution = lexique.getDistribution();
                            Deixis[] localDeixisX1 = deixisRepository.findByClasse(localDistribution.getX1());
                            randomDeixisX1 = ThreadLocalRandom.current().nextInt(0, localDeixisX1.length);
                            logger.info("Index Deixis ==> " + randomDeixisX1);

                            if(localDeixisX1[randomDeixisX1] != null){

                                logger.info("Coordination LE1  deixis valeur de X1 " + localDeixisX1[randomDeixisX1].getValeur() );

                                Formes[] formes = formesRepository.findByForme(localDeixisX1[randomDeixisX1].getValeur());
                                Formes selectedForme = null;
                                if(formes.length>0){
                                    selectedForme = formes[0];
//                                    cas MS
                                    if(selectedForme.getGender().equals("M") && selectedForme.getNumber().equals("S"))
                                    {
                                        phrase.add("le");
                                    }
//                                cas FS
                                    else if(selectedForme.getGender().equals("F") && selectedForme.getNumber().equals("S"))
                                    {
                                        phrase.add("la");
                                    }
//                                cas P
                                    else if(selectedForme.getNumber().equals("P"))
                                    {
                                        phrase.add("les");
                                    }
                                }


                                else{
                                    phrase.add(construction.getPrep1());
                                }
                            }
                        }
                        else {
                            phrase.add(construction.getPrep1());
                        }

                        logger.log(Level.INFO, ">> AFTER ADD Prep1: {0}", phrase.toString());
                    }

                    break;
                case "prep2":
                case "prep2,":
                    logger.log(Level.INFO, ">> prep2: {0}", construction.getPrep2());
                    if (construction.getPrep2() != null) {
                        phrase.add(construction.getPrep2());
                        logger.log(Level.INFO, ">> AFTER ADD Prep2: {0}", phrase.toString());
                    }

                    break;
                case "det":
                case "det,":
                    logger.log(Level.INFO, ">> det: {0}", construction.getDet());
                    if (construction.getDet() != null) {
                        Distribution localDistribution = lexique.getDistribution();
                        Formes[] formes = formesRepository.findByForme(lexique.getValeur());
                        logger.info("formes.length ==> " + formes.length);
                        if(formes.length >0){
                            Formes selectedForme = formes[0];
                            //                        Cas IL0
                            if(construction.getDet().contains("IL0"))  {
                                Deixis[] localDeixisX0 = deixisRepository.findByClasse(localDistribution.getX0());
                                if(localDeixisX0 != null){
                                    randomDeixisX0 = ThreadLocalRandom.current().nextInt(0, localDeixisX0.length);
                                    logger.info("Index randomDeixisX0 ==> " + randomDeixisX0);
                                    logger.info("DeixisX0 selected ==> " + localDeixisX0[randomDeixisX0].getCategorieMorphologique());

                                    if(localDeixisX0 != null){

                                        switch (localDeixisX0[randomDeixisX0].getCategorieMorphologique()){
                                            case "1_S":
                                                phrase.add("je");
                                                break;
                                            case "2_S":
                                                phrase.add("tu");
                                                break;
                                            case "3_S":
                                                phrase.add("il");
                                                break;
                                            case "1_P":
                                                phrase.add("nous");
                                                break;
                                            case "2_P":
                                                phrase.add("vous");
                                                break;
                                            case "3_P":
                                                phrase.add("ils");
                                                break;
                                        }

                                    }
                                }
                            }
                            //                     Cas   POSS0
                            else if(construction.getDet().contains("POSS0")){
                                Deixis[] localDeixisX0 = deixisRepository.findByClasse(localDistribution.getX0());
                                logger.info("Remplcament POSS0");
                                if(localDeixisX0 != null){
                                    logger.info("X0  classe ==> " + localDeixisX0[randomDeixisX0].getClasse() + " || Valeur ==> " + localDeixisX0[randomDeixisX0].getValeur() );
                                    if(selectedForme != null && selectedForme.getGender() != null && selectedForme.getNumber() != null){
//                                    Cas MS
                                        if(selectedForme.getGender().equals("M") && selectedForme.getNumber().equals("S")){
                                            switch (localDeixisX0[randomDeixisX0].getCategorieMorphologique()){
                                                case "1_S":
                                                    phrase.add("mon");
                                                    break;
                                                case "2_S":
                                                    phrase.add("ton");
                                                    break;
                                                case "3_S":
                                                    phrase.add("son");
                                                    break;
                                                case "1_P":
                                                    phrase.add("notre");
                                                    break;
                                                case "2_P":
                                                    phrase.add("votre");
                                                    break;
                                                case "3_P":
                                                    phrase.add("leurs");
                                                    break;
                                            }
                                        }
//                                    Cas FS
                                        if(selectedForme.getGender().equals("F") && selectedForme.getNumber().equals("S")){
                                            switch (localDeixisX0[randomDeixisX0].getCategorieMorphologique()){
                                                case "1_S":
                                                    phrase.add("ma");
                                                    break;
                                                case "2_S":
                                                    phrase.add("ta");
                                                    break;
                                                case "3_S":
                                                    phrase.add("sa");
                                                    break;
                                                case "1_P":
                                                    phrase.add("notre");
                                                    break;
                                                case "2_P":
                                                    phrase.add("votre");
                                                    break;
                                                case "3_P":
                                                    phrase.add("leur");
                                                    break;
                                            }
                                        }
//                                    Cas MP et FP
                                        if( selectedForme.getNumber().equals("P")){
                                            switch (localDeixisX0[randomDeixisX0].getCategorieMorphologique()){
                                                case "1_S":
                                                    phrase.add("mes");
                                                    break;
                                                case "2_S":
                                                    phrase.add("tes");
                                                    break;
                                                case "3_S":
                                                    phrase.add("ses");
                                                    break;
                                                case "1_P":
                                                    phrase.add("nos");
                                                    break;
                                                case "2_P":
                                                    phrase.add("vos");
                                                    break;
                                                case "3_P":
                                                    phrase.add("leurs");
                                                    break;
                                            }
                                        }

                                    }
                                }
                            }

                            //                        Cas POSS1
                            else if(construction.getDet().contains("POSS1")) {
                                Deixis[] localDeixisX1 = deixisRepository.findByClasse(localDistribution.getX1());
                                if(localDeixisX1 != null){
                                    randomDeixisX1 = ThreadLocalRandom.current().nextInt(0, localDeixisX1.length);
                                    logger.info("Index randomDeixisX1 ==> " + randomDeixisX1);
                                    logger.info("DeixisX1 selected ==> " + localDeixisX1[randomDeixisX1].getCategorieMorphologique());

                                    if(localDistribution.getX1() != null) {
                                        logger.info("Remplcament POSS1");
                                        if(localDeixisX1 != null){
                                            logger.info("X1  classe ==> " + localDeixisX1[randomDeixisX1].getClasse() + " || Valeur ==> " + localDeixisX1[randomDeixisX1].getValeur() );
                                            if(selectedForme != null && selectedForme.getGender() != null && selectedForme.getNumber() != null){
//                                    Cas MS
                                                if(selectedForme.getGender().equals("M") && selectedForme.getNumber().equals("S")){
                                                    switch (localDeixisX1[randomDeixisX1].getCategorieMorphologique()){
                                                        case "1_S":
                                                            phrase.add("mon");
                                                            break;
                                                        case "2_S":
                                                            phrase.add("ton");
                                                            break;
                                                        case "3_S":
                                                            phrase.add("son");
                                                            break;
                                                        case "1_P":
                                                            phrase.add("notre");
                                                            break;
                                                        case "2_P":
                                                            phrase.add("votre");
                                                            break;
                                                        case "3_P":
                                                            phrase.add("leurs");
                                                            break;
                                                    }
                                                }
//                                    Cas FS
                                                if(selectedForme.getGender().equals("F") && selectedForme.getNumber().equals("S")){
                                                    switch (localDeixisX1[randomDeixisX1].getCategorieMorphologique()){
                                                        case "1_S":
                                                            phrase.add("ma");
                                                            break;
                                                        case "2_S":
                                                            phrase.add("ta");
                                                            break;
                                                        case "3_S":
                                                            phrase.add("sa");
                                                            break;
                                                        case "1_P":
                                                            phrase.add("notre");
                                                            break;
                                                        case "2_P":
                                                            phrase.add("votre");
                                                            break;
                                                        case "3_P":
                                                            phrase.add("leur");
                                                            break;
                                                    }
                                                }
//                                    Cas MP et FP
                                                if( selectedForme.getNumber().equals("P")){
                                                    switch (localDeixisX1[randomDeixisX1].getCategorieMorphologique()){
                                                        case "1_S":
                                                            phrase.add("mes");
                                                            break;
                                                        case "2_S":
                                                            phrase.add("tes");
                                                            break;
                                                        case "3_S":
                                                            phrase.add("ses");
                                                            break;
                                                        case "1_P":
                                                            phrase.add("nos");
                                                            break;
                                                        case "2_P":
                                                            phrase.add("vos");
                                                            break;
                                                        case "3_P":
                                                            phrase.add("leurs");
                                                            break;
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                            }

                            //                        Cas POSS2
                            else  if(construction.getDet().contains("POSS2")) {
                                Deixis[] localDeixisX2 = deixisRepository.findByClasse(localDistribution.getX2());
                                if(localDeixisX2 != null){
                                    randomDeixisX2 = ThreadLocalRandom.current().nextInt(0, localDeixisX2.length);
                                    logger.info("Index randomDeixisX2 ==> " + randomDeixisX2);
                                    logger.info("DeixisX2 selected ==> " + localDeixisX2[randomDeixisX2].getCategorieMorphologique());


                                    if(localDistribution.getX2() != null) {
                                        logger.info("Remplcament POSS2");
                                        if(localDeixisX2 != null){
                                            logger.info("X2  classe ==> " + localDeixisX2[randomDeixisX2].getClasse() + " || Valeur ==> " + localDeixisX2[randomDeixisX2].getValeur() );
                                            if(selectedForme != null && selectedForme.getGender() != null && selectedForme.getNumber() != null){
//                                    Cas MS
                                                if(selectedForme.getGender().equals("M") && selectedForme.getNumber().equals("S")){
                                                    switch (localDeixisX2[randomDeixisX2].getCategorieMorphologique()){
                                                        case "1_S":
                                                            phrase.add("mon");
                                                            break;
                                                        case "2_S":
                                                            phrase.add("ton");
                                                            break;
                                                        case "3_S":
                                                            phrase.add("son");
                                                            break;
                                                        case "1_P":
                                                            phrase.add("notre");
                                                            break;
                                                        case "2_P":
                                                            phrase.add("votre");
                                                            break;
                                                        case "3_P":
                                                            phrase.add("leurs");
                                                            break;
                                                    }
                                                }
//                                    Cas FS
                                                if(selectedForme.getGender().equals("F") && selectedForme.getNumber().equals("S")){
                                                    switch (localDeixisX2[randomDeixisX2].getCategorieMorphologique()){
                                                        case "1_S":
                                                            phrase.add("ma");
                                                            break;
                                                        case "2_S":
                                                            phrase.add("ta");
                                                            break;
                                                        case "3_S":
                                                            phrase.add("sa");
                                                            break;
                                                        case "1_P":
                                                            phrase.add("notre");
                                                            break;
                                                        case "2_P":
                                                            phrase.add("votre");
                                                            break;
                                                        case "3_P":
                                                            phrase.add("leur");
                                                            break;
                                                    }
                                                }
//                                    Cas MP et FP
                                                if( selectedForme.getNumber().equals("P")){
                                                    switch (localDeixisX2[randomDeixisX2].getCategorieMorphologique()){
                                                        case "1_S":
                                                            phrase.add("mes");
                                                            break;
                                                        case "2_S":
                                                            phrase.add("tes");
                                                            break;
                                                        case "3_S":
                                                            phrase.add("ses");
                                                            break;
                                                        case "1_P":
                                                            phrase.add("nos");
                                                            break;
                                                        case "2_P":
                                                            phrase.add("vos");
                                                            break;
                                                        case "3_P":
                                                            phrase.add("leurs");
                                                            break;
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }

                            }
                            else {
                                phrase.add(construction.getDet());
                            }
                        }

                        else {
                            phrase.add(construction.getDet());
                        }
                        logger.log(Level.INFO, ">> AFTER ADD det: {0}", phrase.toString());
                    }

                        wordLists.getDets().add(construction.getDet());
                    break;
                case "vsup":
                case "vsup,":
                    logger.log(Level.INFO, ">> Vsup: {0}", construction.getVsup());
                    if (construction.getVsup() != null) {
                        phrase.add(construction.getVsup());
                        logger.log(Level.INFO, ">> AFTER ADD Vsup: {0}", phrase.toString());
                    }

                    break;
                case "x0":
                case "x0,":
                    if (construction.getX0().getId() != null && construction.getX0().getValeur() != null) {
                        String pronomX0 = getPronom(construction.getX0(), conjugaisons);
                        logger.log(Level.INFO, ">> pronom X0: {0}", pronomX0);
                        phrase.add(pronomX0);
                        logger.log(Level.INFO, ">> AFTER ADD PRENOM X0: {0}", phrase.toString());
                    }

                    if (construction.getX0().getClasse().equalsIgnoreCase("GN")) {
                        logger.log(Level.INFO, ">> conjugaisons ADD getCategorieMorphologique X0: {0}",
                                construction.getX0().getCategorieMorphologique());
                        conjugaisons.add(construction.getX0().getCategorieMorphologique());
                        String x0 = lexique.getDistribution().getX0();
                        logger.log(Level.INFO, ">> x0 Distribution: {0}", x0);
                        phrase.add(getGN(lexique.getDistribution().getX0(), wordLists));
                        logger.log(Level.INFO, ">> AFTER ADD x0 Distribution: {0}", phrase.toString());
                    }
                    if (construction.getX0().getClasse().equalsIgnoreCase("VAL")) {
                        logger.log(Level.INFO, ">> lexique valeur: {0}", lexique.getValeur());
                        phrase.add(lexique.getValeur());
                        logger.log(Level.INFO, ">> AFTER ADD lexique valeur: {0}", phrase.toString());
                    }

                    break;
                case "x1":
                case "x1,":
                    if (construction.getX1().getId() != null && construction.getX1().getValeur() != null) {
                        String pronomX1 = getPronom(construction.getX1(), conjugaisons);
                        logger.log(Level.INFO, ">> pronom X1: {0}", pronomX1);
                        phrase.add(pronomX1);
                        logger.log(Level.INFO, ">> AFTER ADD PRENOM X1: {0}", phrase.toString());
                    }

                    if (construction.getX1().getClasse().equalsIgnoreCase("GN")) {
                        logger.log(Level.INFO, ">> conjugaisons ADD getCategorieMorphologique X1: {0}",
                                construction.getX1().getCategorieMorphologique());
                        conjugaisons.add(construction.getX1().getCategorieMorphologique());
                        String gGNX1 = getGN(lexique.getDistribution().getX1(), wordLists);
                        logger.log(Level.INFO, ">> gGNX1: {0}", gGNX1);
                        phrase.add(gGNX1);
                        logger.log(Level.INFO, ">> AFTER ADD gGNX1: {0}", phrase.toString());
                    }
                    if (construction.getX1().getClasse().equalsIgnoreCase("VAL")) {
                        logger.log(Level.INFO, ">> lexique valeur: {0}", lexique.getValeur());
                        phrase.add(lexique.getValeur());
                        logger.log(Level.INFO, ">> AFTER ADD lexique valeur: {0}", phrase.toString());
                    }

                    break;
                case "x2":
                case "x2,":
                    if (construction.getX2().getId() != null && construction.getX2().getValeur() != null) {
                        phrase.add(getPronom(construction.getX2(), conjugaisons));
                    }

                    if (construction.getX2().getClasse().equalsIgnoreCase("GN")) {
                        logger.log(Level.INFO, ">> conjugaisons ADD getCategorieMorphologique X2: {0}",
                                construction.getX2().getCategorieMorphologique());
                        conjugaisons.add(construction.getX2().getCategorieMorphologique());
                        logger.log(Level.INFO, "construction getGN x2 : {0}", lexique.getDistribution().getX2());
                        String gGNX2 = getGN(lexique.getDistribution().getX2(), wordLists);
                        phrase.add(gGNX2);
                        logger.log(Level.INFO, ">> AFTER ADD gGNX2: {0}", phrase.toString());
                    }
                    if (construction.getX2().getClasse().equalsIgnoreCase("VAL")) {
                        logger.log(Level.INFO, ">> lexique valeur: {0}", lexique.getValeur());
                        phrase.add(lexique.getValeur());
                        logger.log(Level.INFO, ">> AFTER ADD lexique valeur: {0}", phrase.toString());
                    }

                    break;
                case "val":
                case "val,":
                    if (lexique.getValeur() != null) {
                        if (lexique.getTypes().equalsIgnoreCase("adjectif")) {
                            logger.log(Level.INFO, ">> ADD TO ADJECTIFS WORDLISTS valeur: {0}", lexique.getValeur());
                            wordLists.getAdjectifs().add(lexique.getValeur());
                        }

                        if (lexique.getTypes().equalsIgnoreCase("nom")) {
                            logger.log(Level.INFO, ">> ADD TO Noms WORDLISTS valeur: {0}", lexique.getValeur());
                            wordLists.getNoms().add(lexique.getValeur());
                        }

                        logger.log(Level.INFO, ">> ADD TO LEXIQUE VALEUR TO PHRASE : {0}", lexique.getValeur());
                        phrase.add(lexique.getValeur());
                        logger.log(Level.INFO, ">> AFTER ADD LEXIQUE VALEUR TO PHRASE: {0}", phrase.toString());
                    }
                    break;
            }
        }

    }

    // Remplace les Deixis par des noms commums ou pronoms aleatoires
    // ex: RESIDENCE -> etablissement ou residence
    private String changeGN(ArrayList<String> phrase, int i, String distrib, String prevConj, WordLists wordLists, int indice_X) {
        logger.log(Level.FINE, "changeGN: *************** indice ==> " + i);
        logger.info("indice_X ===========>  " + indice_X);
        String phraseI = phrase.get(i);

        if (!isNullOrVideString(phraseI) && phraseI.equals(distrib)) {// 1/2
            logger.log(Level.INFO, "Changing GN from {0}", phrase.get(i));

            Deixis[] deixis = deixisRepository.findByClasse(phrase.get(i));

            if (deixis.length == 0) {
                logger.log(Level.FINE, "<< ERREUR");
                return "ERREUR DEIXIS non trouvé, pour la classe : " + phrase.get(i);
            }

            int selectedDeixisIndice = randomDeixisX0;
            switch (indice_X){
                case 0:
                    selectedDeixisIndice = randomDeixisX0;
                    break;
                case 1:
                    selectedDeixisIndice = randomDeixisX1;
                    break;
                case 2 :
                    selectedDeixisIndice = randomDeixisX2;
                    break;
            }

            logger.info("selectedDeixisIndice ==> " + selectedDeixisIndice);

            if(selectedDeixisIndice > deixis.length || selectedDeixisIndice == -1){
                selectedDeixisIndice = ThreadLocalRandom.current().nextInt(0, deixis.length);
                logger.info("Actualisation selectedDeixisIndice becauce equal to 0 or oversize");
            }


            String[] nomChoisis = deixis[selectedDeixisIndice].getValeur().split(";");
            listDeixisID.add(deixis[selectedDeixisIndice].getId());
            selectedDeixis = deixis[selectedDeixisIndice];
            int randNom = ThreadLocalRandom.current().nextInt(0, nomChoisis.length);
            wordLists.getNoms().set(wordLists.getNoms().indexOf(phrase.get(i)), nomChoisis[randNom]);
            // wordLists.resetNomsSet();
            if (nomChoisis[randNom].contains("%")) {// 3
                Ontologie ont = ontologieRepository.findOneByCode(nomChoisis[randNom]);
                String[] vals = ont.getValeur().split("/");

                // logger.log(Level.INFO, "checking every ontologie param : {0}", vals);
                phrase.remove(i - 1);
                i--;
                phrase.remove(i);
                for (String val : vals) {// 4
                    Deixis dex = deixisRepository.findOneByValeur(val);
                    listDeixisID.add(dex.getId());
                    if (dex == null) {// 5
                        logger.log(Level.FINE, "<< ERREUR");
                        return "ERREUR Deixis MASSIF COMPTABLE EST NULL : [ DEIXIS non trouvé:" + val + "]";
                    }
                    logger.log(Level.INFO, ">> checking massif comptable for : {0}", val);
                    logger.log(Level.FINE, ">> deixis massifComptable : {0}", dex.getMassifComptable());

                    switch (dex.getMassifComptable()) {// 6
                        case "massif":
                            logger.log(Level.INFO, ">> ADD TO PHRASE : du");
                            phrase.add(i, "du");
                            logger.log(Level.INFO, ">> ADD TO PHRASE : {0}", val);
                            phrase.add(i + 1, val);
                            wordLists.getDets().add("du");
                            wordLists.getNoms().add(val);
                            i++;
                            break;
                        case "comptable":
                            logger.log(Level.INFO, ">> ADD TO PHRASE : un");
                            phrase.add(i, "un");
                            phrase.add(i + 1, val);
                            logger.log(Level.INFO, ">> ADD TO PHRASE : {0}", val);
                            wordLists.getDets().add("un");
                            wordLists.getNoms().add(val);
                            i++;
                            break;
                        default:
                            logger.log(Level.INFO, ">> ADD TO PHRASE : {0}", val);
                            phrase.add(i, val);
                            wordLists.getNoms().add(val);
                            break;
                    }

                    i++;
                }
            } else {// 7
                logger.log(Level.INFO, ">> UPDATE ELEMENT PHRASE : {0}", phrase.get(i));
                logger.log(Level.INFO, ">> BY : {0}", nomChoisis[randNom]);
                phrase.set(i, nomChoisis[randNom]);
            }

            String[] allConj = deixis[selectedDeixisIndice].getCategorieMorphologique().split(";");
            int randNomAllConj = ThreadLocalRandom.current().nextInt(0, allConj.length);
            logger.log(Level.INFO, "<< : {0}", allConj[randNomAllConj]);

            return allConj[randNomAllConj];
        }
        return prevConj;

    }

    private boolean isNullOrVideString(String... str) {
        for (String element : str) {
            if (element == null || element.replaceAll(" ", "").equals("")) {
                return true;
            }
        }
        return false;
    }

    // Applique la distribution, va changer chaque Deixis par une valeur et push les
    // conjugaisons de chaque deixis dans une liste de conjugaisons
    private String applyDistribution(Distribution distribution, ArrayList<String> phrase, WordLists wordLists) {
        logger.log(Level.FINE, "applyDistribution: ************************************");

        if (distribution == null)// 8
            return null;

        String secConj = "";
        String troiConj = "";

        logger.log(Level.FINE, ">> conjugaisons: {0}", wordLists.getConjugaisons().toString());
        for (int i = 0; i < phrase.size(); ++i) {// 1
            if (!wordLists.getConjugaisons().isEmpty()) {// 2
                String chang = changeGN(phrase, i, distribution.getX0(), wordLists.getConjugaisons().get(0), wordLists,0);
                logger.log(Level.FINE, ">> chang: {0}", chang);
                if (chang.toUpperCase().contains("ERREUR")) {// 3
                    return chang;
                }
                wordLists.getConjugaisons().set(0, chang);
            }

            String x1 = distribution.getX1();
            if (!isNullOrVideString(x1)) {// 4
                if (wordLists.getConjugaisons().size() <= 1) {// 5
                    logger.log(Level.FINE, "<< ERREUR");
                    return "Desolé, il y'a une erreur dans mon système, INFO ERREUR [DISTRUBUTION CODE: "
                            + distribution.getCode() + "] X1=" + x1
                            + " EXIST MAIS NOMBRE DE CONJUGAISON NE DEPASSE PAS 1 ELEMENT";
                }

                secConj = changeGN(phrase, i, x1, wordLists.getConjugaisons().get(1), wordLists,1);
                logger.log(Level.FINE, "secConj: {0}", secConj);
                if (secConj.toUpperCase().contains("ERREUR")) {// 6
                    logger.log(Level.FINE, "<< {0}", secConj);
                    return secConj;
                }
            }

            String x2 = distribution.getX2();
            if (!isNullOrVideString(x2)) {// 7
                if (wordLists.getConjugaisons().size() <= 2) {// 8
                    logger.log(Level.FINE, "<< ERREUR");
                    return "Desolé, il y'a une erreur dans mon système, INFO ERREUR [DISTRUBUTION CODE: "
                            + distribution.getCode() + "] X2=" + x2
                            + " EXIST MAIS NOMBRE DE CONJUGAISON NE DEPASSE PAS 2 ELEMENT";
                }
                troiConj = changeGN(phrase, i, distribution.getX2(), wordLists.getConjugaisons().get(2), wordLists,2);
                logger.log(Level.FINE, "troiConj: {0}", troiConj);
                if (troiConj.toUpperCase().contains("ERREUR")) {// 9
                    logger.log(Level.FINE, "<< {0}", troiConj);
                    return troiConj;
                }
            }

            setConjugaisonsList(wordLists, secConj, troiConj);
            logger.log(Level.FINE, "secConj: {0}", secConj);
            logger.log(Level.FINE, "troiConj: {0}", troiConj);
        }
        logger.log(Level.FINE, "<< NULL");
        return null;
    }

    private void setConjugaisonsList(WordLists wordLists, String secConj, String troiConj) {
        logger.log(Level.FINE, "**** setConjugaisonsList");
        if (wordLists.getConjugaisons().size() > 1)// 6
            wordLists.getConjugaisons().set(1, secConj);
        if (wordLists.getConjugaisons().size() > 2)// 7
            wordLists.getConjugaisons().set(2, troiConj);
    }

    // Applique l'actualisation, ajoute les determinants devant les deixis en
    // fonction du temps
    private void applyingActualisation(ArrayList<String> phrase, String[] actu, WordLists wordLists, String[] distrub) {
        // int count = 0;
        logger.log(Level.FINE, "applyingActualisation: ****************************************");
        for (int i = 1; i < phrase.size(); i++) {
            String mot = phrase.get(i);
            // if (wordLists.getNoms().contains(mot)) {
            for (int countAct = 0; countAct < 3; countAct++) {
                logger.log(Level.WARNING, ">> DISTUBUTION: {0}", distrub[countAct]);
                logger.log(Level.WARNING, ">> MOT PHRASE: {0}", mot);
                logger.log(Level.WARNING, ">> ACTUALISATION: {0}", actu[countAct]);
                if (!isNullOrVideString(distrub[countAct], mot, actu[countAct])
                        && (mot.replaceAll(" ", "").equals(distrub[countAct].replaceAll(" ", "")))) {

                    String act = actu[countAct];
                    logger.log(Level.WARNING, " actualisation ligne: {0} ", act);

                    String[] parsedArray = act.split("\\+");

                    int randomNum = ThreadLocalRandom.current().nextInt(0, parsedArray.length);

                    logger.log(Level.WARNING, "add actualisation: {0}", parsedArray[randomNum]);
                    phrase.add(i, parsedArray[randomNum]);
                    i++;
                    break;
                } else {
                    logger.log(Level.WARNING, ">>>>>>>>>>>>>>>>>> CONDITION FAILED <<<<<<<<<<<<<<<<<<<<<<<");
                }
            }
            // }
        }
    }

    // Actualisation, va ajouter des determinants devant les deixis si il y en a
    private String actualisation(ArrayList<String> phrase, Lexique lexique, WordLists wordLists, String erreur) {
        logger.log(Level.FINE, "actualisation: ****************************************");

        if (erreur != null) {
            logger.log(Level.WARNING, "<< ERREUR");
            return erreur;
        }

        Actualisation actualisation = lexique.getActualisation();
        logger.log(Level.WARNING, ">> ACTUALISAITON : {0} ", actualisation.getIdentifiant());
        String[] actu = actualisation.getAxisAsArray();
        Distribution distribution = lexique.getDistribution();
        String[] distrub = distribution.getAxisAsArray();
        logger.log(Level.WARNING, ">> DISTRUBUTION : {0} ", distribution.getCode());
        if (actu[0] != null || actu[1] != null || actu[2] != null) {// 4
            applyingActualisation(phrase, actu, wordLists, distrub);

            intentReponse = "";
            List<String> reponseIntent = new ArrayList<>();

            for (String phr : phrase) {
                if (!isNullOrVideString(phr)) {
                    reponseIntent.add(phr);
                }
            }

            int sizeIntentResponse = reponseIntent.size();
            if (sizeIntentResponse > 0) {
                intentReponse = reponseIntent.toString();
            }

            logger.log(Level.INFO, "Phrase after applyingActualisation : {0} ", phrase.toString());
        } else {
            logger.log(Level.WARNING, ">> ALL ACTUALISATION NULL");
        }

        long debut = System.currentTimeMillis();
        String isNotOK = applyDistribution(distribution, phrase, wordLists);
        logger.log(Level.INFO, "Phrase after applyDistribution : {0} ", phrase.toString());

        if (isNotOK != null) {// 1
            logger.log(Level.WARNING, "<< ERREUR");
            return isNotOK + " [CATEGORIE_LEXIQUE:" + lexique.getCategorie() + "] [IDENTIFIANT_ACTUALISATION:"
                    + actualisation.getIdentifiant() + " ] ";

        }

        logger.log(Level.INFO, "TIME: actualisation : {0} ms", System.currentTimeMillis() - debut);
        logger.log(Level.WARNING, "<< NULL");
        return null;
    }

    // Va conjuguer chaque verbe dans la phrase a la conjugaison qui lui est propre
    private String conjugaison(ArrayList<String> phrase, Lexique lexique, String temps, WordLists wordLists,
            String erreur) {
        logger.log(Level.FINE, "conjugaison: ****************************************");
        if (erreur != null) {
            logger.log(Level.WARNING, "<< ERREUR {0}", erreur);
            return erreur;
        }
        long debut = System.currentTimeMillis();
        ArrayList<String> verbes = new ArrayList<>();
        ArrayList<String> verbesPreconj = new ArrayList<>();
        String vSup = lexique.getConstruction().getVsup();
        verbes.add(vSup);
        if (stringEqualOR(lexique.getTypes(), "verbe", "verbe_compose"))// 1
            verbes.add(lexique.getValeur());

        verbes.addAll(wordLists.getVerbes());

        logger.info(">> verbes a conjuguer: " + verbes);


//        Nettoyage de la liste des verbes pour suprimer les elements vides
//        Start Region
        logger.info(">>> verbes.size() avant nettoyage ===>  " + verbes.size());
        int sizeListVerbes = verbes.size();
        while (sizeListVerbes > 0){
            if(verbes.get(sizeListVerbes - 1).equals("") || verbes.get(sizeListVerbes - 1).equals(" ")){
                logger.info("====================> ICI un verbe vide <========================");
                verbes.remove(sizeListVerbes- 1);

            }
            sizeListVerbes--;
        }
        logger.info(">> verbes a conjuguer apres nettoyage : " + verbes);

        logger.info(">>> verbes.size() apres nettoyage ===>  " + verbes.size());

//        End Region

        // laisser un seul verbe sur la liste des verbes
//        Start Region
        while (verbes.size()>1){
            verbes.remove(verbes.size()-1);
        }
        logger.info(">> verbes a conjuguer apres supression des autes verbes  " + verbes);
//        End Region

        if (verbes.isEmpty()) { // 2
            logger.log(Level.WARNING, "<< NULL");
            return null;
        }

        int conjPtr = 0;

        for (int i = 0; i < phrase.size(); i++) {// 3
            if (verbes.contains(phrase.get(i)) && !phrase.get(i).equals("")) {// 4
                String verb = conjugue(phrase.get(i), wordLists.getConjugaisons().get(conjPtr), temps);
                logger.log(Level.INFO, "VERBE  CONJUGED : {0}", verb);
                if (verb.equals("ERREUR"))// 5
                    continue;

                if (verb.contains("ERREUR CONJUGAISON")) {// 6
                    logger.log(Level.INFO, "conjugaison return : {0}", verb);
                    return verb;
                }
                verbesPreconj.add(verb);
                phrase.set(i, verb);
                conjPtr++;
            }
        }
        logger.info("La liste des mots de la phrase apres conjugaison " + phrase);

        verbesPreconj.addAll(wordLists.getVerbesConj());
        wordLists.getVerbesConj().clear();
        wordLists.getVerbesConj().addAll(verbesPreconj);

        logger.log(Level.INFO, "TIME: conjugaison : {0} ms", System.currentTimeMillis() - debut);
        logger.log(Level.WARNING, "<< NULL");
        return null;
    }

    // Renvoie une Forme d'un tableau de Forme si c est un nom
    private Formes getForme(Formes[] formes) {
        logger.log(Level.WARNING, "*** getForme");
        for (Formes forme : formes) {
            if (forme.getCatgram().equalsIgnoreCase("NOM")) {
                logger.log(Level.WARNING, "<< FROM : {0}", forme.getForme());
                return forme;
            }
        }
        logger.log(Level.WARNING, "<< NULL");
        return null;
    }

    // va changer le determinant devant un nom Feminin
    private int replaceDetF(ArrayList<String> phrase, int i) {
        logger.log(Level.WARNING, "*** replaceDetF");
        long debut = System.currentTimeMillis();
        int counter = 0;
        String lastMot = phrase.get(i - 1);
        if (lastMot.equalsIgnoreCase("du")) {
            phrase.set(i - 1, "de");
            phrase.add(i, "la");
            ++counter;
        } else if (lastMot.equalsIgnoreCase("le"))
            phrase.set(i - 1, "la");
        else if (lastMot.equalsIgnoreCase("un"))
            phrase.set(i - 1, "une");

        logger.log(Level.INFO, "TIME: replaceDetF : {0} ms", System.currentTimeMillis() - debut);
        return counter;
    }

    // va changer le determinant devant un nom au pluriel
    private void replaceDetP(ArrayList<String> phrase, int i) {
        logger.log(Level.WARNING, "*** replaceDetP");
        long debut = System.currentTimeMillis();
        if (phrase.get(i - 1).equalsIgnoreCase("du"))
            phrase.set(i - 1, "des");
        else if (phrase.get(i - 1).equalsIgnoreCase("votre"))
            phrase.set(i - 1, "vos");
        else if (phrase.get(i - 1).equalsIgnoreCase("le"))
            phrase.set(i - 1, "les");
        else if (phrase.get(i - 1).equals("à"))
            phrase.set(i - 1, "aux");

        logger.log(Level.INFO, "TIME: replaceDetP : {0} ms", System.currentTimeMillis() - debut);
    }

    // va accorder les determinants avec leurs noms et les adjectifs aussi
    private String accord(ArrayList<String> phrase, WordLists wordLists) {
        logger.log(Level.WARNING, "accord ************************************");
        long debut = System.currentTimeMillis();
        logger.info("noms :" + wordLists.getNoms().toString());
        int counter = 0;
        int tmp = 0;
        // Boucles pour changer les determinants
        for (int i = 0; i < phrase.size(); i = tmp) {
            counter = 0;
            tmp = i;
            if (wordLists.getNoms().contains(phrase.get(tmp))) {
                ArgumentCompose args[] = argumentComposeRepository.findByForme(phrase.get(tmp));
                logger.log(Level.WARNING, "FIND ArgumentCompose for: {0} ", phrase.get(tmp));
                logger.log(Level.WARNING, "SIZE: {0} ", args.length);
                if (args != null && args.length > 0) {// IF IS ARGUMENT COMPOSE
                    int randomNum = ThreadLocalRandom.current().nextInt(0, args.length);
                    ArgumentCompose arg = args[randomNum];
                    logger.log(Level.WARNING, "USED ArgumentCompose ID: {0} ", arg.getId());
                    logger.log(Level.WARNING, "USED NUMBER: {0} ", arg.getNu());
                    logger.log(Level.WARNING, "USED GENDER: {0} ", arg.getGe());
                    logger.log(Level.WARNING, "USED FORM: {0} ", arg.getCa());
                    if (arg != null) {

                        if (arg.getNu().equalsIgnoreCase("P")) {
                            logger.log(Level.WARNING, ">>> replaceDetP ARG PLURIEL");
                            replaceDetP(phrase, tmp);
                        }

                        if (arg.getGe().equalsIgnoreCase("F")) {
                            logger.log(Level.WARNING, ">>> replaceDetP ARG FEMININ");
                            counter = replaceDetF(phrase, tmp);
                        }

                        tmp += counter;
                    }
                }
                Nslemme[] names = nslemmeRepository.findByLemme(phrase.get(tmp));
                if (names.length > 0) {
                    Nslemme name = names[0];

                    if (name.getCatgram().equalsIgnoreCase("nf")) {
                        counter = replaceDetF(phrase, tmp);
                        tmp += counter;
                    }
                }
                Formes[] formes = formesRepository.findByFormeAndCatgram(phrase.get(tmp), "NOM");
                Formes forme = getForme(formes);
                if (forme != null && (forme.getNumber().equalsIgnoreCase("P"))) {
                    logger.log(Level.WARNING, ">>> replaceDetP FORM PLURIEL : {0} ", forme.getFormeId());
                    replaceDetP(phrase, tmp);
                }

            }
            ++tmp;
        }

        String nom = "";
        // Boucle pour changer les adjectifs
        for (int i = 0; i < phrase.size(); i++) {
            if (wordLists.getNoms().contains(phrase.get(i)))
                nom = phrase.get(i);
            if (wordLists.getAdjectifs().contains(phrase.get(i)) && !nom.equals("")) {
                logger.log(Level.INFO, "According {0}", phrase.get(i) + " with " + nom);
                Formes[] names = formesRepository.findByFormeAndCatgram(nom, "NOM");
                ArgumentCompose arg = null;
                Formes name = null;
                if (names.length == 0) {
                    arg = argumentComposeRepository.findOneByForme(nom);
                } else {
                    name = names[0];
                }
                if ((name != null && name.getGender().equalsIgnoreCase("F"))
                        || (arg != null && arg.getGe().equalsIgnoreCase("F"))) {
                    String adjectif = phrase.get(i);
                    Alemme adj = alemmeRepository.findOneByLemme(adjectif);
                    if (adj != null) {
                        Integer rad = Integer.parseInt(adj.getAcode().getRad());
                        String radical = adjectif.substring(0, adjectif.length() - rad);
                        adjectif = radical + adj.getAcode().getFs();
                        phrase.set(i, adjectif);
                    }
                }
            }
        }

        logger.log(Level.INFO, "TIME: Accord : {0} ms", System.currentTimeMillis() - debut);
        return null;
    }

    // Revoie l'index de mot precedent dans une liste (utile lorsque la liste est
    // remplie de sting vide ou null)
    private int getPreviousWordIndex(int i, ArrayList<String> phrase) {
        logger.log(Level.WARNING, "*** getPreviousWordIndex");
        int j = i - 1;
        while (phrase.get(j).equals("") || phrase.get(j).equals(" ") || phrase.get(j) == null) {
            --j;
        }
        return j;
    }

    // Ajoute "ne" et "pas" au bon endroit dans la phrase
    private void negation(ArrayList<String> phrase, WordLists wordLists) {
        logger.log(Level.WARNING, "negation *****************************************");
        if (!phrase.contains("Non")) {// 1
            return;
        }
        long debut = System.currentTimeMillis();
        int conjPtr = 0;
        for (int i = 0; i < phrase.size(); ++i) {// 2
            int finalI = i;
            if (wordLists.getVerbesConj().stream().anyMatch(str -> str.equals(phrase.get(finalI)))
                    || (i > 0 && wordLists.getVerbesConj().stream()
                            .anyMatch(str -> str.equals(phrase.get(finalI - 1) + " " + phrase.get(finalI))))) {// 3/4/5
                if (wordLists.getConjugaisons().get(conjPtr).equals("inf__")) {// 6
                    phrase.add(i, "ne");
                    phrase.add(i + 1, "pas");
                    ++i;
                } else {// 7
                    int j = getPreviousWordIndex(i, phrase);
                    if (phrase.get(j).equals("a")) {// 8
                        phrase.add(j, "ne");
                        phrase.add(j + 2, "pas");
                    } else {// 9
                        phrase.add(i, "ne");
                        phrase.add(i + 2, "pas");
                    }
                    ++i;
                }
                ++conjPtr;
            }
        }
        logger.log(Level.INFO, "TIME: Negation : {0} ms", System.currentTimeMillis() - debut);
    }

    // Renvoie un pronom aleatoire pour un deixis
    private String getPronom(Deixis deixis, ArrayList<String> conjugaisons) {
        logger.log(Level.WARNING, "*** getPronom");
        String[] pronoms = deixis.getValeur().split(";");
        int randomNum = ThreadLocalRandom.current().nextInt(0, pronoms.length);

        conjugaisons.add(deixis.getCategorieMorphologique().split(";")[randomNum]);
        return pronoms[randomNum];
    }

    // Renvoie un nom/groupe nominal aleatoire pour un deixis
    private String getGN(String x, WordLists wordLists) {
        logger.log(Level.WARNING, "*** getGN");
        String[] noms = x.split("\\+");
        int nomRand = ThreadLocalRandom.current().nextInt(0, noms.length);
        wordLists.getNoms().add(noms[nomRand]);
        return noms[nomRand];
    }

    public boolean clearAndCompare(String st1, String st2) {
        return clearMot(st1).equals(st2);
    }

    public String clearMot(String st1) {
        return st1.replaceAll("\\s+", "");
    }

    // Applique toutes les regex de la table morphologique et applique les regles
    // speciales
    private String ajustements(ArrayList<String> phrase) {
        logger.log(Level.WARNING, "ajustements *************************************");
        long debut = System.currentTimeMillis();

        String phraseStr = String.join(" ", phrase);
        logger.log(Level.WARNING, "Befor Ajustement {0} ", phraseStr);
        phraseStr = beforAjust(phraseStr, false).toLowerCase();
        List<String> phraseAL = new ArrayList<>(Arrays.asList(phraseStr.split("\\s+")));

        // AJUSTEMENT SYNTAXIQUE

        List<RegleAjustementSyntaxique> listRegleSyntaxiques = regleAjustementSyntaxiqueRepository
                .findAllByOrderByOrdreAsc();
        logger.log(Level.WARNING, "+++++ start Ajustement syntaxique +++++");
        long debutRegleAjustement = System.currentTimeMillis();

        // AJUSTEMENT SYNTAXIQUE A DISTANCE
        for (RegleAjustementSyntaxique regleSyntaxique : listRegleSyntaxiques) {
            phraseAL = startAjustementDynamique(phraseStr, phraseAL, regleSyntaxique);
            phraseStr = beforAjust(String.join(" ", phraseAL), false);
        }

        logger.log(Level.INFO, "TIME: end Ajustement syntaxique: {0} ms",
                System.currentTimeMillis() - debutRegleAjustement);

        debutRegleAjustement = System.currentTimeMillis();

        List<RegleAjustementMorphologique> listRegleMorphologique = regleAjustementMorphologiqueRepository
                .findAllByOrderByOrdreAsc();
        logger.log(Level.WARNING, "++++ start Ajustement Morphologique ++++");
        // AJUSTEMENT MORPHOLOGIQUE A DISTANCE
        for (RegleAjustementMorphologique regleMorph : listRegleMorphologique) {
            phraseAL = startAjustementDynamique(phraseStr, phraseAL, regleMorph);
            phraseStr = beforAjust(String.join(" ", phraseAL), false);
        }
        logger.log(Level.INFO, "TIME: end Ajustement Morphologique: {0} ms",
                System.currentTimeMillis() - debutRegleAjustement);

        // CAS SPECIAUX
        phraseStr = phraseStr.replaceAll("' ", "'");

        logger.log(Level.INFO, "AFTER AJUSTEMENT: {0}", phraseStr);

        logger.log(Level.INFO, "TIME: ajustement Dynamique : {0} ms", System.currentTimeMillis() - debut);
        return phraseStr;
    }

    private String[] getListRegleTo(String[] stringActionFrom, String stringActionTo) {
        String[] splitTo;
        if (stringActionTo.equals("")) {// 7
            splitTo = new String[0];
        } else {// 8
            splitTo = beforAjust(stringActionTo, true).split("\\s+");
        }

        int sizeSplitTo = getMaxList(stringActionFrom, splitTo);
        String[] finalTo = new String[sizeSplitTo];
        for (int i = 0; i < sizeSplitTo; i++) {
            if (i < splitTo.length) {
                finalTo[i] = splitTo[i];
            } else {
                finalTo[i] = "";
            }
        }
        return finalTo;
    }

    private int getSizeRegleInPhrase(List<String> phrase, String[] regles) {

        String mot = null;
        for (String reg : regles) {
            if (reg != null && !reg.startsWith("$")) {
                mot = reg;
                break;
            }
        }
        int sizeElement = 0;
        if (mot != null) {
            for (String motPhrase : phrase) {
                if (motPhrase != null && motPhrase.equalsIgnoreCase(mot)) {
                    sizeElement++;
                }
            }
        }

        return sizeElement;
    }

    private String beforAjust(String str, boolean isRegleOrAction) {
        if (isRegleOrAction) {
            str = str.replaceAll("'\\$", "' \\$");
        }

        return str.replaceAll(" d'", " d' ").replaceAll(" l'", " l' ").replaceAll(" n'", " n' ")
                .replaceAll("\\.", " \\. ").replaceAll(",", " , ").replaceAll(";", " ; ").replaceAll("\\s+", " ");
    }

//   DEBUT EXPLICATION DE FONCTION D'APPLICATION D'AJUSTEMENT
//   cette fonction sert à appliquer les regles d'ajustement syntaxique et morphologique :

    private List<String> startAjustementDynamique(String phrase, List<String> phraseList,
            AjustementDynamique ajustementDynamique) {

        String regleList = beforAjust(ajustementDynamique.getRegle(), true);
        if (isSimpleRegle(regleList)) {// 1
            String regleRempalcement = beforAjust(ajustementDynamique.getAction(), true);
            phrase = ' ' + phrase + ' ';
            String remplacement = phrase.replaceAll(' ' + regleList + ' ', ' ' + regleRempalcement + ' ');
            return Arrays.asList(remplacement.substring(1, remplacement.length() - 1).split("\\s+"));

        }

        // il contient un code $
        String[] listMotRegleFrom = regleList.split("\\s+");
        int occurenceOfRege = getSizeRegleInPhrase(phraseList, listMotRegleFrom);

        int sizePhrase = phraseList.size();
        if (listMotRegleFrom.length > sizePhrase) {// 2
            return phraseList;
        }
        int indexRegle;
        int regleOK = 0;
        for (int indexMotInit = 0; indexMotInit < sizePhrase && regleOK < occurenceOfRege; indexMotInit++) {// 3/4

            HashMap<String, String> reglePosition = new HashMap<>();
            indexRegle = 0;
            int maxPhraseIntervalle = (indexMotInit + listMotRegleFrom.length) % (sizePhrase + 1);
            for (int indexMotIntervalle = indexMotInit; indexMotIntervalle < maxPhraseIntervalle
                    && regleOK < occurenceOfRege; indexMotIntervalle++) {// 5/6
                String regle = listMotRegleFrom[indexRegle];
                String mot = phraseList.get(indexMotIntervalle);

                if (regleVerification(mot, regle)) {// 7
                    reglePosition.put(regle, mot);
                    if ((indexMotIntervalle + 1) == maxPhraseIntervalle) {// 8
                        // c'est bon toute la régle est verifié
                        String stringActionTo = beforAjust(ajustementDynamique.getAction(), true);
                        String[] listMotRegleTo = getListRegleTo(listMotRegleFrom, stringActionTo);
                        phraseList = appliqueAjusts(indexMotInit, phraseList, reglePosition, listMotRegleFrom,
                                listMotRegleTo);
                        sizePhrase = phraseList.size();
                        regleOK++;
                    }
                    indexRegle++;

                } else {// 9 la régle n'est pas respectée
                    break;
                }

            }
        }

        return phraseList;

    }

//    FIN EXPLICATION DE FONCTION D'APPLICATION D'AJUSTEMENT

    private int getMaxList(String[] st1, String[] st2) {
        return st1.length > st2.length ? st1.length : st2.length;
    }

    private List<String> appliqueAjusts(int indexDebut, List<String> phraseList, HashMap<String, String> reglePosition,
            String[] listMotRegleFrom, String[] listMotRegleTo) {
        int indexRegle = 0;
        int sizeTo = getMaxList(listMotRegleFrom, listMotRegleTo);
        int add = 1;
        for (int i = indexDebut; i < (indexDebut + sizeTo); i += add) {// 1
            add = 1;
            String remplace;
            if (isSimpleRegle(listMotRegleTo[indexRegle])) {// 2
                remplace = listMotRegleTo[indexRegle];
            } else {// 3
                if (!listMotRegleTo[indexRegle].equals("")) {// 4
                    remplace = reglePosition.get(listMotRegleTo[indexRegle]);
                } else {// 5
                    remplace = "";
                }
            }

            phraseList.set(i, remplace);

            indexRegle++;
        }
        return phraseList;
    }

    boolean isSimpleRegle(String regle) {
        return !regle.contains("$");
    }

    boolean stringEqualOR(String strToCompare, String... strWith) {

        for (String s : strWith) {
            if (s == null) {
                return strToCompare == null;
            } else if (strToCompare.equals(s)) {
                return true;
            }
        }

        return false;
    }

    boolean stringEqualIgnorOR(String strToCompare, String... strWith) {

        for (String s : strWith) {
            if (s == null) {
                return (strToCompare == null);
            } else if (strToCompare.equalsIgnoreCase(s)) {
                return true;
            }
        }

        return false;
    }

    boolean stringStartWithOR(String strToCompare, String... strWith) {

        for (String s : strWith) {
            if (strToCompare.startsWith(s)) {
                return true;
            }
        }

        return false;
    }

    private boolean regleAvoirOuEtre(String mot, String regle) {

        if (regle.startsWith("$etre")) {// 6
            return stringEqualIgnorOR(mot, "est", "sont", "suis", "es", "sommes", "êtes");
        }

        if (regle.startsWith("$avoir")) {// 7
            return stringEqualIgnorOR(mot, "ai", "as", "a", "avons", "avez", "ont");
        }

        return false;
    }

    private boolean relgeIlOrLE(String mot, String regle) {

        if (regle.startsWith("$il")) {// 2
            return stringEqualIgnorOR(mot, "il", "elle", "ils", "elles");
        }

        if (regle.startsWith("$le")) {// 3
            return stringEqualIgnorOR(mot, "le", "la", "les", "l'");
        }
        return false;
    }

    private boolean regleVerification(String mot, String regle) {
        String clearMot = clearMot(mot);

        if (regle.startsWith("$any")) {// 1
            return true;
        }

        if (stringStartWithOR(regle, "$il", "$le")) {// 2
            return relgeIlOrLE(clearMot, regle);
        }

        if (regle.startsWith("$un")) {// 3
            return stringEqualIgnorOR(clearMot, "un", "une", "des");
        }

        if (regle.startsWith("$det")) {// 3
            Determinant det = determinantRepository.findOneByDet(clearMot);
            return det != null;
        }

        if (stringStartWithOR(regle, "$avoir", "$etre")) {// 4
            return regleAvoirOuEtre(clearMot, regle);
        }

        if (regle.startsWith("$hna")) {// 5
            return hNonAspire.contains(clearMot);
        }

        if (regle.startsWith("$argcompose")) {// 6
            ArgumentCompose arg = argumentComposeRepository.findOneByForme(clearMot);
            return arg != null;
        }

        if (stringStartWithOR(regle, "$verbe", "$vrb", "$voyelle", "$nom", "$feminin", "$masculin", "$pluriel")) {// 7
            return formatAndForRegle(regle, clearMot);

        }

        return clearAndCompare(mot, regle);
    }

    boolean formatAndForRegle(String regle, String mot) {
        String[] spliterRegleInterne = regle.split("\\$");// fixe bug split $ not work
        if (spliterRegleInterne.length <= 2) {
            return isFormatSpecialValide(regle, mot);
        } else {
            formsSpecial = null;
            for (String splitElement : spliterRegleInterne) {
                if (!splitElement.equals("") && !isFormatSpecialValide("$" + splitElement, mot)) {
                    return false;
                }
            }
            return true;
        }
    }

    boolean isFormatSpecialValide(String regle, String clearMot) {

        if (regle.startsWith("$voyelle")) {
            clearMot = Normalizer.normalize(clearMot, Normalizer.Form.NFKD);
            logger.log(Level.INFO, "AFTER Normalizer NFKD {0}", clearMot);
            return stringStartWithOR(clearMot, "e", "a", "y", "o", "u", "i");
        }

        if (stringStartWithOR(regle, "$verbe", "$vrb")) {
            formsSpecial = formesRepository.findByFormeAndCatgram(clearMot, "VRB");
            return formsSpecial.length > 0;
        }

        if (regle.startsWith("$nom")) {
            formsSpecial = formesRepository.findByFormeAndCatgram(clearMot, "NOM");

            return formsSpecial.length > 0;
        }

        if (formsSpecial == null) {
            formsSpecial = formesRepository.findByForme(clearMot);
        }

        if (formsSpecial.length > 0) {
            if (regle.startsWith("$feminin")) {
                return formsSpecial[0].getGender().equals("F");
            }

            if (regle.startsWith("$masculin")) {
                return formsSpecial[0].getGender().equals("M");
            }

            if (regle.startsWith("$pluriel")) {
                return formsSpecial[0].getNumber().equals("P");
            }

        }

        return clearAndCompare(clearMot, regle);
    }

    // Retourne le temps formater
    private String getTemps(String temps) {
        logger.log(Level.WARNING, "**** getTemps");
        switch (temps) {
            case "futur":
                return "Fut";
            case "passé":
                return "Ps";
            default:
                return "Pr";
        }
    }

    // Conjuge un verbe a la personne et au temps demander
    private String conjugue(String verbe, String conjugaison, String temps) {
        logger.log(Level.WARNING, "**** conjugue");
        long debut = System.currentTimeMillis();
        String conj = "ind_";

        VerbeCompose[] vbCompose = verbeComposeRepository.findByForm(verbe);

        if (!isArrayNullOrVide(vbCompose)) {// 1
            int randomNum = ThreadLocalRandom.current().nextInt(0, vbCompose.length);
            logger.log(Level.INFO, "conjugaison a la: {0}", conj + getTemps(temps) + "_" + conjugaison);
            String[] personAndNumber = conjugaison.split("_");
            VerbeCompose[] vbComposeConj = verbeComposeRepository.findByLemAndTenAndNumAndPer(
                    vbCompose[randomNum].getLem(), conj + getTemps(temps), personAndNumber[1], personAndNumber[0]);
            if (vbComposeConj.length > 0) {// 2
                randomNum = ThreadLocalRandom.current().nextInt(0, vbComposeConj.length);
            } else {// 3
                logger.log(Level.INFO, "conjugue return : {0}", "ERREUR CONJUGAISON [" + conj + getTemps(temps) + "_"
                        + conjugaison + "] VERBE:" + vbCompose[randomNum].getLem());
                return "ERREUR CONJUGAISON [" + conj + getTemps(temps) + "_" + conjugaison
                        + "] VERBE COMPOSÉ NON TROUVÉ:'" + vbCompose[randomNum].getLem() + "'";
            }

            return vbComposeConj[randomNum].getForm();
        }

        conj += getTemps(temps) + "_";
        Map<String, Object> vscode;
        String prep = "";

        // Verifie la presence de preposition (ex: y avoir,...)
        if (verbe.contains("y ")) {// 4
            verbe = verbe.substring(2);
            prep = "y ";
        }

        if (verbe.equals("etre")) {// 5
            vscode = etre;
        } else {// 6
            Vslemme vslemme = vslemmeRepository.findOneByLemme(verbe);
            ObjectMapper oMapper = new ObjectMapper();
            if (vslemme == null)// 7
                return "ERREUR";
            vscode = oMapper.convertValue(vslemme.getVscode(), Map.class);
        }
        Long rad = (Long) vscode.get("rad");

        String radical = verbe.substring(0, verbe.length() - toIntExact(rad));
        verbe = radical + vscode.get(conj + conjugaison);
        logger.log(Level.INFO, "TIME: conjugue : {0} ms", System.currentTimeMillis() - debut);
        return prep + verbe;
    }

    // Remplace les donnee variables
    private String replaceVariable(ArrayList<String> phrase, String prevPredicat, String erreur) {
        logger.log(Level.WARNING, "**** replaceVariable");
        if (erreur != null) {
            return erreur;
        }
        long debut = System.currentTimeMillis();
        replaceHeure(phrase);
        replaceJour(phrase);
        replaceDate(phrase);
        replaceOuiNon(phrase, prevPredicat);
        logger.log(Level.INFO, "TIME: replaceVariable : {0} ms", System.currentTimeMillis() - debut);
        return null;
    }

    private void replaceHeure(ArrayList<String> phrase) {
        String toReplace = "%heure%";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String time = sdf.format(cal.getTime());
        Collections.replaceAll(phrase, toReplace, time);
    }

    private void replaceDate(ArrayList<String> phrase) {
        String toReplace = "%temporalite_date%";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String time = sdf.format(cal.getTime());
        Collections.replaceAll(phrase, toReplace, time);
    }

    private void replaceOuiNon(ArrayList<String> phrase, String prevPredicat) {
        String toReplace = "%oui/non%";
        String summary = pragmatiqueService.getMeteo();
        prevPredicat = prevPredicat.substring(prevPredicat.indexOf('_') + 1).toLowerCase();
        String replaceBy = "";

        if (pragmatiqueService.checkMeteo(summary, prevPredicat)) {
            replaceBy = "Oui";
        } else {
            replaceBy = "Non";
        }
        Collections.replaceAll(phrase, toReplace, replaceBy);
    }

    private void replaceJour(ArrayList<String> phrase) {
        String toReplace = "%jour%";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String time = sdf.format(cal.getTime());
        Collections.replaceAll(phrase, toReplace,
                cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.FRANCE) + " " + time);

    }

}
