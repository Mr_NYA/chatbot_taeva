package org.teamnet.unitext.application.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "qa_logs")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
public class QALogs implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date")
    Date date;

    @Column(name = "ip_request")
    String ipRequest;

    @OneToOne
    @JoinColumn(name = "id_client", referencedColumnName = "id")
    Client idClient;

    @Column(name = "input_message")
    String inputMessage;

    @Lob
    @Column(name = "intent")
    String intent;

    @Lob
    @Column(name = "intent_reponse")
    String intentReponse;

    @Column(name = "response")
    String response;

    @Column(name = "valide")
    boolean valide;

    @Column(name = "logs_details")
    String logsDetails;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInputMessage() {
        return this.inputMessage;
    }

    public void setInputMessage(String inputMessage) {
        this.inputMessage = inputMessage;
    }

    public boolean getValide() {
        return valide;
    }

    public void setValide(Boolean valide) {
        this.valide = valide;
    }

    public String getIntentReponse() {
        return this.intentReponse;
    }

    public void setIntentReponse(String intentReponse) {
        this.intentReponse = intentReponse;
    }

    public String getIntent() {
        return this.intent;
    }

    public void setIntent(String intent) {
        this.intent = intent;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getResponse() {
        return this.response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getIpRequest() {
        return ipRequest;
    }

    public void setIpRequest(String ipRequest) {
        this.ipRequest = ipRequest;
    }

    public Client getIdClient() {
        return idClient;
    }

    public void setIdClient(Client idClient) {
        this.idClient = idClient;
    }

    public boolean isValide() {
        return valide;
    }

    public void setValide(boolean valide) {
        this.valide = valide;
    }

    public String getLogsDetails() {
        return this.logsDetails;
    }

    public void setLogsDetails(String logsDetails) {
        this.logsDetails = logsDetails;
    }

}
