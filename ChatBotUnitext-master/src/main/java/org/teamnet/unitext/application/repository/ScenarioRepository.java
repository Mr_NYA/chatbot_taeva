package org.teamnet.unitext.application.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.teamnet.unitext.application.domain.Scenario;

@Repository
public interface ScenarioRepository extends JpaRepository<Scenario, Long> {

    Scenario findOneById(long id);

    Scenario findOneByCategorie(String categorie);

    List<Scenario> findByCategorie(String categorie);

    Scenario findOneByCategorieIgnoreCaseAndIdParent(String categorie, long idParent);

    List<Scenario> findByCategorieAndIdParent(String categorie, long idParent);

    Scenario findOneByCategorieAndIdParent(String categorie, long idParent);

    Scenario findOneByIdAndIdParent(int id, long idParent);
}
