package org.teamnet.unitext.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.teamnet.unitext.application.domain.Alemme;

public interface AlemmeRepository extends JpaRepository<Alemme, String> {

    Alemme findOneByLemme(String lemme);
}
