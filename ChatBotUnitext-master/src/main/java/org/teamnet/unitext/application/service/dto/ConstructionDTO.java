package org.teamnet.unitext.application.service.dto;

public class ConstructionDTO {

    private Long id;

    String identifiant;
    String type_construction;
    String vsup;
    String prep;
    String det;
    String prep0;
    String x0;
    String prep1;
    String x1;
    String prep2;
    String x2;
    String prep3;
    String x3;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public String getType_construction() {
        return type_construction;
    }

    public void setType_construction(String type_construction) {
        this.type_construction = type_construction;
    }

    public String getVsup() {
        return vsup;
    }

    public void setVsup(String vsup) {
        this.vsup = vsup;
    }

    public String getPrep() {
        return prep;
    }

    public void setPrep(String prep) {
        this.prep = prep;
    }

    public String getDet() {
        return det;
    }

    public void setDet(String det) {
        this.det = det;
    }

    public String getPrep0() {
        return prep0;
    }

    public void setPrep0(String prep0) {
        this.prep0 = prep0;
    }

    public String getX0() {
        return x0;
    }

    public void setX0(String x0) {
        this.x0 = x0;
    }

    public String getPrep1() {
        return prep1;
    }

    public void setPrep1(String prep1) {
        this.prep1 = prep1;
    }

    public String getX1() {
        return x1;
    }

    public void setX1(String x1) {
        this.x1 = x1;
    }

    public String getPrep2() {
        return prep2;
    }

    public void setPrep2(String prep2) {
        this.prep2 = prep2;
    }

    public String getX2() {
        return x2;
    }

    public void setX2(String x2) {
        this.x2 = x2;
    }

    public String getPrep3() {
        return prep3;
    }

    public void setPrep3(String prep3) {
        this.prep3 = prep3;
    }

    public String getX3() {
        return x3;
    }

    public void setX3(String x3) {
        this.x3 = x3;
    }

}
