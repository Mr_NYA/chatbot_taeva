package org.teamnet.unitext.application.service.dto;


import org.teamnet.unitext.application.domain.QALogs;

import java.util.Date;

public class QALogsDTO {


    private Long id;
    Date date;
    String inputMessage;
    String intent;
    String response;
    boolean valide;
    String logsDetails;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInputMessage() {
        return this.inputMessage;
    }

    public void setInputMessage(String inputMessage) {
        this.inputMessage = inputMessage;
    }

    public boolean getValide() {
        return valide;
    }

    public void setValide(Boolean valide) {
        this.valide = valide;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getResponse() {
        return this.response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getIntent() {
        return this.intent;
    }

    public void setIntent(String intent) {
        this.intent = intent;
    }

    public String getLogsDetails() {
        return this.logsDetails;
    }

    public void setLogsDetails(String logsDetails) {
        this.logsDetails = logsDetails;
    }

    public QALogsDTO(QALogs qaLogs)
    {
        this.inputMessage = qaLogs.getInputMessage();
        this.response = qaLogs.getResponse();
        this.intent = qaLogs.getIntent();
        this.date = qaLogs.getDate();
        this.valide = qaLogs.getValide();
       this.logsDetails = qaLogs.getLogsDetails();

    }

}

