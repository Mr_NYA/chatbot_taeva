package org.teamnet.unitext.application.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.teamnet.unitext.application.domain.QASenior;


@Repository
public interface QASeniorRepository  extends JpaRepository<QASenior, Long> {

    QASenior findOneByInputMessage(String inputMessage);


}

