package org.teamnet.unitext.application.service.dto;

public class DistributionDTO {

    private Long id;

    String code;
    int nb_param;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getNb_param() {
        return nb_param;
    }

    public void setNb_param(int nb_param) {
        this.nb_param = nb_param;
    }

    public DistributionDTO(DistributionDTO qaSenior)
    {
        this.id = qaSenior.getId();
        this.code = qaSenior.getCode();
        this.nb_param = qaSenior.getNb_param();
    }
}
