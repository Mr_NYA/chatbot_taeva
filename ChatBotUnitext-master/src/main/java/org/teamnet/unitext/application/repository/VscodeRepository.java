package org.teamnet.unitext.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.teamnet.unitext.application.domain.QASenior;
import org.teamnet.unitext.application.domain.Vscode;

import java.util.HashMap;

@Repository
public interface VscodeRepository extends JpaRepository<Vscode, String> {

    HashMap findOneByCode(String code);
}
