package org.teamnet.unitext.application.config;

import io.github.jhipster.config.JHipsterProperties;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache = jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(CacheConfigurationBuilder
                .newCacheConfigurationBuilder(Object.class, Object.class,
                        ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(
                        Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(org.teamnet.unitext.application.repository.UserRepository.USERS_BY_LOGIN_CACHE,
                    jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.repository.UserRepository.USERS_BY_EMAIL_CACHE,
                    jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.User.class.getName() + ".authorities",
                    jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.QASenior.class.getName(), jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.QALogs.class.getName(), jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.Lexique.class.getName(), jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.Distribution.class.getName(), jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.Construction.class.getName(), jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.Deixis.class.getName(), jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.Vscode.class.getName(), jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.Vslemme.class.getName(), jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.Nslemme.class.getName(), jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.Alemme.class.getName(), jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.Acode.class.getName(), jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.Actualisation.class.getName(), jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.Client.class.getName(), jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.Formes.class.getName(), jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.VerbeCompose.class.getName(), jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.Ontologie.class.getName(), jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.ArgumentCompose.class.getName(), jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.Reconstruction.class.getName(), jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.RegleAjustementMorphologique.class.getName(),
                    jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.RegleAjustementSyntaxique.class.getName(),
                    jcacheConfiguration);
            cm.createCache(org.teamnet.unitext.application.domain.Determinant.class.getName(), jcacheConfiguration);

            cm.createCache(org.teamnet.unitext.application.domain.Scenario.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
