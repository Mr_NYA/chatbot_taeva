package org.teamnet.unitext.application.service.dto;

public class LexiqueDTO {

    private Long id;

    String categorie;
    String predicat;
    int construction_id;
    int distribution_id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategorie() {
        return this.categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getPredicat() {
        return this.predicat;
    }

    public void setPredicat(String predicat) {
        this.predicat = predicat;
    }

    public int getConstruction_id() {
        return this.construction_id;
    }

    public void setConstruction_id(int construction_id) {
        this.construction_id = construction_id;
    }

    public int getDistribution_id() {
        return distribution_id;
    }

    public void setDistribution_id(int distribution_id) {
        this.distribution_id = distribution_id;
    }

    public LexiqueDTO(LexiqueDTO qaSenior) {
        this.categorie = qaSenior.getCategorie();
        this.predicat = qaSenior.getPredicat();
        this.construction_id = qaSenior.getConstruction_id();
        this.distribution_id = qaSenior.getDistribution_id();
    }

}
