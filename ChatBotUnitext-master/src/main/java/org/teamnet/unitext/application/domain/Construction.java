package org.teamnet.unitext.application.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Table(name = "construction")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
public class Construction implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "identifiant")
    String identifiant;

    @Column(name = "type_construction")
    String typeConstruction;

    @Column(name = "vsup")
    String vsup;

    @Column(name = "prep")
    String prep;

    @Column(name = "det")
    String det;

    @Column(name = "prep0")
    String prep0;

    @ManyToOne
    @JoinColumn(name = "x0", referencedColumnName = "id")
    Deixis x0;

    @Column(name = "prep1")
    String prep1;

    @ManyToOne
    @JoinColumn(name = "x1", referencedColumnName = "id")
    Deixis x1;

    @Column(name = "prep2")
    String prep2;

    @ManyToOne
    @JoinColumn(name = "x2", referencedColumnName = "id")
    Deixis x2;

    @Column(name = "prep3")
    String prep3;

    @ManyToOne
    @JoinColumn(name = "x3", referencedColumnName = "id")
    Deixis x3;

    // @OneToMany
    // @JoinColumn(name = "reconstruction_id", referencedColumnName = "id")
    // ArrayList<Reconstruction> reconstructions = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, targetEntity = Reconstruction.class)
    @JoinColumn(name = "construction_id")
    Set<Reconstruction> reconstructions = new HashSet<>();

    public Set<Reconstruction> getReconstructions() {
        return this.reconstructions;
    }

    public void setReconstructions(Set<Reconstruction> reconstructions) {
        this.reconstructions = reconstructions;
    }

    public Deixis getX0() {
        return x0;
    }

    public void setX0(Deixis x0) {
        this.x0 = x0;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public String getTypeConstruction() {
        return typeConstruction;
    }

    public void setTypeConstruction(String typeConstruction) {
        this.typeConstruction = typeConstruction;
    }

    public String getVsup() {
        return vsup;
    }

    public void setVsup(String vsup) {
        this.vsup = vsup;
    }

    public String getPrep() {
        return prep;
    }

    public void setPrep(String prep) {
        this.prep = prep;
    }

    public String getDet() {
        return det;
    }

    public void setDet(String det) {
        this.det = det;
    }

    public String getPrep0() {
        return prep0;
    }

    public void setPrep0(String prep0) {
        this.prep0 = prep0;
    }

    public String getPrep1() {
        return prep1;
    }

    public void setPrep1(String prep1) {
        this.prep1 = prep1;
    }

    public String getPrep2() {
        return prep2;
    }

    public void setPrep2(String prep2) {
        this.prep2 = prep2;
    }

    public String getPrep3() {
        return prep3;
    }

    public void setPrep3(String prep3) {
        this.prep3 = prep3;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Deixis getX1() {
        return x1;
    }

    public void setX1(Deixis x1) {
        this.x1 = x1;
    }

    public Deixis getX2() {
        return x2;
    }

    public void setX2(Deixis x2) {
        this.x2 = x2;
    }

    public Deixis getX3() {
        return x3;
    }

    public void setX3(Deixis x3) {
        this.x3 = x3;
    }
}
