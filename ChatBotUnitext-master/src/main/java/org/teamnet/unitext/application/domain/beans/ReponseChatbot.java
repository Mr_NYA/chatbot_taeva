package org.teamnet.unitext.application.domain.beans;

public class ReponseChatbot {
    public String intense = null;
    public String reponse;
    public String logsDetails;

    public ReponseChatbot() {

    }

    public ReponseChatbot(String reponse) {
        this.reponse = reponse;
        reponse = "None";
    }

    public ReponseChatbot(String reponse, String intense) {
        this.reponse = reponse;
        this.intense = intense;
    }
    public ReponseChatbot(String reponse, String intense, String logsDetails){
        this.reponse = reponse;
        this.intense = intense;
        this.logsDetails = logsDetails;
    }
}
