package org.teamnet.unitext.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.teamnet.unitext.application.domain.Lexique;

import java.util.List;
@Repository
public interface LexiqueRepository extends JpaRepository<Lexique, Long> {

    Lexique[] findByCategorie(String di);

    List<Lexique> findAllByCategorie(String di);
    Lexique[] findByPredicat(String predicat);

}
