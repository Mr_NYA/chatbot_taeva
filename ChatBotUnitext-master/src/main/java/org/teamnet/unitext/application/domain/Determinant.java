package org.teamnet.unitext.application.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Table(name = "determinant", uniqueConstraints = @UniqueConstraint(columnNames = { "determinant" }))
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
public class Determinant implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idDeterminant;

    @Column(name = "determinant")
    private String det;

    @Column(name = "type")
    private String type;

    @Column(name = "commentaire")
    private String commentaire;

    public Long getId() {
        return idDeterminant;
    }

    public void setId(Long id) {
        this.idDeterminant = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

    public String getDeterminant() {
        return det;
    }

    public void setDeterminant(String determinant) {
        this.det = determinant;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
}
