package org.teamnet.unitext.application.web.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Message implements Serializable {
    private static final long serialVersionUID = -1764970284520387975L;
    @JsonProperty("message")
    String message;

    @JsonProperty("token")
    String token;

    @JsonProperty("idOldTag")
    String idOldTag;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIdOldTag() {
        return idOldTag;
    }

    public void setIdOldTag(String idOldTag) {
        this.idOldTag = idOldTag;
    }
}
