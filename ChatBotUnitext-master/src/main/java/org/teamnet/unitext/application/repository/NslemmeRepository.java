package org.teamnet.unitext.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.teamnet.unitext.application.domain.Nslemme;

public interface NslemmeRepository extends JpaRepository<Nslemme, String> {

    Nslemme[] findByLemme(String lemme);
}
