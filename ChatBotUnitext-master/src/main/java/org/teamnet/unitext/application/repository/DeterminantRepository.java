package org.teamnet.unitext.application.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.teamnet.unitext.application.domain.Determinant;

public interface DeterminantRepository extends JpaRepository<Determinant, Long> {
    ArrayList<Determinant> findAll();

    Determinant findOneByDet(String det);

}
