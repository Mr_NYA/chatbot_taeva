package org.teamnet.unitext.application.domain.beans;

public class Categorie {
    public Long ID;
    public String old_categorie;
    public String new_categorie;

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getOld_categorie() {
        return old_categorie;
    }

    public String getNew_categorie() {
        return new_categorie;
    }

    public void setOld_categorie(String old_categorie) {
        this.old_categorie = old_categorie;
    }

    public void setNew_categorie(String new_categorie) {
        this.new_categorie = new_categorie;
    }



    public Categorie(Long ID, String old_categorie, String new_categorie) {
        this.ID = ID;
        this.old_categorie = old_categorie;
        this.new_categorie = new_categorie;
    }

    public Categorie() {
    }
}
