package org.teamnet.unitext.application.web.rest;

import com.google.cloud.translate.Detection;
import com.google.cloud.translate.Translate;
import com.google.cloud.translate.Translate.TranslateOption;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.teamnet.unitext.application.domain.Client;
import org.teamnet.unitext.application.domain.Lexique;
import org.teamnet.unitext.application.domain.QALogs;
import org.teamnet.unitext.application.domain.Scenario;
import org.teamnet.unitext.application.domain.beans.Categorie;
import org.teamnet.unitext.application.domain.beans.ReponseChatbot;
import org.teamnet.unitext.application.repository.*;
import org.teamnet.unitext.application.service.util.ConstructionInfo;
import org.teamnet.unitext.application.service.util.GenerateTextService;
import org.teamnet.unitext.application.service.util.UnitextService;
import org.teamnet.unitext.application.service.util.UnitextServiceDynamique;

/**
 * Resource to return information about the currently running Spring profiles.
 */
@RestController
@RequestMapping("/api")
public class ChatbotAPI {

    private boolean questionEtaitEnAnglais = false;
    private static Logger logger = Logger.getLogger("ChatbotAPI");
    private HttpServletRequest request;
    public String clientIP = "";
    public String requestIdOldTag = "";
    public String responseIdOldTag = "";

    // @Autowired
    // private HttpSession oldSession;

    @Autowired
    GenerateTextService generateTextService;

    @Autowired
    QASeniorRepository QASeniorRepository;

    @Autowired
    QALogsRepository QALogsRepository;

    UnitextService unitextService = new UnitextService();

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    private ScenarioRepository scenarioRepository;

    @Autowired
    private LexiqueRepository lexiqueRepository;

    @GetMapping(value = "/isCorpusEnding")
    public String isCorpusEnding(
        @RequestParam String token,
        @RequestParam String directoryName,
        HttpServletRequest request,
        HttpServletResponse response
    )
        throws IOException {
        Client client = clientRepository.findOneByToken(token);
        if (client == null) return "Token obligatoire";

        File directory = new File(
            System.getProperty("user.home") + "/workspace/" + directoryName
        );

        if (!directory.exists()) {
            return "NOTFOUND";
        }

        File result = new File(
            directory.getAbsolutePath() + "/resultat_corpusML.txt"
        );

        File endTokenization = new File(
            directory.getAbsolutePath() + "/fin.end"
        );

        return (endTokenization.exists() && result.exists()) + "";
    }

    @GetMapping(value = "/getResultFile")
    public String getResultFile(
        @RequestParam String token,
        @RequestParam String directoryName,
        HttpServletRequest request,
        HttpServletResponse response
    )
        throws IOException {
        Client client = clientRepository.findOneByToken(token);
        if (client == null) return "Token obligatoire";
        logger.log(Level.WARNING, "directoryName: {0}", directoryName);

        File directory = new File(
            System.getProperty("user.home") + "/workspace/" + directoryName
        );

        if (!directory.exists()) {
            return "NOTFOUND";
        }

        File endTokenization = new File(
            directory.getAbsolutePath() + "/fin.end"
        );

        if (!endTokenization.exists()) {
            return "ENCOURS";
        }

        File fileResult = new File(
            directory.getAbsolutePath() + "/resultat_corpusML.txt"
        );

        if (!fileResult.exists()) {
            deleteDirectory(directory);
            return "DELETED";
        }

        response.setContentType("application/text");
        response.addHeader(
            "Content-Disposition",
            "attachment; filename=" + fileResult.getName()
        );

        Files.copy(fileResult.toPath(), response.getOutputStream());
        response.getOutputStream().flush();
        deleteDirectory(directory);
        return "";
    }

    private void deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                deleteDirectory(file);
            }
        }
        directoryToBeDeleted.delete();
    }

    @PostMapping(
        value = "/generatorUnitex",
        consumes = MediaType.MULTIPART_FORM_DATA_VALUE
    )
    public @ResponseBody String generateUnitex(
        @RequestParam(value = "file") final List<MultipartFile> files,
        @RequestParam(value = "token") final String token
    )
        throws IOException {
        Client client = clientRepository.findOneByToken(token);
        if (client == null) return "Token obligatoire";
        logger.log(Level.WARNING, "generateUnitex: {0}", "OK");

        List<File> dics = new ArrayList<File>();
        List<byte[]> dictionnairesInf = new ArrayList();
        // files bytes
        Date now = new Date();
        File tmpFolder = new File(
            System.getProperty("user.home") +
            "/workspace/Corpus" +
            now.getTime()
        );

        tmpFolder.mkdir();

        File corpusTXT = new File(
            tmpFolder.getAbsoluteFile() + "/corpusML.txt"
        );

        File graphe = new File(
            tmpFolder.getAbsoluteFile() + "/GRAPHE_PERE_PROJET_VERBE.fst2"
        );

        try {
            logger.log(Level.WARNING, "****** files size: {0}", files.size());

            for (MultipartFile multipartFile : files) {
                OutputStream os = null;
                InputStream initialStream = multipartFile.getInputStream();
                byte[] buffer = new byte[initialStream.available()];
                initialStream.read(buffer);
                String fileName = multipartFile.getOriginalFilename();

                logger.log(
                    Level.WARNING,
                    "****** uploaded fileName: {0}",
                    fileName
                );

                if (fileName.endsWith("txt")) {
                    os = new FileOutputStream(corpusTXT);
                    logger.log(
                        Level.WARNING,
                        "****** corpus detected: {0}",
                        fileName
                    );
                } else if (fileName.endsWith("fst2")) {
                    os = new FileOutputStream(graphe);
                    logger.log(
                        Level.WARNING,
                        "****** graphe detected: {0}",
                        fileName
                    );
                } else if (fileName.endsWith("bin")) {
                    logger.log(
                        Level.WARNING,
                        "****** bin detected: {0}",
                        fileName
                    );

                    File binFile = new File(
                        tmpFolder.getAbsoluteFile() + "/" + fileName
                    );
                    os = new FileOutputStream(binFile);
                    dics.add(binFile);
                } else if (fileName.endsWith("inf")) {
                    logger.log(
                        Level.WARNING,
                        "****** inf detected: {0}",
                        fileName
                    );

                    File binInf = new File(
                        tmpFolder.getAbsoluteFile() + "/" + fileName
                    );
                    os = new FileOutputStream(binInf);
                }

                os.write(buffer);
                os.flush();
                os.close();
            }
        } catch (Exception e) {}

        // instance
        UnitextServiceDynamique unitextService = new UnitextServiceDynamique(
            tmpFolder,
            graphe,
            dics,
            corpusTXT
        );

        Thread thread = new Thread(unitextService);
        thread.start();
        //init

        return tmpFolder.getName();
    }

    @PostMapping(value = "/UpdateLexique")
    public int UpdateLexique(
        @RequestBody Categorie categorie
    ){
        int numberOfLexique = 0 ;
        logger.info("Dans la méthode UpdateLexique");
        logger.info("old cat ===> " + categorie.old_categorie);
        logger.info("New cat ===> " + categorie.new_categorie);

        if(categorie.old_categorie != null && categorie.new_categorie != null){
            List <Lexique> lexiques = lexiqueRepository.findAllByCategorie(categorie.old_categorie);
            numberOfLexique = lexiques.size();
            logger.info("le nombre de lexique trouvé pour la categorie ===>  " + categorie.old_categorie +" est ======>" + numberOfLexique);
            if(numberOfLexique > 0 ){
                int i = numberOfLexique;
                while ( i > 0 ){
                    i--;
                    if(lexiques.get(i) !=  null){
                        Lexique lexiqueFound = lexiques.get(i);
                        logger.info("l'identifiant du lexique trouvé est " + lexiqueFound.getId());
                        logger.info("Avant la modification de la categorie du Lexique ID " + lexiqueFound.getId());
                        lexiqueFound.setCategorie(categorie.new_categorie);
                        lexiqueRepository.save(lexiqueFound);
                        logger.info("Après la modification de la categorie du Lexique ID " + lexiqueFound.getId());

                    }
                }
            }
        }

        return numberOfLexique;
    }

    @PostMapping(value = "/ChatbotAPI")
    public MessageChat chatWithMe(
        @RequestBody Message json,
        HttpServletRequest request
    ) throws UnsupportedEncodingException {
        Translate translate = TranslateOptions
            .newBuilder()
            .setApiKey("AIzaSyDMZObdAt6VWB41lLGXCbOtGv0DoKmCHx8")
            .build()
            .getService();

        Client client = clientRepository.findOneByToken(json.token);
        String messageDecoded = java.net.URLDecoder.decode(
            json.message,
            "UTF-8"
        );
        try {
            if(json.idOldTag != null){
                 requestIdOldTag = java.net.URLDecoder.decode(
                    json.idOldTag,
                    "UTF-8"
                );
                if(requestIdOldTag != null){
                    logger.info("idOldTag ==================> "+ requestIdOldTag);
                }
            }
            else {
                requestIdOldTag = null;
            }

            logger.info("==================================> ClientIP : |" + clientIP + "|");
            String newClientIP = getClientIp();
            logger.info("==================================> newClientIP : |" + newClientIP + "|");

            if(clientIP != "" && !clientIP.equals(newClientIP) ){
                generateTextService.indexLexique = 0;
                logger.info(" =======================> reinitialisation indexLexique <=========================");
            }
            if(clientIP.equals(newClientIP)){
                logger.info("========================> la même @IP");
            }
            clientIP = newClientIP;

            if (client == null) return new MessageChat(
                "Requête non authorisée. Erreur 403.",
                false
            );
            this.request = request;


            Detection detections = translate.detect(messageDecoded);

            boolean oldCheckIsEnglish;
            if (
                detections.getLanguage().equals("en") &&
                !messageDecoded.contains("X=")
            ) {
                Translation translation = translate.translate(
                    messageDecoded,
                    TranslateOption.sourceLanguage("en"),
                    TranslateOption.targetLanguage("fr")
                );
                messageDecoded = translation.getTranslatedText();
                // questionEtaitEnAnglais = true;
            }

            HttpSession session = this.request.getSession();

            // if (oldSession == null) {
            // oldSession = session;
            // }

            Long idOldTag = (Long) session.getAttribute("TAG_QUESTION");
            if(idOldTag == null && requestIdOldTag != null)
            {
                idOldTag = Long.parseLong(requestIdOldTag) ;
                logger.info("recuperation idOldTag depuis la requette " + idOldTag);
            }
            else {
                logger.info("recuperation idOldTag depuis la session " + idOldTag);
            }
            // resultat contient TAGS

            String resultat;
            if(messageDecoded.startsWith("[")){
                resultat = messageDecoded;
            }
            else {
                resultat = unitextService.chatWithMe(messageDecoded);
            }

            logger.log(
                Level.WARNING,
                "tag resultat befor transformReponseIfScenario: ======================> {0}",
                resultat
            );
           resultat =
               transformReponseIfScenario(session, messageDecoded, resultat);
            logger.log(
                Level.WARNING,
                "tag resultat after transformReponseIfScenario: {0}",
                resultat
            );

            Integer lastTagIndex = resultat.lastIndexOf(']') + 1;
            Integer firstTagIndex = resultat.indexOf('[');
            logger.info("Resultat : " + resultat);
            generateTextService.listDeixisID.clear();

           if (firstTagIndex < 0) {
               String error = "Desolé, je n'ai pas de réponse";

               fillQALogsDetails(
                   client,
                   messageDecoded,
                   error,
                   "None",
                   "None",
                   false,
                   "Aucun Tag trouvé pour la question : " + messageDecoded
               );
               error =
                   translateIfEnglish(
                       questionEtaitEnAnglais,
                       error,
                       translate
                   );
               oldCheckIsEnglish = questionEtaitEnAnglais;
               questionEtaitEnAnglais = false;
               // oldSession = session;
               return new MessageChat(error, oldCheckIsEnglish);
           } else {
               String tag = resultat.substring(firstTagIndex, lastTagIndex);
                logger.info("tag récupéré ============================> : " + tag);

                logger.log(Level.WARNING, "tag new question: {0}", tag);

                Matcher matcher = Pattern.compile("\\[(.*?)\\]").matcher(tag);
                List<String> tags = new ArrayList<>();
                while (matcher.find()) {
                    tags.add(
                        tag.substring(matcher.start() + 1, matcher.end() - 1)
                    );
                }
                logger.info("TAGS: " + tags.toString());

                StringBuilder strResponse = new StringBuilder();

                for (String newtag : tags) {
                    boolean valid = true;
                    ReponseChatbot responseChatbot = generateTextService.generateResponse(
                        newtag,
                        new ReponseChatbot(""),
                        new ConstructionInfo(),
                        true
                    );

                    if (
                        responseChatbot.reponse.contains(
                            "Desolé, je n'ai pas de réponse"
                        )
                    ) {
                        valid = false;
                    }

                    responseChatbot.reponse =
                        translateIfEnglish(
                            questionEtaitEnAnglais,
                            responseChatbot.reponse,
                            translate
                        );

                    strResponse.append(responseChatbot.reponse);

                    fillQALogsDetails(
                        client,
                        messageDecoded,
                        strResponse.toString(),
                        tag,
                        responseChatbot.intense,
                        valid,
                        responseChatbot.logsDetails
                    );

                    strResponse.append(". ");
                }
                oldCheckIsEnglish = questionEtaitEnAnglais;
                questionEtaitEnAnglais = false;
                // oldSession = session;
                return new MessageChat(
                    strResponse.toString(),
                    oldCheckIsEnglish,
                    tag,
                    responseIdOldTag
                );
           }
        } catch (Exception e) {
            String error = "Desolé, il y'a une erreur dans mon système";
            error =
                translateIfEnglish(questionEtaitEnAnglais, error, translate);
            boolean oldCheckIsEnglish = questionEtaitEnAnglais;
            questionEtaitEnAnglais = false;
            logger.log(
                Level.SEVERE,
                "Error in ChatbotAPI: {0}",
                e.getLocalizedMessage()
            );
            fillQALogs(
                client,
                messageDecoded,
                error,
                "None",
                "None",
                false
            );
            // e.printStackTrace();
            return new MessageChat(error, oldCheckIsEnglish);
        }
    }

    private String transformReponseIfScenario(
        HttpSession session,
        String messageEncode,
        String tagRecived
    ) {
        logger.log(
            Level.WARNING,
            "transformReponseIfScenario ------------------------- tagRecived {0}",
            tagRecived
        );
        responseIdOldTag = null;

        if (tagRecived == null) {
            tagRecived = messageEncode;
        }

        Long idOldTag = (Long) session.getAttribute("TAG_QUESTION");
        if(idOldTag == null && requestIdOldTag != null)
        {
            idOldTag = Long.parseLong(requestIdOldTag) ;
            logger.info("recuperation idOldTag depuis la requette " + idOldTag);
        }
        else {
            logger.info("recuperation idOldTag depuis la session " + idOldTag);
        }

        Integer lastTagIndex = tagRecived.lastIndexOf(']') + 1;
        Integer firstTagIndex = tagRecived.indexOf('[');
        String tagFiltred = tagRecived;
        if (firstTagIndex >= 0) { // c'est un simple tag
            tagFiltred = tagRecived.substring(firstTagIndex, lastTagIndex);
        }

        if (idOldTag == null) {
            idOldTag = new Long(1);
        }

        logger.log(
            Level.WARNING,
            "scenarioRepository find tagFiltred {0}",
            tagFiltred
        );
        logger.log(
            Level.WARNING,
            "scenarioRepository find idOldTag {0}",
            idOldTag
        );
        Scenario newScenario = scenarioRepository.findOneByCategorieAndIdParent(
            tagFiltred,
            idOldTag
        );
        if (newScenario != null) {
            logger.log(
                Level.WARNING,
                "scenario finded id: {0}",
                newScenario.getId()
            );
            if (newScenario.getId() != newScenario.getIdDirection()) {
                logger.log(
                    Level.WARNING,
                    "finded scenario id {0}",
                    newScenario.getId()
                );
                Scenario scenarioDirection = scenarioRepository.findOneById(
                    newScenario.getIdDirection()
                );
                logger.log(
                    Level.WARNING,
                    "obj direction ? {0}",
                    (scenarioDirection == null)
                );
                if (scenarioDirection != null) {
                    logger.log(
                        Level.WARNING,
                        "1.save TAG_QUESTION : {0}",
                        scenarioDirection.getId()
                    );
                    session.setAttribute(
                        "TAG_QUESTION",
                        scenarioDirection.getId()
                    );
                    responseIdOldTag = scenarioDirection.getId().toString();
                    logger.log(
                        Level.WARNING,
                        "Direction categorie {0}",
                        scenarioDirection.getCategorie()
                    );
                    return scenarioDirection.getCategorie();
                }
            }
            logger.log(
                Level.WARNING,
                "2.save TAG_QUESTION : {0}",
                newScenario.getId()
            );
            session.setAttribute("TAG_QUESTION", newScenario.getId());
            responseIdOldTag = newScenario.getId().toString();
        } else {
            List<Scenario> scenarioSelf = scenarioRepository.findByCategorie(
                tagFiltred
            );
            for (Scenario sc : scenarioSelf) {
                Long id = sc.getId();
                if (id == sc.getIdDirection() && id == sc.getIdParent()) {
                    logger.log(Level.WARNING, "3.save TAG_QUESTION : {0}", id);
                    session.setAttribute("TAG_QUESTION", id);
                    responseIdOldTag = id.toString();
                    break;
                }
            }
        }

        // session.removeAttribute("TAG_QUESTION");

        // oldSession = session;

        return tagRecived;
    }

    public String translateIfEnglish(
        boolean isEnglish,
        String text,
        Translate translate
    ) {
        if (isEnglish) {
            Translation translation = translate.translate(
                text,
                TranslateOption.sourceLanguage("fr"),
                TranslateOption.targetLanguage("en")
            );
            text = translation.getTranslatedText();
        }
        return text;
    }

    private void fillQALogs(
        Client client,
        String input,
        String response,
        String intent,
        String intenteReponse,
        boolean valide
    ) {
        logger.info("Saving logs in database");
        QALogs qa_logs = new QALogs();
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        date = new Date(sdf.format(date));
        qa_logs.setIpRequest(getClientIp());
        qa_logs.setIdClient(client);
        qa_logs.setDate(date);
        qa_logs.setInputMessage(input);
        qa_logs.setIntent(intent);
        qa_logs.setIntentReponse(intenteReponse);
        qa_logs.setResponse(response);
        qa_logs.setValide(valide);
        QALogsRepository.save(qa_logs);
    }

    private void fillQALogsDetails(
        Client client,
        String input,
        String response,
        String intent,
        String intenteReponse,
        boolean valide,
        String logsDetail
    ) {
        logger.info("Saving logs in database");
        QALogs qa_logs = new QALogs();
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        date = new Date(sdf.format(date));
        qa_logs.setIpRequest(getClientIp());
        qa_logs.setIdClient(client);
        qa_logs.setDate(date);
        qa_logs.setInputMessage(input);
        qa_logs.setIntent(intent);
        qa_logs.setIntentReponse(intenteReponse);
        qa_logs.setResponse(response);
        qa_logs.setValide(valide);
        qa_logs.setLogsDetails(logsDetail);
        QALogsRepository.save(qa_logs);
    }
    public class MessageChat {

        private String that;
        private boolean isEnglish;
        private String Tags;
        private String idOldTag;

        public String getThat() {
            return that;
        }

        public boolean getIsEnglish() {
            return isEnglish;
        }

        public void setIsEnglish(boolean isEnglish) {
            this.isEnglish = isEnglish;
        }

        public void setThat(String that) {
            this.that = that;
        }
        public String getTags() {
            return Tags;
        }

        public void setTags(String tags) {
            Tags = tags;
        }

        public String getIdOldTag() {
            return idOldTag;
        }

        public void setIdOldTag(String idOldTag) {
            this.idOldTag = idOldTag;
        }

        public MessageChat(String value, boolean isEnglish) {
            this.that = value;
            this.isEnglish = isEnglish;
        }

        public MessageChat(String that, boolean isEnglish, String tags) {
            this.that = that;
            this.isEnglish = isEnglish;
            Tags = tags;
        }

        public MessageChat(String that, boolean isEnglish, String tags, String idOldTag) {
            this.that = that;
            this.isEnglish = isEnglish;
            Tags = tags;
            this.idOldTag = idOldTag;
        }
    }

    private String getClientIp() {
        String remoteAddr = "";

        if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
            }
        }

        logger.log(Level.INFO, "REQUEST ID : {0}", remoteAddr);

        return remoteAddr;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }
}
