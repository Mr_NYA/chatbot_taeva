package org.teamnet.unitext.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.teamnet.unitext.application.domain.Vslemme;

public interface VslemmeRepository extends JpaRepository<Vslemme, String> {

    Vslemme findOneByLemme(String lemme);
}
