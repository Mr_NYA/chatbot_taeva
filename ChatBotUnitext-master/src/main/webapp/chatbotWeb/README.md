# VoiceBotJS

VoiceBot which uses Unitex chatbot (https://teamnet.ml/voicebot-api). 
It's a webApp that currently only runs on Chrome.

See https://developer.mozilla.org/fr/docs/Web/API/SpeechRecognition#Browser_compatibility
