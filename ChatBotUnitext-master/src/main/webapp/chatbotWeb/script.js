var SpeechRecognition = webkitSpeechRecognition
var SpeechGrammarList = webkitSpeechGrammarList

var hotwords = [
    'robot',
    'alexa',
    'ubo',
    'hubo',
    'eko',
    'echo',
    'eco',
    'barbamama',
    'barbule',
    'barbulle'
]
var hotwordsList = new SpeechGrammarList()
for (var i = 0; i < hotwords.length; i++) {
    hotwordsList.addFromString(hotwords[i], 2)
}

/*
$.ajax({
    url:'hotwords.txt',
    success: function (data){
      console.log(data.trim().split('\n'))
    }
  });*/

var recognition = new SpeechRecognition()
recognition.grammars = hotwordsList
recognition.continuous = true
recognition.lang = 'fr-FR'
recognition.interimResults = true
recognition.maxAlternatives = 1

var trailingDate
var trailingTimeout

var confidence = document.querySelector('.confidence')
var microphone = document.querySelector('#microphone')
var manualInput = document.querySelector('#phrase')
var button = document.querySelector('#button')

var isMicBtnOn = false

var user = '<div id="user">'
var chatbotAnswer = '<div id="chatbot">'
var chatbotError = '<div id="chatbot-err">'
var endDiv = '</div>'

recognition.start()

function micOnOff() {
    isMicBtnOn = !isMicBtnOn
    if (isMicBtnOn) {
        microphone.style.color = '#e04343'
    } else {
        microphone.style.color = '#4c4c4c'
    }
}

microphone.onclick = function () {
    micOnOff()
}

recognition.onstart = function (event) { }

recognition.onresult = function (event) {
    var interims = []
    for (var i = event.resultIndex; i < event.results.length; ++i) {
        if (event.results[i].isFinal) {
            var finalTranscript = event.results[i][0].transcript.trim()

            var words = finalTranscript.split(' ')

            //Trailing after result
            if (Date.now() < trailingDate) {
                clearTimeout(trailingTimeout)
                microphone.style.color = '#e04343'
                console.log("trailing")

                $('#phrase').attr('placeholder', event.results[i][0].transcript)
                confidence.textContent = 'Confidence: ' + event.results[0][0].confidence
                txtToChatbotToTts(finalTranscript)
                trailingDate = Date.now() + 5000
                trailingTimeout = setTimeout(function () {
                    microphone.style.color = '#4c4c4c'
                    recognition.stop()
                    console.log("stopped")
                }, 5000);
            }
            //HOTWORD TRIGGERED
            else if (hotwords.includes(words[0]) && !isMicBtnOn) {

                console.log("hotword & no micBtnOn")

                $('#phrase').attr('placeholder', event.results[i][0].transcript)
                console.log('Hotword heard : ' + finalTranscript)
                var tmp = finalTranscript.replace(words[0], '').trim()
                console.log('replaced phrase : ' + tmp)
                confidence.textContent = 'Confidence: ' + event.results[0][0].confidence
                txtToChatbotToTts(tmp)
                trailingDate = Date.now() + 5000
                trailingTimeout = setTimeout(function () {
                    microphone.style.color = '#4c4c4c'
                    recognition.stop()
                    console.log("stopped")
                }, 5000);
            } else if (isMicBtnOn) {
                console.log("micBtnOn")
                $('#phrase').attr('placeholder', event.results[i][0].transcript)
                confidence.textContent = 'Confidence: ' + event.results[0][0].confidence
                txtToChatbotToTts(finalTranscript)
            }
        } else {
            var interimTranscript = event.results[i][0].transcript.trim()
            interims.push(interimTranscript)
            for (var i = 0; i < hotwords.length; i++) {
                if (interimTranscript.startsWith(hotwords[i]) && interims.length < 2) {
                    $('#phrase').attr('placeholder', event.results[i][0].transcript)
                    microphone.style.color = '#e04343'
                    console.log('interim : ' + interimTranscript)
                }
            }
        }
    }
}

recognition.onend = function (event) {
    recognition.stop()
    recognition.start()
}

recognition.onnomatch = function (event) {
    console.log("Didn't recognize anything")
}
recognition.onerror = function (event) {
    console.log('Error occurred in recognition: ' + event.error)
}


const poserMoiUneQuestion = '[INTERPELLATION]';
var dateLastQuestion = new Date();
const delaisIntervalle = 60000;//60 secondes
var isWaiting = false;
setInterval(function () {
    if (!isWaiting && new Date().getTime() - dateLastQuestion.getTime() >= delaisIntervalle) {
        console.log('lancement de la questio automatique')
        txtToChatbotToTts(poserMoiUneQuestion);
    }
}, delaisIntervalle)

function txtToChatbotToTts(parole) {
    isWaiting = true;
    dateQuestion = new Date();
    if (encodeURI(parole.trim()) == '') {
        return
    }

    var protocol = document.querySelector('#protocol')
    var port = document.querySelector('#port');
    if (parole !== poserMoiUneQuestion) {
        $('#data-return').append(user + parole.trim() + endDiv);
    }

    parole = encodeURI(parole.trim())
    console.log("REQUEST SENT : " + parole)

    var obj = {}
    obj.message = parole
    obj.token = 'cLt9yYygYtVY6rCuQt1F'
    var data = JSON.stringify(obj)
    console.log(protocol.value + '://localhost:' + port.value + '/api/ChatbotAPI')
    $.ajax({
        url: protocol.value + '://localhost:' + port.value + '/api/ChatbotAPI',
        type: 'POST',
        crossOrigin: 'false',
        data: data,
        dataType: 'json',
        contentType: 'application/json',
        success: function (answer) {
            dateQuestion = new Date();
            isWaiting = false;
            console.log('answer', answer)
            $('#data-return').append(chatbotAnswer + answer.that + endDiv)
            var elem = document.getElementById('data-return')
            elem.scrollTop = elem.scrollHeight
            var msg = new SpeechSynthesisUtterance(answer.that)
            console.log('data.isEnglish', answer.isEnglish)

            if (!answer.isEnglish) {
                msg.lang = 'fr-FR'
            } else {
                msg.lang = 'en-EN'
            }
            console.log('msg.lang', msg.lang)
            window.speechSynthesis.speak(msg)
        },
        error: function () {
            $('#data-return').append(chatbotError + "API communication issues" + endDiv)
            var elem = document.getElementById('data-return')
            elem.scrollTop = elem.scrollHeight
        }
    })
}

document.addEventListener("keyup", function (event) {
    event.preventDefault()
    if (event.keyCode === 13) {
        button.click()
    }
})

function manualText() {
    txtToChatbotToTts(manualInput.value)
    manualInput.value = ''
}
