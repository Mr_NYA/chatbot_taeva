FROM openjdk:8-jdk-slim
COPY . /code
WORKDIR /code
RUN echo Europe/Paris > /etc/timezone
ENV TZ=Europe/Paris
ENV LD_LIBRARY_PATH=/code/additionalFiles
RUN mv /code/workspace $HOME/
RUN mkdir -p $HOME/.m2/repository/fr/umlv/unitex/jni/UnitexJni/1.0.0/
RUN cp /code/additionalFiles/* $HOME/.m2/repository/fr/umlv/unitex/jni/UnitexJni/1.0.0/
WORKDIR /code/ChatBotUnitext-master
RUN sed -i 's/\r//' mvnw
# RUN ./mvnw install